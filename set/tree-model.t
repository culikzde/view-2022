
/* tree-model.t */

/* ---------------------------------------------------------------------- */

class Record
{
   public:
      QString name;
      Record * above;
      QList < Record * > items;
};

/* ---------------------------------------------------------------------- */

class TreeModel : public QAbstractItemModel
{
   public:
      QVariant data (const QModelIndex & index, int role) const override;
      Qt::ItemFlags flags (const QModelIndex & index) const override;
      QVariant headerData (int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
      QModelIndex index (int line, int column, const QModelIndex & parent = QModelIndex ()) const override;
      QModelIndex parent (const QModelIndex & index) const override;
      int rowCount (const QModelIndex & parent = QModelIndex ()) const override;
      int columnCount (const QModelIndex & parent = QModelIndex ()) const override;

      Qt::DropActions supportedDropActions () const override;
      QStringList mimeTypes() const override;
      bool canDropMimeData (const QMimeData * data, Qt::DropAction action,
                            int row, int column, const QModelIndex & parent) const override;
      bool dropMimeData (const QMimeData * data, Qt::DropAction action,
                         int line, int column, const QModelIndex & parent) override;

   private:
      Record * rootItem;

   public:
      TreeModel (QObject * parent, Record * root);
      ~TreeModel ();
};

/* ---------------------------------------------------------------------- */

TreeModel::TreeModel (QObject * parent, Record * root) :
   QAbstractItemModel (parent),
   rootItem (root)
{
}

TreeModel::~TreeModel ()
{
}

/* ---------------------------------------------------------------------- */

int TreeModel::columnCount (const QModelIndex & parent) const
{
   return 1;
}

QVariant TreeModel::data (const QModelIndex & index, int role) const
{
   QVariant result;
   if (index.isValid ())
   {
      if (role == Qt::DisplayRole)
      {
         Record * item = static_cast < Record * > (index.internalPointer ());
         result = item->name;
         // print ("DATA", result, item);
      }
   }
   return result;
}

/* ---------------------------------------------------------------------- */

QModelIndex TreeModel::index (int line, int column, const QModelIndex & parent) const
{
   QModelIndex result;
   if (hasIndex (line, column, parent))
   {
      Record * parentItem;
      if (! parent.isValid ())
         parentItem = rootItem;
      else
         parentItem = static_cast < Record * > (parent.internalPointer ());

      Record * childItem = NULL;
      if (parentItem != NULL)
      {
         if (line < parentItem->items.size ())
            childItem = parentItem->items [line];
      }

      if (childItem != NULL)
      {
         result = createIndex (line, column, childItem);
         // print ("INDEX OF", parentItem->name, parentItem, "LINE", line, "IS", childItem->name, childItem);
      }
   }
   return result;
}

QModelIndex TreeModel::parent (const QModelIndex & index) const
{
   QModelIndex result;
   if (index.isValid ())
   {
      Record * childItem = static_cast < Record * > (index.internalPointer ());
      Record * parentItem = childItem->above;

      if (parentItem != NULL && parentItem != rootItem)
      {
         int pos = parentItem->items.indexOf (childItem);
         result = createIndex (pos, 0, parentItem);
         // print ("PARENT OF", childItem->name, childItem, "IS", parentItem->name, parentItem);
      }
   }
   return result;
}

int TreeModel::rowCount (const QModelIndex & parent) const
{
   Record * parentItem;
   if (! parent.isValid ())
      parentItem = rootItem;
   else
      parentItem = static_cast < Record * > (parent.internalPointer ());

   int cnt = 0;

   if (parentItem != NULL)
   {
      cnt = parentItem->items.size ();
   }

   // print ("ROW COUNT OF", parentItem->name, parentItem, "IS", cnt);

   return cnt;
}

/* ---------------------------------------------------------------------- */

QVariant TreeModel::headerData (int section, Qt::Orientation orientation, int role) const
{
   QVariant result;
   if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
      result = "title";
   return result;
}

Qt::ItemFlags TreeModel::flags (const QModelIndex & index) const
{
   if (! index.isValid ())
      return Qt::ItemIsDropEnabled;
   else
      return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled | Qt::ItemIsDropEnabled ;
}

Qt::DropActions TreeModel::supportedDropActions () const
{
     return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
}

QStringList TreeModel::mimeTypes() const
{
   QStringList types;
   types << "application/x-color";
   return types;
}

bool TreeModel::canDropMimeData (const QMimeData * data, Qt::DropAction action,
                                 int row, int column, const QModelIndex & parent) const
{
   bool ok = false;
   if (data->hasColor ())
   {
      if (parent.isValid ())
      {
          Record * item = static_cast < Record * > (parent.internalPointer ());
          ok = true;
      }
   }
   return ok;
}

bool TreeModel::dropMimeData (const QMimeData * data, Qt::DropAction action,
                              int row, int column, const QModelIndex & parent)
{
   bool ok = false;
   if (data->hasColor ())
   {
      // QColor color = data->colorData ().value <QColor> ();

      if (parent.isValid ())
      {
          Record * item = static_cast < Record * > (parent.internalPointer ());
          ok = true;
      }
   }
   return ok;
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
