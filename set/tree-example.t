
/* ---------------------------------------------------------------------- */

struct Item
{
   public :
      QString text;

      // string value;
      QIcon     icon;
      QColor    color;
      bool      selected;
      long      size;
      QDateTime time;

      Item * up;
      QList <Item *> subitems;
};

/* ---------------------------------------------------------------------- */

TreeModule
{
   Record = Item;    // data class

   name = text;      // field with text
   above = up;       // field with pointer to above level
   items = subitems; // field with list of subitems

   root = storage;   // variable identifier for pointer to tree root
}

/* ---------------------------------------------------------------------- */

PropertyTableModule
{
   Record = Item;
   Properties = PropertyWindow;
}

/* ---------------------------------------------------------------------- */

/*
XmlIO
{
    Record = Item;
    items = subitems;
    Collection = Data;
}
*/

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
   private:

      /* example data */

      Item * top
      {
          text = "top level";
          Item * branch
          {
             text = "branch";
             up = top;
             Item * leaf
             {
                text = "leaf";
                up = branch; // important
             }
             subitems.append (leaf);
             Item * leaf2
             {
                text = "leaf 2";
                up = branch;
             }
             subitems.append (leaf2);
          }
          subitems.append (branch);
      }

      /* forward declarations */

   private:

      void readFile (QString fileName);
      void writeFile (QString fileName);

   public:

      void openFile ();
      void saveFile ();

   private:

      /* user interface */

      QMenuBar * mainMenu
      {
          QMenu * fileMenu
          {
              title = "&File";

              QAction * openMenuItem
              {
                 text = "&Open...";
                 shortcut = "Ctrl+O";
                 triggered = openFile;
              }

              QAction * saveMenuItem
              {
                 text  = "Save &As...";
                 shortcut = "Ctrl+S";
                 triggered { saveFile (); }
              }

              QAction * quitMenuItem
              {
                  text = "&Quit";
                  shortcut = "Ctrl+Q";
                  triggered () { this->close (); }
              }
          }
      }

      QSplitter * hsplitter
      {
         QTreeView * treeView { }
         QGraphicsView * graphicsView { }
         PropertyWindow * prop { }
      }

   public:

      /* constructor */

      Window (QWidget * parent = null) :
         QMainWindow (parent)
      {
          #if 0
          Item * top = new Item;
          top->text = "top level";

          Item * branch = new Item;
          branch->text = "branch";
          top->subitems.append (branch);
          branch->up = top;

          for (int i = 1; i <= 3; i++)
          {
             Item * leaf = new Item;
             leaf->text = "leaf " + QString::number (i);
             branch->subitems.append (leaf);
             leaf->up = branch;
          }
          #endif

          TreeModel * model = new TreeModel (this, top);
          treeView->setModel (model);
      }
};

/* ---------------------------------------------------------------------- */

void Window::readFile (QString fileName)
{
}

void Window::writeFile (QString fileName)
{
}

void Window::openFile ()
{
   QString fileName = QFileDialog::getOpenFileName (this, "Open file");
   if (fileName != "")
      readFile (fileName);
}

void Window::saveFile ()
{
   QString fileName = QFileDialog::getSaveFileName (this, "Save file");
   if (fileName != "")
      writeFile (fileName);
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
    QApplication * appl = new QApplication (argc, argv);
    Window * window = new Window;
    window->show ();
    appl->exec ();
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
