
/* builder.t */

// #include <QWidget>

// #include <QTabWidget>
// #include <QToolBar>
// #include <QVBoxLayout>

// #include <QPushButton>
// #include <QCheckBox>
// #include <QLineEdit>
// #include <QTextEdit>
// #include <QTreeWidget>
// #include <QTableWidget>

// #include <QMouseEvent>

/* ---------------------------------------------------------------------- */

class Builder : public QWidget
{
   // Q_OBJECT

   public:
      Builder (QWidget * parent = NULL);

   protected:
      void dragEnterEvent (QDragEnterEvent *event);
      void dropEvent (QDropEvent *event);
};

/* ---------------------------------------------------------------------- */

class Adapter : public QWidget
{
   public:
      Adapter (QWidget * p_widget, QWidget * parent = NULL);

   private:
      QWidget * widget;
      bool down;
      QPoint point;
      Qt::MouseButton button;

   protected:
      bool eventFilter (QObject *obj, QEvent *event) override;
};

Adapter::Adapter (QWidget * p_widget, QWidget * parent) :
   QWidget (parent),
   widget (p_widget),
   down (false)
{ }

bool Adapter::eventFilter (QObject * obj, QEvent * event)
{

    QEvent::Type type = event->type ();
    // cout << "eventFilter" << endl;
    if (type == QEvent::MouseButtonPress)
    {
        // cout << "MouseButtonPress" << endl;
        // if (obj == widget)
        {
           QMouseEvent *mouseEvent = static_cast <QMouseEvent *> (event);
           down = true;
           point = mouseEvent->pos ();
           /* Qt::MouseButton  */ button = mouseEvent->button ();
           Qt::KeyboardModifiers modifiers = mouseEvent->modifiers ();

           if (button == Qt::LeftButton)
           {
              // showProperties ();
           }

           else if (button == Qt::MiddleButton)
           {
              if (modifiers == Qt::ControlModifier)
                 widget->lower ();
              else
                 widget->raise ();
           }
        }
        return true;
    }
    else if (type == QEvent::MouseMove)
    {
        // cout << "MouseMove" << endl;
        // if (obj == widget)
        {
           if (down)
           {
              QMouseEvent *mouseEvent = static_cast <QMouseEvent *> (event);
              /* Qt::MouseButton button = mouseEvent->button (); */
              Qt::KeyboardModifiers modifiers = mouseEvent->modifiers ();

              if (button == Qt::LeftButton)
              {
                 QPoint pos = widget->pos () + mouseEvent->pos () - point;
                 widget->move (pos);
              }
              else if (button == Qt::RightButton)
              {
                 QPoint delta = mouseEvent->pos () - point;
                 QSize delta2 = QSize (delta.x(), delta.y());
                 QSize s = widget->size () + delta2;
                 widget->resize (s);
                 point = mouseEvent->pos ();
              }
           }
       }
       return true;
    }
    else if (type == QEvent::MouseButtonRelease)
    {
        // if (obj == widget)
        {
           // QMouseEvent *mouseEvent = static_cast <QMouseEvent *> (event);
           down = false;
        }
        return true;
    }
    else if (type == QEvent::ContextMenu)
    {
        return true; // QLineEdit, QTextEdit
    }
    else
    {
        // if (obj != widget) return true;
        // return false;
        // standard event processing
        return QObject::eventFilter (obj, event);
    }
}

/* ---------------------------------------------------------------------- */

QWidget * createWidget (QString name)
{
    QWidget * result = NULL;
    if (name == "QPushButton")
       result = new QPushButton ("button");
    else if (name == "QCheckBox")
       result = new QCheckBox ("checkbox");
    else if (name == "QLineEdit")
       result = new QLineEdit;
    else if (name == "QTextEdit")
       result = new QTextEdit;
    else if (name == "QTreeWidget")
       result = new QTreeWidget;
    else if (name == "QTableWidget")
       result = new QTableWidget;
    return result;
}

void addFilter (QObject * target, QObject * adapter)
{
    target->installEventFilter (adapter);
    for (QObject * item : target->children ())
        addFilter (item, adapter);
}

void setupWidget (QWidget * widget, int x, int y, int w, int h, QWidget * parent)
{
    widget->setParent (parent);
    widget->setGeometry (x, y, w, h);
    // widget->installEventFilter (new Adapter (widget, parent));
    widget->setVisible (true);
    addFilter (widget, new Adapter (widget, parent));
}

/* ---------------------------------------------------------------------- */

Builder::Builder (QWidget * parent) :
   QWidget (parent)
{
   setAcceptDrops (true); // important
}

void Builder::dragEnterEvent(QDragEnterEvent * event)
{
    const QMimeData * data = event->mimeData();
    if (data->hasColor() || data->hasFormat (widgetFormat) )
    {
       event->acceptProposedAction(); // important
       // setText ("color");
       // cout << "dragEnter" << endl;
    }
}

void Builder::dropEvent(QDropEvent * event)
{
    const QMimeData * mime = event->mimeData();
    if (mime->hasColor())
    {
       QColor color = mime->colorData().value <QColor> ();
       // QColor color = mime->colorData(); // !?
    }
    if (mime->hasFormat (widgetFormat))
    {
        QString tool = QString (mime->data (widgetFormat));
        QPoint p = event->pos ();
        QWidget * widget = createWidget (tool);
        if (widget != NULL)
        {
           setupWidget (widget, p.x (), p.y (), 100, 80, this);
           // refreshTree ();
        }
    }
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
