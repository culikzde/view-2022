
/* ---------------------------------------------------------------------- */

struct Item
{
   public :
      QString text;

      // string value;
      QIcon     icon;
      QColor    color;
      bool      selected;
      long      size;
      QDateTime time;

      Item * up;
      QList <Item *> subitems;
};

/* ---------------------------------------------------------------------- */

TreeModule
{
   Record = "Item";

   name = text;
   above = up;
   items = subitems;

   root = storage;
}

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
   public:

      #if 1
      Item * top
      {
          text = "top level";
          Item * branch
          {
             text = "branch";
             up = top;
             Item * leaf
             {
                text = "leaf";
                up = branch; // important
             }
             subitems.append (leaf);
             Item * leaf2
             {
                text = "leaf 2";
                up = branch;
             }
             subitems.append (leaf2);
          }
          subitems.append (branch);
      }
      #endif

      #if 1
      QTreeView * treeView { }
      #endif

      Window (QWidget * parent = null) :
         QMainWindow (parent)
      {
          #if 0
          Item * top = new Item;
          top->text = "top level";

          Item * branch = new Item;
          branch->text = "branch";
          top->subitems.append (branch);
          branch->up = top;

          for (int i = 1; i <= 3; i++)
          {
             Item * leaf = new Item;
             leaf->text = "leaf " + QString::number (i);
             branch->subitems.append (leaf);
             leaf->up = branch;
          }
          #endif

          #if 0
          QTreeView * treeView = new QTreeView (this);
          setCentralWidget (treeView);
          #endif

          TreeModel * model = new TreeModel (this, top);
          treeView->setModel (model);
      }
};

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
    QApplication * appl = new QApplication (argc, argv);
    Window * window = new Window;
    window->show ();
    appl->exec ();
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
