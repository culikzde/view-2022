
/* ---------------------------------------------------------------------- */

struct Item
{
    QString   text;
    int       size;
    /*
    QIcon     icon;
    QColor    color;
    bool      selected;
    QDateTime time;
    */
};

struct Queue
{
    QList <Item *> subitems;
};

/* ---------------------------------------------------------------------- */

TableModule
{
   Record = Item;

   Collection = Queue;
   items = subitems;

   store = storage;
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
     QApplication * appl = new QApplication (argc, argv);

     Queue data;

     for (int i = 1; i <= 7; i++)
     {
        Item * item = new Item;
        item->text = "item " + QString::number (i);
        item->size = i;
        data.subitems.append (item);
     }

     TableView * window = new TableView (NULL, &data);
     window->show ();
     appl->exec ();
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
