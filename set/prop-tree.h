#ifndef PROPERTY_TREE_H
#define PROPERTY_TREE_H

#include <QTreeWidget>
#include <QStyledItemDelegate>

#include <QCheckBox>
#include <QComboBox>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QPushButton>

/* ---------------------------------------------------------------------- */

class PropertyTree : public QTreeWidget
{
Q_OBJECT

protected:
    QTreeWidgetItem * addTreeItem (QString name);

public:
    QTreeWidgetItem * treeItem (const QModelIndex & index) { return itemFromIndex (index); }

public:
    PropertyTree (QWidget * parent = nullptr);

public :
   virtual void store () { }

public slots:
    void onItemChanged (QTreeWidgetItem * item) { store (); }
};

/* ---------------------------------------------------------------------- */

class CustomEditor : public QWidget
{
public:
      bool enable_checkbox;
      bool enable_list;
      bool enable_text;
      bool enable_numeric;
      bool enable_real;
      bool enable_dialog;

      QStringList list_values;

      QCheckBox * check_box;
      QLineEdit * line_edit;
      QSpinBox * numeric_edit;
      QDoubleSpinBox * real_edit;
      QComboBox * combo_box;
      QPushButton * button;

      QTreeWidgetItem * node;

      void onDialogClick ();

      CustomEditor (QWidget * parent) :
            QWidget (parent),
            enable_checkbox (false),
            enable_list (false),
            enable_text (false),
            enable_numeric (false),
            enable_real (false),
            enable_dialog (false),

            check_box (nullptr),
            line_edit (nullptr),
            numeric_edit (nullptr),
            real_edit (nullptr),
            combo_box (nullptr),
            button (nullptr),

            node (nullptr)
      { }
};

/* ---------------------------------------------------------------------- */

class CustomDelegate : public QStyledItemDelegate
{
   public:
      virtual QWidget * createEditor
         (QWidget * parent,
          const QStyleOptionViewItem & option,
          const QModelIndex & index) const override;

      virtual void setEditorData
         (QWidget * param_editor,
          const QModelIndex & index) const override;

      virtual void setModelData
         (QWidget * param_editor,
          QAbstractItemModel * model,
          const QModelIndex & index) const override;

    /*
    virtual void updateEditorGeometry
       (QWidget * param_editor,
        const QStyleOptionViewItem & option,
        const QModelIndex &index) const override;

    virtual QSize sizeHint
       (const QStyleOptionViewItem & option,
        const QModelIndex & index) const override;
    */

    PropertyTree * tree;

    CustomDelegate (PropertyTree * p_tree) :
       tree  (p_tree)
       {  }
};

#endif // PROPERTY_TREE_H

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
