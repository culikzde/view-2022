
// #include <Qt3DCore/QEntity>
// #include <Qt3DCore/QTransform>

// #include <Qt3DRender/qrenderaspect.h>
// #include <Qt3DRender/QCamera>
// #include <Qt3DRender/QMaterial>

// #include <Qt3DExtras/QTorusMesh>
// #include <Qt3DExtras/QCuboidMesh>
// #include <Qt3DExtras/QSphereMesh>
// #include <Qt3DExtras/QConeMesh>
// #include <Qt3DExtras/QCylinderMesh>

// #include <Qt3DExtras/QPhongMaterial>

// #include <Qt3DExtras/QOrbitCameraController>
// #include <Qt3DExtras/QFirstPersonCameraController>
// #include <Qt3DExtras/Qt3DWindow>


typedef double qreal;
// class QAbstractGraphicsShapeItem;
// class QGraphicsRectItem;

using namespace Qt3DCore;
using namespace Qt3DRender;
using namespace Qt3DExtras;

/*
class QEntity;
class QPhongMaterial;
class QCuboidMesh;
class QSphereMesh;
class QTorusMesh;
class QCylinderMesh;
class Qt3DWindow;
class QCamera;
*/

/* ---------------------------------------------------------------------- */

// https://vicrucann.github.io/tutorials/qt3d-cmake/

void addMark (QEntity * root,  qreal x, qreal y, qreal z, QColor color, qreal r = 5)
{

    QEntity * torus = new QEntity (root);

    QTorusMesh * mesh = new QTorusMesh;
    mesh->setRadius (r);
    mesh->setMinorRadius (r/5);

    QTransform * transform = new QTransform;
    transform->setTranslation (QVector3D (x, y, z));

    QPhongMaterial * material = new QPhongMaterial (torus);
    material->setAmbient (color);

    torus->addComponent (mesh);
    torus->addComponent (transform);
    torus->addComponent (material);
}

const qreal step = -40;

void addShape (QEntity * root,  QGraphicsItem * item, qreal x0, qreal y0, qreal z)
{
    qreal x = x0 + item->x();
    qreal y = y0 + item->y();

    QString pen = "";
    QString brush = "";

    QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item);
    if (shape)
    {
        QColor pen =  shape->pen().color();
        QColor brush = shape->brush().color();

        if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
        {
            QRectF r = e->rect ();
            qreal w = r.width ();
            qreal h = r.height();

            QEntity * entity = new QEntity (root);

            QCuboidMesh * mesh = new QCuboidMesh ();
            mesh->setXExtent (w);
            mesh->setYExtent (h);
            mesh->setZExtent (5);

            QTransform * transform = new QTransform;
            transform->setTranslation (QVector3D (x+w/2, y+h/2, z));

            QPhongMaterial * material = new QPhongMaterial (entity);
            material->setAmbient (brush);

            entity->addComponent (mesh);
            entity->addComponent (transform);
            entity->addComponent (material);

            /*
            addMark (root, x,   y,   z, "red");
            addMark (root, x,   y+h, z, "red");
            addMark (root, x+w, y,   z, "red");
            addMark (root, x+w, y+h, z, "red");
            */
        }

        if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
        {
            QRectF r = e->rect ();
            qreal w = r.width ();
            qreal h = r.height();

            QEntity * entity = new QEntity (root);

            QSphereMesh * mesh = new QSphereMesh ();
            mesh->setRadius (w/2);

            QTransform * transform = new QTransform;
            transform->setTranslation (QVector3D (x+w/2, y+w/2, z));

            QPhongMaterial * material = new QPhongMaterial (entity);
            material->setAmbient (brush);

            entity->addComponent (mesh);
            entity->addComponent (transform);
            entity->addComponent (material);

            // addMark (root, x+w/2, y+w/2, step, "red", w/2);
        }
    }

    if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (item))
    {
        QColor color = e->pen().color ();
        qreal w = e->line().dx();
        qreal h = e->line().dy();

        qreal length = sqrt (w*w + h*h);

        qreal angle = 0;
        if (length != 0)
            angle = asin (w/length) / M_PI * 180.0;
        if (h < 0)
           angle = 180-angle;

        QEntity * entity = new QEntity (root);

        QCylinderMesh * mesh = new QCylinderMesh ();
        mesh->setRadius (2);
        mesh->setLength (length);


        QVector3D point (x, y, z-step);
        QVector3D axis (0, 0, 1);
        QMatrix4x4 m;
        m.translate (0, length/2, 0);
        m.translate (point);
        QMatrix4x4 n = QTransform::rotateAround (point, -angle, axis);
        m = n * m;

        QTransform * transform = new QTransform;
        transform->setMatrix(m);
        // transform->setRotation (QQuaternion::fromAxisAndAngle (QVector3D (0,0,1), -angle));
        // transform->setTranslation (QVector3D (x+length/2, y, z-step));

        QPhongMaterial * material = new QPhongMaterial (entity);
        material->setAmbient (color);

        entity->addComponent (mesh);
        entity->addComponent (transform);
        entity->addComponent (material);
    }

    for (QGraphicsItem * t : item->childItems())
        addShape (root, t, x, y, z+step);
}

QWidget * createQt3DWindow (QWidget * parent)
{
    QEntity * root = new QEntity;

    QTransform * globalTransform = new QTransform;
    globalTransform->setRotation (QTransform::fromAxisAndAngle (QVector3D (1,0,0), 140.0f));
    globalTransform->setScale (0.1);
    root->addComponent (globalTransform);

    if (false)
    {
        int x = 40;
        int y = 100;
        int w = 200;
        int h = 100;
        addMark (root, x,     y, 0, "blue");
        addMark (root, x+w,   y, 0, "red");
        addMark (root, x,   y+h, 0, "green");
        addMark (root, x+w, y+h, 0, "orange");
    }

    if (0)
    {
        QEntity * torus = new QEntity (root);

        QTorusMesh * mesh = new QTorusMesh;
        mesh->setRadius (50);
        mesh->setMinorRadius (10);
        mesh->setRings (100);
        mesh->setSlices (20);

        QTransform * transform = new QTransform;
        transform->setRotation (QQuaternion::fromAxisAndAngle (QVector3D(1,0,0), 45.f ));

        QPhongMaterial * material = new QPhongMaterial (torus);
        material->setAmbient (QColor ("blue"));

        torus->addComponent (mesh);
        torus->addComponent (transform);
        torus->addComponent (material);
    }


    QGraphicsScene * scene = getScene ();
    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
        if (item->parentItem() == nullptr)
            addShape (root, item, 0, 0, 0);

    // window
    Qt3DWindow * view = new Qt3DWindow;
    view->setRootEntity (root);

    // camera
    QCamera * camera = view->camera ();
    camera->lens()->setPerspectiveProjection (45.0f, 16.0f/9.0f, 0.1f, 1000.0f);
    camera->setPosition (QVector3D (0, 0, 40.0f));
    camera->setViewCenter (QVector3D (0, 0, 0));

    // manipulator
    QOrbitCameraController* manipulator = new QOrbitCameraController (root);
    manipulator->setLinearSpeed (50.f);
    manipulator->setLookSpeed (180.f);
    manipulator->setCamera (camera);
    /*
    QFirstPersonCameraController * manipulator = new QFirstPersonCameraController (root);
    manipulator->setLinearSpeed (50.f);
    manipulator->setLookSpeed (180.f);
    manipulator->setCamera (camera);
    */

    return QWidget::createWindowContainer (view, parent);

    // https://doc.qt.io/qt-5/qwidget.html#createWindowContainer
    // The window container does not interoperate with QGraphicsProxyWidget
}

/* ---------------------------------------------------------------------- */
