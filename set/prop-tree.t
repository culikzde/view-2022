
/* prop-tree.t */

/* ---------------------------------------------------------------------- */

namespace plastic
{
   class Record { };

   typedef QString FileName;
   typedef QList <int> IntList;
   typedef QList <float> FloatList;
   typedef QList <double> DoubleList;
}

using namespace plastic;


namespace building
{

/* ---------------------------------------------------------------------- */

class BoolIO
{
    void display (QTreeWidgetItem * node, bool v)
    {
        node->setData (1, Qt::EditRole, v);
        node->setData (1, Qt::UserRole, "bool");
    }

    void store (QTreeWidgetItem * node, bool & v)
    {
        v = node->data (1, Qt::EditRole).var.toBool ();
    }
};

class IntIO
{
    void display (QTreeWidgetItem * node, int v)
    {
        node->setData (1, Qt::EditRole, v);
        node->setData (1, Qt::UserRole, "int");
    }

    void store (QTreeWidgetItem * node, int & v)
    {
        v = node->data (1, Qt::EditRole).var.toInt ();
    }
};

class DoubleIO
{
    void display (QTreeWidgetItem * node, double v)
    {
        node->setData (1, Qt::EditRole, v);
        node->setData (1, Qt::UserRole, "double");
    }

    void store (QTreeWidgetItem * node, double & v)
    {
        v = node->data (1, Qt::EditRole).var.toDouble ();
    }
};

class ColorIO
{
    void display (QTreeWidgetItem * node, QColor v)
    {
        node->setData (1, Qt::EditRole, v);
        node->setData (1, Qt::UserRole, "color");
    }

    void store (QTreeWidgetItem * node, QColor & v)
    {
        v = node->data (1, Qt::EditRole).var.value <QColor> ();
    }
};

class FontIO
{
    void display (QTreeWidgetItem * node, QFont v)
    {
        node->setData (1, Qt::EditRole, v);
        node->setData (1, Qt::UserRole, "font");
    }

    void store (QTreeWidgetItem * node, QFont & v)
    {
        v = node->data (1, Qt::EditRole).var.value <QFont> ();
    }
};

class StringIO
{
    void display (QTreeWidgetItem * node, QString v)
    {
        node->setText (1, v);
        // node->setData (1, Qt::UserRole, "string");
    }

    void store (QTreeWidgetItem * node, QString & v)
    {
        v = node->text (1);
    }
};

class FileNameIO
{
    void display (QTreeWidgetItem * node, QString v)
    {
        node->setText (1, v);
        node->setData (1, Qt::UserRole, "file_name");
    }

    void store (QTreeWidgetItem * node, QString & v)
    {
        v = node->text (1);
    }
};

class StringListIO
{
    void display (QTreeWidgetItem * node, QStringList v)
    {
        // node->setData (1, Qt::EditRole, v);
        // node->setData (1, Qt::UserRole, "string_list");

        int inx = 0;
        for (QString item : v)
        {
           QTreeWidgetItem * other = new QTreeWidgetItem (node);
           other->setText (0, QString::number (inx));
           other->setText (1, item);
           // other->setData (1, Qt::UserRole, "string");
           inx ++;
        }
    }

    void store (QTreeWidgetItem * node, QString & v)
    {
        int cnt = node->childCount ();
        for (int inx = 0; inx < cnt; inx++)
        {
           QTreeWidgetItem * other = node->child (inx);
           v [inx] = other->text (1);
        }
    }
};

/* ---------------------------------------------------------------------- */

} // end of namespace building

using namespace building;

namespace set
{

/* ---------------------------------------------------------------------- */

class Properties : public PropertyTree
{
public:
    Record * data;

    void init ()
    {
    }

    void display ()
    {
    }

    void store ()
    {
    }

    Properties (QWidget * parent, Record * p_data) :
       PropertyTree (parent),
       data (p_data)
    {
       init ();
       display ();
       show ();
    }
};

/* ---------------------------------------------------------------------- */

} // end of namespace set

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
