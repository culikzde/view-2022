/* area-example.t */

#include "color-button.t"
#include "tool-button.t"
#include "area.t"

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
    public:
       void openFile ();
       void saveFile ();

   windowTitle = "Area Example";
   // setWindowTitle ("Area Example");

   QMenuBar * mainMenu
   {
       QMenu * fileMenu
       {
           title = "&File";
           QAction * openMenuItem
           {
               text = "&Open...";
               shortcut = "Ctrl+O";
               triggered = openFile;
           }

           QAction * saveMenuItem
           {
               text = "&Save...";
               shortcut = "Ctrl+S";
               triggered { saveFile (); }
           }

           QAction * quitMenuItem
           {
               text = "&Quit";
               shortcut = "Ctrl+Q";
               triggered { this->close (); }
           }
       }
   }

   QToolBar * toolbar
   {
       QToolButton * selectButton
       {
           text = "select";
           icon = "window-new";
       }

       QToolButton * resizeButton
       {
           text = "resize";
           icon = "view-refresh";
       }

       QToolButton * connectButton
       {
           text = "connect";
           icon = "go-jump";
       }

       QTabWidget * palette
       {
           QToolBar * colorToolBar
           {
               // text = "Colors";
               setTabText (0, "Colors");
               setTabIcon (0, QIcon::fromTheme ("color-fill"));

               new ColorButton (colorToolBar, "red");
               new ColorButton (colorToolBar, "blue");
               new ColorButton (colorToolBar, "green");
               new ColorButton (colorToolBar, "yellow");
               new ColorButton (colorToolBar, "orange");
               new ColorButton (colorToolBar, "silver");
               new ColorButton (colorToolBar, "gold");
               new ColorButton (colorToolBar, "goldenrod");
               new ColorButton (colorToolBar, "lime");
               new ColorButton (colorToolBar, "lime green");
               new ColorButton (colorToolBar, "yellow green");
               new ColorButton (colorToolBar, "green yellow");
               new ColorButton (colorToolBar, "forest green");
               new ColorButton (colorToolBar, "coral");
               new ColorButton (colorToolBar, "cornflower blue");
               new ColorButton (colorToolBar, "dodger blue");
               new ColorButton (colorToolBar, "royal blue");
               new ColorButton (colorToolBar, "wheat");
               new ColorButton (colorToolBar, "chocolate");
               new ColorButton (colorToolBar, "peru");
               new ColorButton (colorToolBar, "sienna");
               new ColorButton (colorToolBar, "brown");
           }

           QToolBar * shapeToolBar
           {
               // text = "Shapes";
               new ToolButton (shapeToolBar, "area", "", shapeFormat);
               new ToolButton (shapeToolBar, "sub-area", "", shapeFormat);
               new ToolButton (shapeToolBar, "source", "", shapeFormat);
               new ToolButton (shapeToolBar, "target", "", shapeFormat);
               new ToolButton (shapeToolBar, "ellipse", "", shapeFormat);
           }
       }
   }

   QSplitter * vsplitter
   {
       orientation = Qt::Vertical;
       QSplitter * hsplitter
       {
          QTreeWidget * tree { }
          AreaView * area { }
          QTableWidget * prop { }

          setStretchFactor (0, 1);
          setStretchFactor (1, 4);
          setStretchFactor (2, 1);
       }
       QPlainTextEdit * info { }
       setStretchFactor (0, 3);
       setStretchFactor (1, 1);
   }

   QStatusBar * status
   {
       QLabel * lab
       {
           text = "status bar";
       }
   }

};

/* ---------------------------------------------------------------------- */

void Window::openFile ()
{
    QString fileName = QFileDialog::getOpenFileName (this, "Open text file");
    if (fileName != "")
    {
    }
}

void Window::saveFile ()
{
    QString fileName = QFileDialog::getSaveFileName (this, "Save text file");
    if (fileName != "")
    {
    }
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
   // QIcon::setThemeName ("oxygen");
   QApplication * appl = new QApplication (argc, argv);
   Window * win = new Window ();
   win->show ();
   appl->exec ();
}


// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
