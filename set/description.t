
/* dialog.t */


namespace plastic
{

/* ---------------------------------------------------------------------- */

class Basic
{
};

class Record
{
};

/* ---------------------------------------------------------------------- */

}

using namespace plastic;

namespace building
{

/* ---------------------------------------------------------------------- */

/* ---------------------------------------------------------------------- */

} // end of namespace building

using namespace building;

namespace set
{
/* ---------------------------------------------------------------------- */

/* Field Description */

class FieldDesc
{
public:
    QString name;
    virtual QVariant read (Basic * item) { return QVariant (); }
    virtual void write (Basic * item, QVariant value) { }

    virtual QString readText (Basic * item) { return read (item).toString (); }
    virtual QVariant decoration (Basic * item) { return QVariant (); }
};

class BoolFieldDesc : public FieldDesc
{
public:
    // void setupEditor (CustomEditor * editor) override { editor->enable_checkbox = true; }
};

class IntFieldDesc : public FieldDesc
{
public:
    // void setupEditor (CustomEditor * editor) override { editor->enable_numeric = true; }
};

class DoubleFieldDesc : public FieldDesc
{
public:
    // void setupEditor (CustomEditor * editor) override { editor->enable_real = true; }
};

class StringFieldDesc : public FieldDesc
{
public:
    // void setupEditor (CustomEditor * editor) override { editor->enable_text = true; }
};

class ColorFieldDesc : public FieldDesc
{
public:
    // virtual QString readText (Basic * item) override;
    virtual QVariant decoration (Basic * item) override { return read (item); }
};

/* Type Description */

class TypeDesc
{
public:
    QString name;
    QList < FieldDesc * > fields;
    void add (FieldDesc * field) { fields.append (field); }
    FieldDesc * findField (QString field_name);

    virtual Basic * createInstance () { return null; }
};

/* Description */

class RecordDesc : public TypeDesc
{
public:
    RecordDesc ()
    {
    }

    Basic * createInstance () { return new Record; }
};

/* ---------------------------------------------------------------------- */

} // end of namespace set

using namespace set;

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
