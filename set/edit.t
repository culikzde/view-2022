
/* edit.t */

// #include <QTextEdit>
// #include <QCompleter>
// #include <QAction>

// #include <QHBoxLayout>
// #include <QLabel>
// #include <QComboBox>
// #include <QSpinBox>
// #include <QCheckBox>
// #include <QToolButton>
// #include <QPushButton>

// #include <QKeyEvent>
// #include <QAbstractItemView>
// #include <QSyntaxHighlighter>
// #include <QMenu>
// #include <QScrollBar>
// #include <QToolTip>
// #include <QLineEdit>

// #include <QFileDialog>
// #include <QTextStream>

#define Q_OBJECT
#define slots

class QString;
template <class T> class QList;
class ExtraSelection;

/* ---------------------------------------------------------------------- */

class Bookmark
{
   public:
      int line;
      int column;
      int mark;

      Bookmark () : line (0), column (0), mark (0) { }
};

typedef QList <Bookmark> BookmarkList;

class FindBox;
class GoToLineBox;

/* ---------------------------------------------------------------------- */

class Edit : public QTextEdit
{
   Q_OBJECT

   public:
      explicit Edit (QWidget * parent);
      // virtual ~ Edit ()

   public:
      void select (int line, int column = 0, int endLine = 0, int endColumn = 0);

   protected:
      void keyPressEvent (QKeyEvent *e) override;
      void focusInEvent (QFocusEvent *e) override;

   private slots:
      void insertCompletion (const QString &completion);

   private:
      QCompleter * completer;

   private:
      void setCompleter (QCompleter * c);
      QString textUnderCursor() const;

   private:

      #if 0
      QString getFileName ();
      QString getCommentMark ();
      QString linesInSelection ();
      void comment ();
      void uncomment ();
      void indent ();
      void unindent ();
      #endif

      void keyPressEvent2 (QKeyEvent * e);
      void setBookmark (int markType = 1);
      void gotoBookmark (int markType = 1);
      void gotoPrevBookmark (int markType=1);
      void gotoNextBookmark (int markType=1);
      void clearBookmarks ();
      BookmarkList getBookmarks ();
      void setBookmarks (BookmarkList bookmarks);


   private:
      QTextCursor lastCursor;
      bool lastUnderline;
      QColor lastUnderlineColor;
      bool closeWithoutQuestion;

      FindBox * findBox;
      GoToLineBox * lineBox;
      QString origFileName;
      int origTimeStamp;
      QString origText;

      void init ();
      void showStatus (QString text);
      void onCursorPositionChanged ();
      void selectLineByPosition (int pos);
      void select2 (int start_line, int start_col, int stop_line, int stop_col);
      bool event (QEvent * e) override;
      void mouseMoveEvent (QMouseEvent * e) override;
      void mousePressEvent (QMouseEvent * e) override;
      void jumpToObject (QObject * obj);
      void * findGlobalData ();
      void * findIdentifier (QString qual_name);
      QString findContext (QTextCursor cursor);
      void displayProperty (QMenu * menu, QTextCharFormat format, QString title, int property);
      void contextMenuEvent (QContextMenuEvent * e) override;
      QStringList getCompletionList ();
      QString getCursorInfo ();
      void showMemo (QString name="");
      void showReferences (QString name="");
      void gotoNextFunction ();
      void gotoPrevFunction ();
      void gotoBegin ();
      void gotoEnd ();
      void gotoPageUp ();
      void gotoPageDown ();
      void scrollUp ();
      void scrollDown ();
      void scrollLeft ();
      void scrollRight ();
      void enlargeFont ();
      void shrinkFont ();
      void moveLines (bool up);
      void moveLinesUp ();
      void moveLinesDown ();

   public:
      void selectLine (int line);

   private:
      void init2 ();
      void findText ();
      void findNext ();
      void findPrev ();
      void replaceText ();
      void findSelected ();
      void findIncremental ();
      void goToLine ();
      QString getFileName ();
      void simpleReadFile (QString fileName);
      QString readFileData (QString fileName);
      void readFile (QString fileName);
      void writeFile (QString fileName);
      bool isModified ();
      bool isModifiedOnDisk ();
      void openFile ();
      void saveFile ();
      void saveFileAs ();
};

/* ---------------------------------------------------------------------- */

class FindBox: public QWidget
{
   Q_OBJECT

   private:
      QHBoxLayout * horiz_layout;
      QToolButton * closeButton;
      QLabel * label;
      QToolButton * clearButton;
      QCheckBox * wholeWords;
      QCheckBox * matchCase;
      QPushButton * nextButton;
      QPushButton * prevButton;

   private:
      bool small;
      QColor red;
      QColor green;
      int start;

   public:
      QComboBox * line;
      Edit * edit;
      bool incremental;

   public slots:
      // void hide ();
      void returnPressed ();
      void textChanged (QString text);
      void findNext ();
      void findPrev ();

   public:
      FindBox (QWidget * parent = NULL);
      void open ();

      void resizeEvent (QResizeEvent * e) override;
      void findStep (bool back);
};

class GoToLineBox: public QWidget
{
   Q_OBJECT

   private:
      QHBoxLayout * horiz_layout;
      QToolButton * closeButton;
      QLabel * label;
      QSpinBox * line;
      QPushButton * button;

   public:
      Edit * edit;

   public slots:
      void editingFinished ();
      // void hide ();

   public:
      GoToLineBox (QWidget * parent = NULL);
      void open ();
};

/* ---------------------------------------------------------------------- */

struct HighlightingRule
{
   QRegExp pattern;
   QTextCharFormat format;
};

class Highlighter : public QSyntaxHighlighter
{
   // Q_OBJECT

   public:
      Highlighter (QTextDocument *parent = 0);

   protected:
      void highlightBlock (const QString & text) override;
      void highlightBlock2 (const QString & text);

   private:
      QVector<HighlightingRule> highlightingRules;

      QRegExp commentStartExpression;
      QRegExp commentEndExpression;

      QTextCharFormat keywordFormat;
      QTextCharFormat classFormat;
      QTextCharFormat singleLineCommentFormat;
      QTextCharFormat multiLineCommentFormat;
      QTextCharFormat quotationFormat;
      QTextCharFormat functionFormat;
};

Highlighter::Highlighter (QTextDocument *parent)
      : QSyntaxHighlighter (parent)
{
   HighlightingRule rule;

   keywordFormat.setForeground (Qt::blue);
   keywordFormat.setFontWeight (QFont::Bold);

   QStringList keywordPatterns;
   keywordPatterns << "char" << "class" << "const"
   << "double" << "enum" << "explicit"
   << "friend" << "inline" << "int"
   << "long" << "namespace" << "operator"
   << "private" << "protected" << "public"
   << "short" << "signals" << "signed"
   << "slots" << "static" << "struct"
   << "template" << "typedef" << "typename"
   << "union" << "unsigned" << "virtual"
   << "void" << "volatile"
   << "function" << "for";

   for (const QString & pattern : keywordPatterns)
   {
      rule.pattern = QRegExp ("\\b" + pattern + "\\b");
      rule.format = keywordFormat;
      highlightingRules.append (rule);
   }

   classFormat.setFontWeight (QFont::Bold);
   classFormat.setForeground (QColor ("darkorange"));
   rule.pattern = QRegExp ("\\bQ[A-Za-z]+\\b");
   rule.format = classFormat;
   highlightingRules.append (rule);

   singleLineCommentFormat.setForeground (Qt::red);
   rule.pattern = QRegExp ("//[^\n]*");
   rule.format = singleLineCommentFormat;
   highlightingRules.append (rule);

   multiLineCommentFormat.setForeground (Qt::red);

   quotationFormat.setForeground (Qt::darkGreen);
   rule.pattern = QRegExp ("\".*\"");
   rule.format = quotationFormat;
   highlightingRules.append (rule);

   functionFormat.setFontItalic (true);
   functionFormat.setForeground (QColor ("coral"));
   rule.pattern = QRegExp ("\\b[A-Za-z0-9_]+(?=\\()");
   rule.format = functionFormat;
   highlightingRules.append (rule);

   commentStartExpression = QRegExp ("/\\*");
   commentEndExpression = QRegExp ("\\*/");
}

void Highlighter::highlightBlock (const QString &text)
{
   for (const HighlightingRule & rule : highlightingRules)
   {
      QRegExp expression (rule.pattern);
      int index = expression.indexIn (text);
      while (index >= 0)
      {
         int length = expression.matchedLength ();
         setFormat (index, length, rule.format);
         index = expression.indexIn (text, index + length);
      }
   }
   setCurrentBlockState (0);

   int startIndex = 0;
   if (previousBlockState () != 1)
      startIndex = commentStartExpression.indexIn (text);

   while (startIndex >= 0)
   {
      int endIndex = commentEndExpression.indexIn (text, startIndex);
      int commentLength;
      if (endIndex == -1)
      {
         setCurrentBlockState (1);
         commentLength = text.length () - startIndex;
      }
      else
      {
         commentLength = endIndex - startIndex
                         + commentEndExpression.matchedLength ();
      }
      setFormat (startIndex, commentLength, multiLineCommentFormat);
      startIndex = commentStartExpression.indexIn (text, startIndex + commentLength);
   }
}

/* ---------------------------------------------------------------------- */

#if 0
void Highlighter::highlightBlock2 (const QString & text)
{
    // if (!enabled)
    //  return;

    bool use_cursor = true; // enable_info_property;
    QTextCursor cursor;
    int cursor_inx = 0;
    if (use_cursor)
    {
        cursor = QTextCursor (currentBlock ());
        cursor_inx = 0;
    }
    int cnt = text.length ();
    int inx = 0;
    bool inside_comment = previousBlockState () == 1;
    int start_comment = 0;

    while (inx < cnt)
    {
        if (inside_comment)
        {
            if (inx == 0)
                inx = 1;
            while (inx < cnt && (text[inx - 1] != '*' || text[inx] != '/')) inx++;
            if (inx < cnt)
            {
                inx = inx + 1;
                setFormat (start_comment, inx - start_comment, commentFormat);
                inside_comment = false;
            }
        }
        else
        {
            while (inx < cnt && text[inx] <= ' ') inx ++;
            int start = inx;
            if (inx < cnt)
            {
                char c = text[inx];
                if (use_cursor)
                {
                    cursor.movePosition (QTextCursor::NextCharacter, QTextCursor::MoveAnchor, inx + 1 - cursor_inx);
                    cursor_inx = inx + 1;
                    QTextBlockFormat fmt = cursor.charFormat ();
                }
                if (isLetter (c))
                {
                    while (inx < cnt && isLetterOrDigit (text[inx])) inx ++;
                    if (use_cursor && fmt.hasProperty (infoProperty))
                    else if (c == 'Q')
                        setFormat (start, inx - start, qidentifierFormat);
                    else
                        setFormat (start, inx - start, identifierFormat);
                }
                else if (isDigit (c))
                {
                    while (inx < cnt && isDigit (text[inx]))
                        inx = inx + 1;
                    if (use_cursor && fmt.hasProperty (infoProperty))
                        // nothing
                    else
                        setFormat (start, inx - start, numberFormat);
                }
                else if (c == '"')
                {
                    inx = inx + 1;
                    while (inx < cnt && text[inx] != '"') inx ++;
                    inx = inx + 1;
                    if (use_cursor && fmt.hasProperty (infoProperty))
                        // nothing
                    else
                        setFormat (start, inx - start, stringFormat);
                }
                else if (c == "'")
                {
                    inx = inx + 1;
                    while (inx < cnt && text[inx] != "'")
                        inx = inx + 1;
                    inx = inx + 1;
                    if (use_cursor && fmt.hasProperty (infoProperty))
                        // nothing
                    else
                        setFormat (start, inx - start, characterFormat);
                }
                else if (c == '/')
                {
                    inx = inx + 1;
                    if (inx < cnt && text[inx] == '/')
                    {
                        inx = cnt;
                        setFormat (start, inx - start, commentFormat);
                    }
                    else if (inx < cnt && text[inx] == '*')
                    {
                        inx = inx + 1;
                        inside_comment = true;
                        start_comment = inx - 2;
                    }
                    else if (use_cursor && fmt.hasProperty (infoProperty))
                        // nothing
                    else
                        setFormat (start, inx - start, separatorFormat);
                }
                else
                {
                    inx = inx + 1;
                    if (use_cursor && fmt.hasProperty (infoProperty))
                        // nothing
                    else
                        setFormat (start, inx - start, separatorFormat);
                }
            }
        }
    }

    if (inside_comment)
    {
        setFormat (start_comment, inx - start_comment, commentFormat);
        setCurrentBlockState (1);
    }
    else
        setCurrentBlockState (0);
}
#endif

/* ---------------------------------------------------------------------- */

void Edit::setCompleter (QCompleter * c)
{
   if (completer)
      QObject::disconnect (completer, 0, this, 0);

   completer = c;

   if (!completer)
      return;

   completer->setWidget (this);
   completer->setCompletionMode (QCompleter::PopupCompletion);
   completer->setCaseSensitivity (Qt::CaseInsensitive);


   // !?
   // QObject::connect (completer, SIGNAL (activated (const QString&)),
   //                  this, SLOT (insertCompletion (const QString&)));
}

void Edit::insertCompletion (const QString& completion)
{
   if (completer->widget () != this)
      return;
   QTextCursor tc = textCursor ();
   int extra = completion.length () - completer->completionPrefix ().length ();
   tc.movePosition (QTextCursor::Left);
   tc.movePosition (QTextCursor::EndOfWord);
   tc.insertText (completion.right (extra));
   setTextCursor (tc);
}

QString Edit::textUnderCursor () const
{
   QTextCursor tc = textCursor ();
   tc.select (QTextCursor::WordUnderCursor);
   return tc.selectedText ();
}

void Edit::focusInEvent (QFocusEvent *e)
{
   if (completer)
      completer->setWidget (this);
   QTextEdit::focusInEvent (e);
}

void Edit::keyPressEvent (QKeyEvent *e)
{
    keyPressEvent2 (e);

   if (completer && completer->popup ()->isVisible ())
   {
      // The following keys are forwarded by the completer to the widget
      switch (e->key ())
      {
         case Qt::Key_Enter: ;


         case Qt::Key_Return:
         case Qt::Key_Escape:
         case Qt::Key_Tab:
         case Qt::Key_Backtab:
            e->ignore ();
            return; // let the completer do default behavior
         default:
            break;
      }
   }

   bool isShortcut = ( (e->modifiers () & Qt::ControlModifier) && e->key () == Qt::Key_E); // CTRL+E
   if (!completer || !isShortcut) // dont process the shortcut when we have a completer
      QTextEdit::keyPressEvent (e);

   const bool ctrlOrShift = e->modifiers () & (Qt::ControlModifier | Qt::ShiftModifier);
   if (!completer || (ctrlOrShift && e->text ().isEmpty ()))
      return;

   static QString eow ("~!@#$%^&*()_+{}|:\"<>?,./;'[]\\-="); // end of word
   bool hasModifier = (e->modifiers () != Qt::NoModifier) && !ctrlOrShift;
   QString completionPrefix = textUnderCursor ();

   if (!isShortcut && (hasModifier || e->text ().isEmpty () || completionPrefix.length () < 3
                       || eow.contains (e->text ().right (1))))
   {
      completer->popup ()->hide ();
      return;
   }

   if (completionPrefix != completer->completionPrefix ())
   {
      completer->setCompletionPrefix (completionPrefix);
      completer->popup ()->setCurrentIndex (completer->completionModel ()->index (0, 0));
   }
   QRect cr = cursorRect ();
   cr.setWidth (completer->popup ()->sizeHintForColumn (0)
                + completer->popup ()->verticalScrollBar ()->sizeHint ().width ());
   completer->complete (cr); // popup it up!
}

/* ---------------------------------------------------------------------- */

Edit::Edit (QWidget * parent) :
   QTextEdit (parent),
   completer (NULL)
{
   setLineWrapMode (QTextEdit::NoWrap); // otherwise line numbers are different

   setMouseTracking (true);

   Highlighter * highlighter = new Highlighter (document ());

   QStringList wordList;
   wordList << "alpha" << "beta" << "gamma";

   QCompleter * completer = new QCompleter (wordList, this);
   completer->setModelSorting (QCompleter::CaseInsensitivelySortedModel);
   completer->setCaseSensitivity (Qt::CaseInsensitive);
   completer->setWrapAround (false);
   this->setCompleter (completer);

   append ("// comment");
   append ("/* comment */");
   append ("");
   append ("module M;");
   append ("");
   append ("class Hello");
   append ("{");
   append ("   private:");
   append ("      // int x, y, z;");
   append ("   public:");
   append ("      void fce ();");
   append ("};");

   init ();
   init2 ();
}

/* ---------------------------------------------------------------------- */

void Edit::select (int line, int column, int endLine, int endColumn)
{
   if (line > 0)
   {
       QTextCursor cursor = this->textCursor ();

       cursor.movePosition (QTextCursor::Start);
       cursor.movePosition (QTextCursor::QTextCursor::Down, QTextCursor::MoveAnchor, line-1);

       if (column > 0)
          cursor.movePosition (QTextCursor::QTextCursor::Right, QTextCursor::MoveAnchor, column-1);

       if (endLine >= line) // select text area
       {
          cursor.movePosition (QTextCursor::QTextCursor::Down, QTextCursor::KeepAnchor, endLine-line);

          if (endColumn > 0)
          {
             if (endLine == line)
             {
                cursor.movePosition (QTextCursor::QTextCursor::Right, QTextCursor::KeepAnchor, endColumn-column);
             }
             else if (endLine > line)
             {
                cursor.movePosition (QTextCursor::QTextCursor::StartOfLine, QTextCursor::KeepAnchor);
                cursor.movePosition (QTextCursor::QTextCursor::Right, QTextCursor::KeepAnchor, endColumn);
             }
          }
          else
          {
              // no endColumn, select to the end of line
              cursor.movePosition (QTextCursor::QTextCursor::EndOfLine, QTextCursor::KeepAnchor);
          }
       }

       this->setTextCursor (cursor);
       this->ensureCursorVisible ();
   }
}

/* ---------------------------------------------------------------------- */

#if 0
QString Edit::getFileName ()
{
    return "";
}

QString Edit::getCommentMark ()
{
    (name, ext) = os.path.splitext (getFileName ());
    mark = "";
    if (ext in [".py", ".sh"])
        mark = "#";
    else
        mark = "//";
    return mark;
}

QString Edit::linesInSelection ()
{
    cursor = textCursor ();
    cursor.beginEditBlock ();
    if (cursor.hasSelection ())
    {
        start = cursor.selectionStart ();
        stop = cursor.selectionEnd ();
        cursor.setPosition (stop);
        stop_line = cursor.blockNumber ();
        cursor.setPosition (start);
        start_line = cursor.blockNumber ();
        for (n in range (stop_line - start_line + 1))
        {
            cursor.movePosition (QTextCursor::StartOfLine);
            yield cursor;
            cursor.movePosition (QTextCursor::NextBlock);
        }
    }
    else
    {
        cursor.movePosition (QTextCursor::StartOfLine);
        yield cursor;
    }
    cursor.endEditBlock ();
}

void Edit::comment ()
{
    QString mark = getCommentMark () + " ";
    for (cursor in linesInSelection ())
        cursor.insertText (mark);
}

void Edit::uncomment ()
{
    mark = getCommentMark ();
    for (cursor in linesInSelection ())
    {
        line = cursor.block ().text ();
        inx = 0;
        cnt = len (line);
        while (inx < cnt && line[inx] <= ' ')
        {
            cursor.movePosition (QTextCursor::NextCharacter);
            inx = inx + 1;
        }
        m = len (mark);
        if (inx + m <= cnt && line[inx:inx + m] == mark)
        {
            for (i in range (m))
                cursor.deleteChar ();
            inx = inx + m;
            if (inx < cnt && line[inx] == ' ')
                cursor.deleteChar ();
        }
    }
}

void Edit::indent ()
{
    delta = 1;
    for (cursor in linesInSelection ())
        cursor.insertText (" " * delta);
}

void Edit::unindent ()
{
    delta = 1;
    for (cursor in linesInSelection ())
    {
        line = cursor.block ().text ();
        if (line.startsWith ('\t'))
        {
            cursor.deleteChar ();
            cursor.insertText (" " * delta - 1);
        }
        else
            for (char in line[:delta])
            {
                if (char != ' ')
                    break;
                cursor.deleteChar ();
            }
    }
}
#endif

/* ---------------------------------------------------------------------- */

const int bookmarkProperty = QTextFormat::UserProperty;
const int locationProperty = QTextFormat::UserProperty + 1; // source location in output window
const int infoProperty = QTextFormat::UserProperty + 2;
const int defnProperty = QTextFormat::UserProperty + 3;
const int openProperty = QTextFormat::UserProperty + 4;
const int closeProperty = QTextFormat::UserProperty + 5;
const int outlineProperty = QTextFormat::UserProperty + 6;

const QColor bookmark_colors [5] =
             { QColor (Qt::yellow).lighter (160),
               QColor (Qt::green).lighter (160),
               QColor (Qt::blue).lighter (160),
               QColor (Qt::red).lighter (160),
               QColor (Qt::gray).lighter (140) };

void Edit::keyPressEvent2 (QKeyEvent * e)
{
    bool done = false;
    Qt::KeyboardModifiers modifiers = e->modifiers ();
    if (modifiers & Qt::KeypadModifier == 0)
    {
        Qt::KeyboardModifiers mask = Qt::ShiftModifier | Qt::ControlModifier | Qt::AltModifier | Qt::MetaModifier;
        Qt::KeyboardModifiers mod = modifiers & mask;
        int key = e->key ();
        if (mod == Qt::ControlModifier && key >= Qt::Key_1 && key <= Qt::Key_5)
        {
            gotoBookmark (key - Qt::Key_0);
            return;
        }
        else if (mod == Qt::MetaModifier && key >= Qt::Key_1 && key <= Qt::Key_5)
        {
            setBookmark (key - Qt::Key_0);
            done = true;
        }
        else if (mod == Qt::ControlModifier + Qt::ShiftModifier)
        {
            if (key == Qt::Key_Exclam)
            {
                setBookmark (1);
                done = true;
            }
            if (key == Qt::Key_At)
            {
                setBookmark (2);
                done = true;
            }
            if (key == Qt::Key_NumberSign)
            {
                setBookmark (3);
                done = true;
            }
            if (key == Qt::Key_Dollar)
            {
                setBookmark (4);
                done = true;
            }
            if (key == Qt::Key_Percent)
            {
                setBookmark (5);
                done = true;
            }
        }
    }
    if (!done)
    {
       QTextEdit::keyPressEvent (e);
    }
}

void Edit::setBookmark (int markType)
{
    int line = textCursor ().blockNumber ();
    QList <ExtraSelection> selections = extraSelections ();
    bool found = false;
    int cnt = selections.count ();
    for (int inx = 0; inx < cnt && ! found; inx++)
    {
        ExtraSelection item = selections [inx];
        if (item.format.hasProperty (bookmarkProperty) &&
            item.format.intProperty (bookmarkProperty) == markType &&
            item.cursor.blockNumber () == line)
            {
               selections.removeAt (inx);
               setExtraSelections (selections);
               found = true;
            }
    }
    if (!found)
    {
        QColor lineColor = bookmark_colors [markType - 1];
        ExtraSelection item = QTextEdit::ExtraSelection ();
        item.format.setBackground (lineColor);
        item.format.setProperty (bookmarkProperty, markType);
        item.format.setProperty (QTextFormat::FullWidthSelection, QVariant (true));
        item.cursor = textCursor ();
        selections.append (item);
        setExtraSelections (selections);
    }
    // if (win != NULL)
    //     win.bookmarks.activate ();
}

void Edit::gotoBookmark (int markType)
{
    int line = textCursor ().blockNumber ();
    QList <ExtraSelection> selections = extraSelections ();
    bool found = false;
    int cnt = selections.count ();
    for (int inx = 0; inx < cnt && ! found; inx++)
    {
        ExtraSelection item = selections [inx];
        if (item.format.hasProperty (bookmarkProperty) &&
            item.format.intProperty (bookmarkProperty) == markType &&
            item.cursor.blockNumber () > line)
            {
                setTextCursor (item.cursor);
                found = true;
            }
    }
    for (int inx = 0; inx < cnt && ! found; inx++)
    {
        ExtraSelection item = selections [inx];
        if (item.format.hasProperty (bookmarkProperty) &&
            item.format.intProperty (bookmarkProperty) == markType)
        {
            setTextCursor (item.cursor);
            found = true;
        }
    }
}

void Edit::gotoPrevBookmark (int markType)
{
    int line = textCursor ().blockNumber ();
    QList <ExtraSelection> selections = extraSelections ();
    bool found = false;
    int cnt = selections.count ();
    for (int inx = 0; inx < cnt && ! found; inx++)
    {
        ExtraSelection item = selections [inx];
        if (item.format.hasProperty (bookmarkProperty) &&
            item.cursor.blockNumber () < line)
            setTextCursor (item.cursor);
    }
}

void Edit::gotoNextBookmark (int markType)
{
    int line = textCursor ().blockNumber ();
    QList <ExtraSelection> selections  = extraSelections ();
    bool found = false;
    int cnt = selections.count ();
    for (int inx = 0; inx < cnt && ! found; inx++)
    {
        ExtraSelection item = selections [inx];
        if (item.format.hasProperty (bookmarkProperty) &&
            item.cursor.blockNumber () > line)
        {
            setTextCursor (item.cursor);
            found = true;
        }
    }
}

void Edit::clearBookmarks ()
{
    // clear all bookmarks in one document
    QList <ExtraSelection> selections = extraSelections ();
    int cnt = selections.count ();
    for (int inx = 0; inx < cnt; inx++)
    {
        ExtraSelection item = selections [inx];
        if (item.format.hasProperty (bookmarkProperty))
        {
            selections.removeAt (inx);
            cnt --;
        }
        else
        {
           inx ++;
        }
    }
    setExtraSelections (selections);
    // if (win != NULL)
    //    win.bookmarks.activate ();
}

BookmarkList Edit::getBookmarks ()
{
    BookmarkList result;
    QList <ExtraSelection> selections = extraSelections ();
    int cnt = selections.count ();
    for (int inx = 0; inx < cnt; inx++)
    {
        ExtraSelection item = selections [inx];
        if (item.format.hasProperty (bookmarkProperty))
        {
            QTextCursor cursor = item.cursor;
            Bookmark answer;
            answer.line = cursor.blockNumber () + 1;
            answer.column = cursor.columnNumber () + 1;
            answer.mark = item.format.intProperty (bookmarkProperty);
            result.append (answer);
        }
    }
    return result;
}

void Edit::setBookmarks (BookmarkList bookmarks)
{
    QList <ExtraSelection> selections = extraSelections ();
    int max = sizeof (bookmark_colors) / sizeof (bookmark_colors [0]);
    for (Bookmark bookmark : bookmarks)
    {
        QTextEdit::ExtraSelection item;
        item.cursor = textCursor ();
        item.cursor.movePosition (QTextCursor::Start);
        if (bookmark.line > 0)
            item.cursor.movePosition (QTextCursor::NextBlock, QTextCursor::MoveAnchor, bookmark.line - 1);
        int index = bookmark.mark;
        if (index < 1)
            index = 1;
        if (index > max)
            index = max;
        item.format.setBackground (bookmark_colors [index - 1]);
        item.format.setProperty (bookmarkProperty, index);
        item.format.setProperty (QTextFormat::FullWidthSelection, QVariant (true));
        selections.append (item);
    }
    setExtraSelections (selections);
}

/* ---------------------------------------------------------------------- */

void Edit::init ()
{
    // setLineWrapMode (QPlainTextEdit::NoWrap);
    setMouseTracking (true);
    // highlighter = Highlighter (document ());
    // lastCursor = NULL;
    lastUnderline = 0;
    // lastUnderlineColor = 0;
}

void Edit::showStatus (QString text)
{
    // if (win != NULL)
    //     win.showStatus (text);
}

void Edit::onCursorPositionChanged ()
{
    QTextCursor cursor = textCursor ();
    int line = cursor.blockNumber () + 1;
    int column = cursor.columnNumber () + 1;
    showStatus ("Line: " + QString::number (line) + " Col: " + QString::number (column));
}

void Edit::selectLine (int line)
{
    QTextCursor cursor = textCursor ();
    cursor.movePosition (QTextCursor::Start);
    cursor.movePosition (QTextCursor::NextBlock, QTextCursor::MoveAnchor, line - 1);
    cursor.movePosition (QTextCursor::EndOfLine, QTextCursor::KeepAnchor);
    setTextCursor (cursor);
    ensureCursorVisible ();
}

void Edit::selectLineByPosition (int pos)
{
    QTextCursor cursor = textCursor ();
    cursor.setPosition (pos);
    cursor.movePosition (QTextCursor::StartOfLine);
    cursor.movePosition (QTextCursor::EndOfLine, QTextCursor::KeepAnchor);
    setTextCursor (cursor);
    ensureCursorVisible ();
}

void Edit::select2 (int start_line, int start_col, int stop_line, int stop_col)
{
    QTextCursor cursor = textCursor ();
    cursor.movePosition (QTextCursor::Start);
    cursor.movePosition (QTextCursor::NextBlock, QTextCursor::MoveAnchor, start_line - 1);
    cursor.movePosition (QTextCursor::NextCharacter, QTextCursor::MoveAnchor, start_col - 1);
    if (start_line == stop_line)
        cursor.movePosition (QTextCursor::NextCharacter, QTextCursor::KeepAnchor, stop_col - start_col + 1);
    else
    {
        cursor.movePosition (QTextCursor::NextBlock, QTextCursor::KeepAnchor, stop_line - start_line);
        cursor.movePosition (QTextCursor::StartOfLine, QTextCursor::KeepAnchor);
        cursor.movePosition (QTextCursor::NextCharacter, QTextCursor::KeepAnchor, stop_col);
    }
    setTextCursor (cursor);
}

bool Edit::event (QEvent * e)
{
    QEvent::Type type = e->type ();
    // show tooltip for current word
    if (type == QEvent::ToolTip)
    {
        // QToolTipEvent * te = e;
        QString tooltip = "";
        QHelpEvent * h = dynamic_cast <QHelpEvent *> (e);
        QTextCursor cursor = cursorForPosition (h->pos ());
        cursor.select (QTextCursor::WordUnderCursor);
        if (cursor.selectedText () != "")
        {
            QTextCharFormat fmt = cursor.charFormat ();
            if (fmt.hasProperty (QTextFormat::TextToolTip))
                tooltip = fmt.toolTip ();
            if (tooltip == "")
                if (fmt.hasProperty (defnProperty))
                    tooltip = fmt.stringProperty (defnProperty);
            if (tooltip == "")
                if (fmt.hasProperty (infoProperty))
                    tooltip = fmt.stringProperty (infoProperty);
        }
        if (tooltip != "")
            QToolTip::showText (h->globalPos (), tooltip);
        else
            QToolTip::hideText ();
        return true;
    }
    else
        return QTextEdit::event (e);
}

void Edit::mouseMoveEvent (QMouseEvent * e)
{
    // hide previous cursor underline'
    if (! lastCursor.isNull ())
    {
        QTextCharFormat fmt = lastCursor.charFormat ();
        fmt.setFontUnderline (lastUnderline);
        fmt.setUnderlineColor (lastUnderlineColor);
        lastCursor.setCharFormat (fmt);
        lastCursor = QTextCursor ();
    }
    Qt::KeyboardModifiers mask = Qt::ShiftModifier | Qt::ControlModifier | Qt::AltModifier | Qt::MetaModifier;
    Qt::KeyboardModifiers mod = e->modifiers () & mask;
    // Ctrl Mouse Move : underline word under cursor
    if (mod == Qt::ControlModifier)
    {
        QTextCursor cursor = cursorForPosition (e->pos ());
        if (cursor.selectedText () == "")
            cursor.select (QTextCursor::WordUnderCursor);
        QTextCharFormat fmt = cursor.charFormat ();
        if (fmt.hasProperty (defnProperty))
        {
            lastCursor = cursor;
            lastUnderline = fmt.fontUnderline ();
            lastUnderlineColor = fmt.underlineColor ();
            fmt.setFontUnderline (true);
            fmt.setUnderlineColor (Qt::red);
            cursor.setCharFormat (fmt);
        }
        else if (fmt.hasProperty (infoProperty))
        {
            lastCursor = cursor;
            lastUnderline = fmt.fontUnderline ();
            lastUnderlineColor = fmt.underlineColor ();
            fmt.setFontUnderline (true);
            fmt.setUnderlineColor (Qt::blue);
            cursor.setCharFormat (fmt);
        }
    }
    QTextEdit::mouseMoveEvent (e);
}

void Edit::mousePressEvent (QMouseEvent * e)
{
    Qt::KeyboardModifiers mask = Qt::ShiftModifier | Qt::ControlModifier | Qt::AltModifier | Qt::MetaModifier;
    Qt::KeyboardModifiers mod = e->modifiers () & mask;
    // Ctrl Mouse : show properties
    if (mod == Qt::ControlModifier)
    {
        QTextCursor cursor = cursorForPosition (e->pos ());
        cursor.select (QTextCursor::WordUnderCursor);
        QTextCharFormat fmt = cursor.charFormat ();
        if (fmt.hasProperty (infoProperty))
        {
            QString name = fmt.stringProperty (infoProperty);
            // QString obj = findIdentifier (name);
            // if (obj != NULL)
            //    win.showProperties (obj);
        }
    }
    // Ctrl Shift Mouse : find compiler data
    if (mod == Qt::ControlModifier + Qt::ShiftModifier)
    {
        QTextCursor cursor = cursorForPosition (e->pos ());
        int line = cursor.blockNumber () + 1;
        int column = cursor.columnNumber () + 1;
        // win.findCompilerData (this, line, column);
    }
    QTextEdit::mousePressEvent (e);
}

void Edit::jumpToObject (QObject * obj)
{
    #if 0
    if (obj != NULL && win != NULL)
    {
        if (hasattr (obj, "src_file"))
        {
            fileName = indexToFileName (obj.src_file);
            line = NULL;
            column = NULL;
            pos = NULL;
            if (hasattr (obj, "src_line"))
                line = obj.src_line;
            if (hasattr (obj, "src_column"))
                column = obj.src_column;
            if (hasattr (obj, "src_pos"))
                pos = obj.src_pos;
            if (fileName == getFileName ())
                edit = this;
            else
                edit = win.loadFile (fileName);
            if (edit != NULL)
                if (line != NULL)
                    edit.selectLine (line);
                else if (pos != NULL)
                    edit.selectLineByPosition (pos);
        }
        win.showProperties (obj);
    }
    #endif
}

void * Edit::findGlobalData ()
{
    #if 0
    data = getattr (this, "navigator_data", NULL);
    return data;
    #else
    return NULL;
    #endif
}

void * Edit::findIdentifier (QString qual_name)
{
    void * data = findGlobalData ();
    if (data != NULL)
    {
        #if 0
        name_list = qual_name->split ('.');
        for (name in name_list)
        {
            answer = NULL;
            if (hasattr (data, "item_dict"))
                answer = data.item_dict.get (name);
            if (answer == NULL && hasattr (data, "registered_scopes"))
                answer = data.registered_scopes.get (name);
            if (answer == NULL && isinstance (data, dict))
                answer = data.get (name);
            if (answer == NULL)
                return NULL;
            data = answer;
        }
        #endif
    }
    return data;
}

QString Edit::findContext (QTextCursor cursor)
{
    QString result = "";
    int level = 0;
    bool first = true;
    int position = cursor.position ();
    bool found = false;
    QTextBlock block = cursor.block ();
    while (! found && block.isValid ())
    {
        QTextBlock::iterator begin = block.begin ();
        QTextBlock::iterator iter = block.end ();
        bool stop = false;
        if (iter != begin)
            iter --;
        else
            stop = true;
        while (! stop && ! found)
        {
            QTextFragment fragment = iter.fragment ();
            bool ok = true;
            if (first)
                ok = fragment.position () < position;
            if (ok)
            {
                QTextCharFormat fmt = fragment.charFormat ();
                if (fmt.hasProperty (closeProperty))
                    level ++;
                if (fmt.hasProperty (openProperty))
                {
                    if (level == 0)
                    {
                        result = fmt.stringProperty (openProperty);
                        found = true;
                    }
                    level --;
                }
            }
            if (iter != begin)
                iter --;
            else
                stop = true;
        }
        block = block.previous ();
        first = false;
    }
    return result;
}

void Edit::displayProperty (QMenu * menu, QTextCharFormat format, QString title, int property)
{
    if (format.hasProperty (property))
        menu->addAction (title + ": " + format.stringProperty (property));
}

void Edit::contextMenuEvent (QContextMenuEvent * e)
{
    QMenu * menu = createStandardContextMenu (e->pos ());
    QTextCursor cursor = cursorForPosition (e->pos ());
    cursor.select (QTextCursor::WordUnderCursor);
    // if (win != NULL)
    {
        menu->addSeparator ();
        QTextCharFormat fmt = cursor.charFormat ();
        if (fmt.hasProperty (infoProperty))
        {
            QString name = fmt.stringProperty (infoProperty);
            QAction * act = menu->addAction ("Show Memo");
            // act->triggered.connect (lambda param, this=this, name=name: showMemo (name));
            act = menu->addAction ("Show References");
            // act->triggered.connect (lambda param, this=this, name=name: showReferences (name));
            void * obj = findIdentifier (name);
            if (obj != NULL)
            {
                act = menu->addAction ("Show Properties");
                // act->triggered.connect (lambda param, this=this, obj=obj: win.showProperties (obj));
                act = menu->addAction ("Jump to object");
                // act->triggered.connect (lambda param, this=this, obj=obj: jumpToObject (obj));
            }
        }
        /*
        if (getattr (this, "compiler_data", NULL) != NULL)
        {
            act = menu->addAction ("Find Compiler Data");
            edit = this;
            line = cursor.blockNumber () + 1;
            column = columnNumber (cursor) + 1;
            act->triggered.connect (lambda param, this=this, line=line, column=column: win.findCompilerData (this, line, column));
        }
        */
    }
    menu->addSeparator ();
    cursor = textCursor ();
    QString text = "line: " + QString::number (cursor.blockNumber () + 1);
    text = text + ", column: " + QString::number (cursor.columnNumber () + 1);
    menu->addAction (text);
    menu->addAction ("selected text: " + cursor.selectedText ());
    menu->addAction ("context: " + findContext (cursor));
    QTextCharFormat fmt = cursor.charFormat ();
    displayProperty (menu, fmt, "tooltip", QTextFormat::TextToolTip);
    displayProperty (menu, fmt, "info", infoProperty);
    displayProperty (menu, fmt, "defn", defnProperty);
    displayProperty (menu, fmt, "open", openProperty);
    displayProperty (menu, fmt, "close", closeProperty);
    displayProperty (menu, fmt, "outline", outlineProperty);
    menu->exec (e->globalPos ());
}

QStringList Edit::getCompletionList ()
{
    QStringList result;
    result << "Completion" << "List";
    QTextCursor cursor = textCursor ();
    QString context = findContext (cursor);
    void * obj = NULL;
    if (context == "")
        obj = findGlobalData ();
    else
        obj = findIdentifier (context);
    /*
    while (obj != NULL)
    {
        if (hasattr (obj, "item_list"))
            for (item in obj.item_list)
                result.append (item.item_name);
        obj = getattr (obj, "item_context", NULL);
    }
    */
    return result;
}

QString Edit::getCursorInfo ()
{
    QString name = "";
    QTextCursor cursor = textCursor ();
    cursor.select (QTextCursor::WordUnderCursor);
    QTextCharFormat fmt = cursor.charFormat ();
    if (fmt.hasProperty (infoProperty))
        name = fmt.stringProperty (infoProperty);
    return name;
}

void Edit::showMemo (QString name)
{
    if (name == "")
        name = getCursorInfo ();
    // if (win != NULL)
    //     win.showMemo (name);
}

void Edit::showReferences (QString name)
{
    if (name == "")
        name = getCursorInfo ();
    // if (win != NULL)
    //     win.showReferences (name);
}

void Edit::gotoNextFunction ()
{
    bool found = false;
    QTextCursor cursor = textCursor ();
    QTextBlock block = cursor.block ().next ();
    while (!found && block.isValid ())
    {
        QTextBlock::iterator iter = block.begin ();
        while (!found && !iter.atEnd ())
        {
            QTextFragment fragment = iter.fragment ();
            if (fragment.isValid ())
            {
                QTextCharFormat fmt = fragment.charFormat ();
                if (fmt.hasProperty (outlineProperty))
                {
                    cursor.setPosition (block.position ());
                    setTextCursor (cursor);
                    found = true;
                }
            }
            iter ++;
        }
        block = block.next ();
    }
}

void Edit::gotoPrevFunction ()
{
    bool found = false;
    QTextCursor cursor = textCursor ();
    QTextBlock block = cursor.block ().previous ();
    while (!found && block.isValid ())
    {
        QTextBlock::iterator iter = block.end ();
        QTextBlock::iterator begin = block.begin ();
        while (!found && iter != begin)
        {
            QTextFragment fragment = iter.fragment ();
            if (fragment.isValid ())
            {
                QTextCharFormat fmt = fragment.charFormat ();
                if (fmt.hasProperty (outlineProperty))
                {
                    cursor.setPosition (block.position ());
                    setTextCursor (cursor);
                    found = true;
                }
            }
            iter --;
        }
        block = block.previous ();
    }
}

void Edit::gotoBegin ()
{
    verticalScrollBar ()->triggerAction (QAbstractSlider::SliderToMinimum);
}

void Edit::gotoEnd ()
{
    verticalScrollBar ()->triggerAction (QAbstractSlider::SliderToMaximum);
}

void Edit::gotoPageUp ()
{
    verticalScrollBar ()->triggerAction (QAbstractSlider::SliderPageStepSub);
}

void Edit::gotoPageDown ()
{
    verticalScrollBar ()->triggerAction (QAbstractSlider::SliderPageStepAdd);
}

void Edit::scrollUp ()
{
    verticalScrollBar ()->triggerAction (QAbstractSlider::SliderSingleStepSub);
}

void Edit::scrollDown ()
{
    verticalScrollBar ()->triggerAction (QAbstractSlider::SliderSingleStepAdd);
}

void Edit::scrollLeft ()
{
    horizontalScrollBar ()->triggerAction (QAbstractSlider::SliderSingleStepSub);
}

void Edit::scrollRight ()
{
    horizontalScrollBar ()->triggerAction (QAbstractSlider::SliderSingleStepAdd);
}

void Edit::enlargeFont ()
{
    QFont font = document ()->defaultFont ();
    if (font.pixelSize () > 0)
        font.setPixelSize (font.pixelSize () + 1);
    else
        font.setPointSize (font.pointSize () + 1);
    document ()->setDefaultFont (font);
}

void Edit::shrinkFont ()
{
    QFont font = document ()->defaultFont ();
    if (font.pixelSize () > 0)
    {
        if (font.pixelSize () > 8)
            font.setPixelSize (font.pixelSize () - 1);
    }
    else if (font.pointSize () > 8)
    {
        font.setPointSize (font.pointSize () - 1);
    }
    document ()->setDefaultFont (font);
}

void Edit::moveLines (bool up)
{
    QTextCursor cursor = textCursor ();
    cursor.beginEditBlock ();
    if (!cursor.hasSelection ())
    {
        cursor.movePosition (QTextCursor::StartOfBlock);
        cursor.movePosition (QTextCursor::NextBlock, QTextCursor::KeepAnchor);
    }
    setTextCursor (cursor);
    cut ();
    setTextCursor (cursor);
    cursor = textCursor ();
    if (up)
        cursor.movePosition (QTextCursor::PreviousBlock);
    else
        cursor.movePosition (QTextCursor::NextBlock);
    setTextCursor (cursor);
    paste ();
    cursor.movePosition (QTextCursor::PreviousBlock);
    setTextCursor (cursor);
    cursor.endEditBlock ();
}

void Edit::moveLinesUp ()
{
    moveLines (true);
}

void Edit::moveLinesDown ()
{
    moveLines (false);
}

/* ---------------------------------------------------------------------- */

void Edit::init2 ()
{
    findBox = NULL;
    lineBox = NULL;
    origFileName = "";
    origTimeStamp = 0;
    origText = "";
    closeWithoutQuestion = false;
    // setEditFont (this);
}

void Edit::findText ()
{
    if (lineBox != NULL)
    {
        lineBox->hide ();
    }
    if (findBox != NULL)
    {
        findBox->edit = this;
        findBox->open ();
    }
}

void Edit::findNext ()
{
    if (findBox != NULL)
    {
        findBox->edit = this;
        findBox->findNext ();
    }
}

void Edit::findPrev ()
{
    if (findBox != NULL)
    {
        findBox->edit = this;
        findBox->findPrev ();
    }
}

void Edit::replaceText ()
{
    if (lineBox != NULL)
    {
        lineBox->hide ();
    }
    if (findBox != NULL)
    {
        findBox->edit = this;
        findBox->open ();
    }
}

void Edit::findSelected ()
{
    findText ();
    if (findBox != NULL)
        findBox->line->lineEdit ()->setText (textCursor ().selectedText ());
}

void Edit::findIncremental ()
{
    findText ();
    if (findBox)
    {
        findBox->line->lineEdit ()->setText (textCursor ().selectedText ());
        findBox->incremental = true;
    }
}

void Edit::goToLine ()
{
    if (findBox != NULL)
    {
        findBox->hide ();
    }
    if (lineBox)
    {
        lineBox->edit = this;
        lineBox->open ();
    }
}

QString Edit::getFileName ()
{
    return origFileName;
}

#if 0
void Edit::simpleReadFile (QString fileName)
{
    f = open (fileName);
    QString text = f.read ();
    setPlainText (text);
}

QString Edit::readFileData (QString  fileName)
{
    QFile f (fileName);
    if (!f.open (QIODevice::ReadOnly))
       throw IOError (f.errorString ());
    QTextStream stream (&f);
    stream.setCodec ("UTF-8");
    QString text = stream.readAll ();
    f.close ();
    return text;
}

void Edit::readFile (QString fileName)
{
    QString origFileName = os.path.abspath (fileName);
    origTimeStamp = os.path.getmtime (fileName);
    try
    {
        QString text = readFileData (fileName);
        if (text.length () > 64 * 1024)
            highlighter.enabled = false;
        QString origText = text;
        setPlainText (text);
        document ().setModified (false);
        QFileInfo fi (fileName);
        setWindowTitle (fi.fileName ());
    }
    catch (IOError e)
    {
        QMessageBox::warning (this, "Read File Error", "Cannot open file %s: %s" % (fileName, e));
    }
    catch (OSError e)
    {
        QMessageBox::warning (this, "Read File Error", "Cannot open file %s: %s" % (fileName, e));
    }
}

void Edit::writeFile (QString fileName)
{
    QString origFileName = os.path.abspath (fileName);
    QString text = toPlainText ();
    fh = NULL;
    try
    {
        try
        {
            fh = QFile (fileName);
            if (!fh.open (QIODevice.WriteOnly))
                raise IOError (unicode (fh.errorString ()))
            stream = QTextStream (fh);
            stream.setCodec ("UTF-8");
            stream << text;
            document ().setModified (false);
            QString origText = text;
        }
        catch (IOError e)
        {
            QMessageBox::warning (this, "Write File Error", "Failed to save %s: %s" % (fileName, e));
        }
        catch (OSError e)
        {
            QMessageBox::warning (this, "Write File Error", "Failed to save %s: %s" % (fileName, e));
        }
    }
    finally
    {
        if (fh is not NULL)
            fh.close ();
    }
    origTimeStamp = os.path.getmtime (fileName);
}
#endif

bool Edit::isModified ()
{
    QString text = toPlainText ();
    return text != origText;
}

#if 0
bool Edit::isModifiedOnDisk ()
{
    bool result = false;
    if (os.path.getmtime (origFileName) > origTimeStamp)
    {
        // time stamp changed
        QString text = readFileData (origFileName);
        if (text != toPlainText ())
        {
            // text changed
            result = true;
        }
    }
    return result;
}
#endif

void Edit::openFile ()
{
    QString fileName = QFileDialog::getOpenFileName (this, "Open File");
    // if (fileName != "")
    //     readFile (fileName);
}

void Edit::saveFile ()
{
    // writeFile (origFileName);
}

void Edit::saveFileAs ()
{
    QString fileName = QFileDialog::getSaveFileName (this, "Save File As", origFileName);
    // if (fileName != "")
    //     writeFile (fileName);
}

/* ---------------------------------------------------------------------- */

FindBox::FindBox (QWidget * parent) :
   QWidget (parent)
{
    edit = NULL;
    small = false;
    red = QColor (Qt::red).lighter (160);
    green = QColor (Qt::green).lighter (160);

    horiz_layout = new QHBoxLayout;
    setLayout (horiz_layout);

    closeButton = new QToolButton;
    closeButton->setIcon (QIcon::fromTheme ("dialog-close"));
    closeButton->setShortcut (QKeySequence ("Esc"));
    connect (closeButton, SIGNAL (clicked ()), this, SLOT (hide ()));

    horiz_layout->addWidget (closeButton);
    label = new QLabel;
    label->setText ("Find:");
    horiz_layout->addWidget (label);

    line = new QComboBox;
    line->setEditable (true);
    horiz_layout->addWidget (line);

    clearButton = new QToolButton;
    clearButton->setIcon (QIcon::fromTheme ("edit-clear-list"));
    connect (clearButton, SIGNAL (clicked ()), line, SLOT (clearEditText ()));
    horiz_layout->addWidget (clearButton);

    wholeWords = new QCheckBox;
    wholeWords->setText ("Whole &words");
    horiz_layout->addWidget (wholeWords);

    matchCase = new QCheckBox;
    matchCase->setText ("Match &case");
    horiz_layout->addWidget (matchCase);

    nextButton = new QPushButton;
    nextButton->setText ("&Next");
    connect (nextButton, SIGNAL (clicked ()), this, SLOT (findNext));
    horiz_layout->addWidget (nextButton);

    prevButton = new QPushButton;
    prevButton->setText ("&Prev");
    connect (prevButton, SIGNAL (clicked ()), this , SLOT (findPrev));
    horiz_layout->addWidget (prevButton);

    horiz_layout->setStretch (2, 10);
    connect (line->lineEdit(), SIGNAL (returnPressed ()), this, SLOT (returnPressed));
    connect (line, SIGNAL (editTextChanged (string)), this, SLOT (textChanged));
}

void FindBox::resizeEvent (QResizeEvent * e)
{
    int w = e->size ().width ();
    if (w < 640)
    {
        if (! small)
        {
            wholeWords->setText ("&Words");
            matchCase->setText ("&Case");
            small = true;
        }
    }
    else if (small)
    {
        wholeWords->setText ("Whole &words");
        matchCase->setText ("Match &case");
        small = false;
    }
    FindBox::resizeEvent (e);
}

void FindBox::returnPressed ()
{
    findNext ();
}

void FindBox::textChanged (QString text)
{
    findNext ();
    QTextDocument::FindFlags opt (0);
    if (wholeWords->isChecked ())
        opt = opt | QTextDocument::FindWholeWords;
    if (matchCase->isChecked ())
        opt = opt | QTextDocument::FindCaseSensitively;
    QTextCursor cursor;
    int pos;
    if (incremental)
    {
        cursor = edit->textCursor ();
        cursor.setPosition (start);
        edit->setTextCursor (cursor);
    }
    else
    {
        cursor = edit->textCursor ();
        pos = cursor.position ();
    }
    bool ok = edit->find (text, opt);
    if (!ok)
        ok = edit->find (text, opt | QTextDocument::FindBackward);
    if (!incremental)
    {
        cursor.setPosition (pos);
        edit->setTextCursor (cursor);
    }
    QColor color;
    if (ok)
        color = green;
    else
        color = red;
    QWidget * widget = line->lineEdit ();
    QPalette palette = widget->palette ();
    QPalette::ColorRole role = widget->backgroundRole ();
    palette.setColor (role, color);
    widget->setPalette (palette);
}

void FindBox::open ()
{
    show ();
    line->setFocus ();
    line->lineEdit ()->selectAll ();
    incremental = false;
    start = 0;
    if (edit != NULL)
        start = edit->textCursor ().position ();
}

void FindBox::findNext ()
{
    findStep (false);
}

void FindBox::findPrev ()
{
    findStep (true);
}

void FindBox::findStep (bool back)
{
    QString text = line->currentText ();
    if (text != "")
    {
        if (line->findText (text) == -1)
            line->addItem (text);
        QTextDocument::FindFlags opt (0);
        if (wholeWords->isChecked ())
            opt = opt | QTextDocument::FindWholeWords;
        if (matchCase->isChecked ())
            opt = opt | QTextDocument::FindCaseSensitively;
        if (back)
            opt = opt | QTextDocument::FindBackward;
        bool ok = edit->find (text, opt);
        if (!ok)
        {
            if (back)
                edit->moveCursor (QTextCursor::End);
            else
                edit->moveCursor (QTextCursor::Start);
            edit->find (text, opt);
        }
    }
}

/* ---------------------------------------------------------------------- */

GoToLineBox::GoToLineBox (QWidget * parent) :
   QWidget (parent)
{
    edit = NULL;

    horiz_layout = new QHBoxLayout;
    setLayout (horiz_layout);

    closeButton = new QToolButton;
    closeButton->setIcon (QIcon::fromTheme ("dialog-close"));
    closeButton->setShortcut (QKeySequence ("Esc"));
    connect (closeButton, SIGNAL (clicked ()), this, SLOT (hide));
    horiz_layout->addWidget (closeButton);

    label = new QLabel;
    label->setText ("Go to line:");
    horiz_layout->addWidget (label);

    line = new QSpinBox;
    horiz_layout->addWidget (line);

    button = new QPushButton;
    button->setText ("Go");
    horiz_layout->addWidget (button);

    horiz_layout->addStretch ();
    connect (line, SIGNAL (editingFinished ()), this, SLOT (editingFinished));
}

void GoToLineBox::open ()
{
    QTextCursor cursor = edit->textCursor ();
    QTextDocument * document = edit->document ();
    line->setMinimum (1);
    line->setMaximum (document->blockCount ());
    line->setValue (cursor.blockNumber () + 1);
    line->setFocus ();
    line->selectAll ();
    show ();
}

void GoToLineBox::editingFinished ()
{
    edit->selectLine (line->value ());
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

