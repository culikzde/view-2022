
/* ---------------------------------------------------------------------- */

class BoolIO
{
    bool read (QVariant v)
    {
       return v.toBool ();
    }

    QVariant write (bool b)
    {
       return QVariant (b);
    }
};

class IntIO
{
    int read (QVariant v)
    {
       return v.toInt ();
    }

    QVariant write (int n)
    {
       return QVariant (n);
    }
};

class UIntIO
{
    unsigned int read (QVariant v)
    {
       return v.toUInt ();
    }

    QVariant write (unsigned int n)
    {
       return QVariant (n);
    }
};

class LongIO
{
    long read (QVariant v)
    {
       return v.toLongLong ();
    }

    QVariant write (long n)
    {
       return QVariant (n);
    }
};

class ULongIO
{
    unsigned long read (QVariant v)
    {
       return v.toULongLong ();
    }

    QVariant write (unsigned long n)
    {
       return QVariant (n);
    }
};

class FloatIO
{
    float read (QVariant v)
    {
       return v.toFloat ();
    }

    QVariant write (float x)
    {
       return QVariant (x);
    }
};

class DoubleIO
{
    double read (QVariant v)
    {
       return v.toDouble ();
    }

    QVariant write (double x)
    {
       return QVariant (x);
    }
};

class StringIO
{
    QString read (QVariant v)
    {
       return v.toString ();
    }

    QVariant write (QString s)
    {
       return QVariant (s);
    }
};

class StringListIO
{
    QStringList read (QVariant v)
    {
       return v.toStringList ();
    }

    QVariant write (QStringList s)
    {
       return QVariant (s);
    }
};

class ColorIO
{
    QColor read (QVariant v)
    {
       return v.value <QColor> ();
    }

    QVariant write (QColor c)
    {
       return QVariant (c);
    }
};

class ImageIO
{
    QImage read (QVariant v)
    {
       return v.value <QImage> ();
    }

    QVariant write (QImage i)
    {
       return QVariant (i);
    }
};

class IconIO
{
    QIcon read (QVariant v)
    {
       return v.value <QIcon> ();
    }

    QVariant write (QIcon i)
    {
       return QVariant (i);
    }
};

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
