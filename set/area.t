
/* area.t */

// #include <QGraphicsView>
// #include <QGraphicsScene>
// #include <QGraphicsItemGroup>
// #include <QGraphicsRectItem>
// #include <QGraphicsLineItem>

// #include <QTabWidget>
// #include <QToolBar>
// #include <QVBoxLayout>

// #include <QGraphicsSceneDragDropEvent>
// #include <QMimeData>
// #include <QBitmap>

// #include <cassert>

#define Q_OBJECT
#define signals
#define slots

/* ---------------------------------------------------------------------- */

class GraphicsItemChange;
typedef double qreal;

/* ---------------------------------------------------------------------- */

class Area;
class Source;
class Target;

/* ---------------------------------------------------------------------- */

class Area : public QGraphicsRectItem
{
   private :
      QString title;

   public:
      void add (QGraphicsItem * a);
      Area * aboveArea ();

      void setTitle (QString s);

  public:
      void updateConnections ();
      void updateTargets (Area * target_area);
      virtual QVariant itemChange (GraphicsItemChange change, const QVariant & value);

  protected:
      virtual void dragEnterEvent (QGraphicsSceneDragDropEvent * event);
      virtual void dropEvent (QGraphicsSceneDragDropEvent * event);

      virtual void mouseMoveEvent (QGraphicsSceneMouseEvent * event);
      virtual void mousePressEvent (QGraphicsSceneMouseEvent * event);
      virtual void mouseReleaseEvent (QGraphicsSceneMouseEvent * event);

  public:
      Area ();
};

/* ---------------------------------------------------------------------- */

class Source : public QGraphicsRectItem
{
   private:
      Target * target;

   private:
      QGraphicsItemGroup * group;
      qreal x1;
      qreal y1;
      QPen pen;
      void addLine (qreal x2, qreal y2);
      void horizLine (qreal x2);
      void vertLine (qreal y2);

   public:
      Area * getArea ();
      Target * getTarget () { return target; }
      void setTarget (Target * t);
      void updateConnection ();

      Source ();
};

/* ---------------------------------------------------------------------- */

class Target : public QGraphicsRectItem
{
   public:
      Area * getArea ();
      Target ();
};

/* ---------------------------------------------------------------------- */

class Plane : public QGraphicsRectItem
{
   protected:
      virtual void dragEnterEvent (QGraphicsSceneDragDropEvent * event);
      virtual void dropEvent (QGraphicsSceneDragDropEvent * event);

   public:
      void add (QGraphicsItem * a);
      Plane ();
};

/* ---------------------------------------------------------------------- */

class AreaView : public QGraphicsView
{
   Q_OBJECT

   private:
      void example ();

   private:
      QGraphicsScene * scene;
      Plane * plane;

   private:
      QGraphicsItem * getRoot () { return plane; }
      void add (QGraphicsItem * a);

   public:
      QGraphicsScene * getScene () { return scene; }

   public slots:
      void onSelectionChanged ();

   public signals:
      void itemSelected (QGraphicsItem * item);

   public:
       explicit AreaView (QWidget * parent = 0);
       // virtual ~ AreaView ();
};

/* ---------------------------------------------------------------------- */

Area::Area ()
{
   setAcceptDrops (true);

   setFlag (QGraphicsItem::ItemIsMovable);
   setFlag (QGraphicsItem::ItemIsSelectable);
   setFlag (QGraphicsItem::ItemSendsScenePositionChanges);

   setRect (0, 0, 100, 100);
}

void Area::add (QGraphicsItem * a)
{
    assert (a != NULL);
    a->setParentItem (this);
}

Area * Area::aboveArea ()
{
    QGraphicsItem * a = parentItem ();
    return dynamic_cast < Area * > (a);
}

void Area::setTitle (QString s)
{
    title = s;
}

/* ---------------------------------------------------------------------- */

Source::Source ()
{
   target = NULL;
   group = NULL;

   setRect (0, 0, 10, 10);
   setPen (QPen (Qt::red));
   setBrush (QBrush (Qt::green));
   setFlag (QGraphicsItem::ItemIsMovable);
   setFlag (QGraphicsItem::ItemIsSelectable);
}

void Source::setTarget (Target * t)
{
    target = t;
}

Area * Source::getArea ()
{
    return dynamic_cast < Area * > (parentItem ());
}

/* ---------------------------------------------------------------------- */

Target::Target ()
{
   setRect (0, 0, 10, 10);
   setPen (QPen (Qt::red));
   setBrush (QBrush (Qt::yellow));
   setFlag (QGraphicsItem::ItemIsMovable);
   setFlag (QGraphicsItem::ItemIsSelectable);
}

Area * Target::getArea ()
{
    return dynamic_cast < Area * > (parentItem ());
}

/* ---------------------------------------------------------------------- */

void Source::addLine (qreal x2, qreal y2)
{
   if (x1 != x2 || y1 != y2)
   {
      QGraphicsLineItem * line = new QGraphicsLineItem;
      line->setLine (x1, y1, x2, y2);
      // line->setPen (QPen (Qt::red));
      line->setPen (pen);
      line->setParentItem (group);

      x1 = x2;
      y1 = y2;
   }
}

void Source::horizLine (qreal x2)
{
   addLine (x2, y1);
}

void Source::vertLine (qreal y2)
{
   addLine (x1, y2);
}

void Source::updateConnection ()
{
   if (target != null)
   {
      // delete previous group
      if (group != NULL)
      {
         delete group;
         group = NULL;
      }

      // create new group
      group = new QGraphicsItemGroup;
      group->setFlag (QGraphicsItem::ItemHasNoContents);

      QPointF pt1 = this->mapToScene (this->rect().center ());
      QPointF pt2 = target->mapToScene (target->rect().center ());

      Area * a1 = this->getArea ();
      QRectF r1 = a1->rect();
      QPointF tr1 = a1->mapToScene (r1.topRight());
      QPointF br1 = a1->mapToScene (r1.bottomRight());
      qreal src_right = tr1.x();
      qreal src_top = tr1.y();
      qreal src_bottom = br1.y();

      Area * a2 = target->getArea ();
      QRectF r2 = a2->rect();
      QPointF tr2 =a2->mapToScene (r2.topRight());
      QPointF br2 = a2->mapToScene (r2.bottomRight());
      qreal tgt_right = tr2.x();
      qreal tgt_top = tr2.y();
      qreal tgt_bottom = br2.y();

      this->x1 = pt1.x();
      this->y1 = pt1.y();

      qreal x2 = pt2.x();
      qreal y2 = pt2.y();

      pen = QPen (Qt::red);

      const int delta = 20;

      if (x1 + 2*delta < x2)
      {
         qreal x = (x1 + x2) / 2;
         horizLine (x); // half way in horizontal direction

         vertLine (y2);
         horizLine (x2);
      }
      else
      {
         // real target
         qreal x3 = x2;
         qreal y3 = y2;

         // point behind target
         x2 -= delta;
         // y2 -= delta;

         horizLine (x1+delta); // small starting line

         qreal y = (y1 + y2) / 2;

         if (src_bottom + delta <= tgt_top)
         {
            qreal y = (src_bottom + tgt_top)/2;
            vertLine (y); // half way in vertical direction
            horizLine (x2); // line in horizontal direction
         }

         else if (tgt_bottom + delta <= src_top)
         {
            qreal y = (tgt_bottom + src_top)/2;
            vertLine (y); // half way in vertical direction
            horizLine (x2); // line in horizontal direction
         }

         else if (y1 < y2)
         {
            // go below target
            if (tgt_right > src_right)
               horizLine (tgt_right + delta);
            else
               horizLine (src_right + delta);
            vertLine (tgt_bottom + delta);
            horizLine (x2);
         }

         else
         {
            // go above target
            if (tgt_right > src_right)
               horizLine (tgt_right + delta);
            else
               horizLine (src_right + delta);
            vertLine (tgt_top - delta);
            horizLine (x2);
         }

         // to center of target
         vertLine (y3);
         horizLine (x3);
      }

      QGraphicsScene * scn = this->scene ();
      if (scn != NULL)
         scn->addItem (group);
   }
}

void Area::updateConnections ()
{
   QList < QGraphicsItem * > list = childItems ();
   int cnt = list.count ();
   for (int inx = 0 ; inx < cnt ; inx ++)
   {
      QGraphicsItem * item = list [inx];

      Source * source = dynamic_cast < Source * > (item);
      if (source != NULL)
          source->updateConnection ();

      Area * area = dynamic_cast < Area * > (item);
      if (area != NULL)
          area->updateConnections ();
   }
}

void Area::updateTargets (Area * target_area)
{
   QList < QGraphicsItem * > list = childItems ();
   int cnt = list.count ();
   for (int inx = 0 ; inx < cnt ; inx ++)
   {
      QGraphicsItem * item = list [inx];

      Source * source = dynamic_cast < Source * > (item);
      if (source != NULL)
      {
         Target * target = source->getTarget ();
         if (target != NULL && target->getArea () == target_area)
            source->updateConnection ();
      }

      Area * area = dynamic_cast < Area * > (item);
      if (area != NULL)
      {
         area->updateTargets (target_area);
      }
   }
}

QVariant Area::itemChange (GraphicsItemChange change, const QVariant & value)
{
   if (change == ItemPositionChange)
   {
       QList < QGraphicsItem * > list = childItems ();
       int cnt = list.count ();
       for (int inx = 0 ; inx < cnt ; inx ++)
       {
          QGraphicsItem * item = list [inx];

          // update connections with source in this area
          Source * source = dynamic_cast < Source * > (item);
          if (source != NULL)
              source->updateConnection ();
       }

       // update connections with targets in this area
       Area * a = this;
       Area * above = a->aboveArea ();
       while (above != NULL)
       {
          a = above;
          above = a->aboveArea ();
       }

       a->updateTargets (this);
   }

   return QGraphicsRectItem::itemChange (change, value);
}

/* ---------------------------------------------------------------------- */

void dropShape (QGraphicsItem * owner,  QGraphicsSceneDragDropEvent * event)
{
   const QMimeData * mime = event->mimeData ();
   QString tool = QString (mime->data (shapeFormat));

   if (tool == "area")
   {
      Area * item = new Area;
      item->setPen (QColor (Qt::green));
      item->setBrush (QColor (Qt::yellow));
      item->setTitle ("Cecko");
      item->setToolTip ("Cecko");

      item->setPos (event->scenePos() - owner->scenePos());
      item->setParentItem (owner);
   }
   else if (tool == "sub-area")
   {
      Area * item = new Area;
      item->setTitle ("orange");
      item->setPen (QColor (Qt::red));
      item->setBrush (QColor ("orange"));
      item->setToolTip ("Subitem");
      item->setRect (10, 10, 80, 20);

      item->setPos (event->scenePos() - owner->scenePos());
      item->setParentItem (owner);
   }
   else if (tool == "source")
   {
      Source * item = new Source ();
      item->setPos (event->scenePos ());
      item->setParentItem (owner);
   }
   else if (tool == "target")
   {
      Target * item = new Target ();
      item->setPos (event->scenePos() - owner->scenePos());
      item->setParentItem (owner);
   }
   else if (tool == "ellipse")
   {
      QGraphicsEllipseItem * item = new QGraphicsEllipseItem;
      item->setRect (0, 0, 100, 50);
      item->setPen (QColor (Qt::blue));
      item->setBrush (QBrush (Qt::yellow));
      item->setFlag (QGraphicsItem::ItemIsMovable);
      item->setFlag (QGraphicsItem::ItemIsSelectable);

      item->setPos (event->scenePos() - owner->scenePos());
      item->setParentItem (owner);
   }
}

/* ---------------------------------------------------------------------- */

void Area::dragEnterEvent (QGraphicsSceneDragDropEvent * event)
{
   const QMimeData * mime = event->mimeData ();

   if (mime->hasColor () ||
       mime->hasFormat (shapeFormat))
   {
       event->setAccepted (true);
   }
   else
   {
       event->setAccepted (false);
   }
 }

void Area::dropEvent (QGraphicsSceneDragDropEvent * event)
{
   const QMimeData * mime = event->mimeData ();

   if (mime->hasColor())
   {
      QVariant v = event->mimeData()->colorData();
      QColor c = v.value<QColor>();
      setBrush (c);

      // QColor c2 = qVariantValue<QColor> (event->mimeData()->colorData());
      QColor c3 = event->mimeData()->colorData().var.value<QColor>();
      // QColor c4 = event->mimeData()->colorData().value();
      // QColor c5 = event->mimeData()->var->colorData();
   }
   else if (mime->hasFormat (shapeFormat))
   {
      dropShape (this, event);
   }
}

/* ---------------------------------------------------------------------- */

QPointF apos;
QRectF  arec;

void Area::mousePressEvent (QGraphicsSceneMouseEvent * event)
{
   QGraphicsRectItem::mousePressEvent (event);
   apos = event->scenePos ();
   arec = this->rect();
}

void Area::mouseMoveEvent (QGraphicsSceneMouseEvent * event)
{
   QGraphicsRectItem::mouseMoveEvent (event);
   QPointF bpos = event->scenePos ();
   QRectF brec = arec;
   brec.adjust (0, 0, bpos.x()-apos.x(), bpos.y()-apos.y());
}

void Area::mouseReleaseEvent (QGraphicsSceneMouseEvent * event)
{
   QGraphicsRectItem::mouseReleaseEvent (event);
}

/* ---------------------------------------------------------------------- */

Plane::Plane ()
{
   /* plane */

   setPen (QPen (Qt::NoPen));
   setBrush (QBrush (Qt::NoBrush));
   // setFlags (0);
   setRect (10, 10, 630, 470);
}

void Plane::add (QGraphicsItem * a)
{
    assert (a != NULL);
    a->setParentItem (this);
}

void Plane::dragEnterEvent (QGraphicsSceneDragDropEvent * event)
{
   const QMimeData * mime = event->mimeData ();

   if (mime->hasColor () || mime->hasFormat (shapeFormat))
       event->setAccepted (true);
   else
       event->setAccepted (false);
 }

void Plane::dropEvent (QGraphicsSceneDragDropEvent * event)
{
   const QMimeData * mime = event->mimeData ();
   if (mime->hasFormat (shapeFormat))
      dropShape (this, event);
}

/* ---------------------------------------------------------------------- */

AreaView::AreaView (QWidget * parent) :
   scene (NULL),
   plane (NULL)
{
   /* graphics view */

   scene = new QGraphicsScene;
   scene->setSceneRect (0, 0, 800, 600);
   this->setScene (scene);

   /* basic plane */

   plane = new Plane;
   scene->addItem (plane);

   /* background */

   /*
   const int n = 16;
   QBitmap texture (n, n);
   texture.clear ();

   QPainter * painter = new QPainter (& texture);
   painter->drawLine (0, 0, n-1, 0);
   painter->drawLine (0, 0, 0, n-1);
   delete painter;

   QBrush brush (QColor ("cornflowerblue"));
   brush.setTexture (texture);
   scene->setBackgroundBrush (brush);
   */

   /* connect */

   // connect (scene, SIGNAL(selectionChanged()), SLOT(onSelectionChanged()));
   // connect (scene, &QGraphicsView::selectionChanged, &AreaView::onSelectionChanged);
   scene->selectionChanged () { this->onSelectionChanged(); }
   // scene->selectionChanged { onSelectionChanged(); }
   // scene->selectionChanged = onSelectionChanged;

   /* initialization */

   example ();
}

void AreaView::onSelectionChanged ()
{
   QList <QGraphicsItem * > list = scene->selectedItems ();
   if (list.count() == 1)
   {
       QGraphicsItem * item = list.at (0);
       emit itemSelected (item);
   }
}

void AreaView::add (QGraphicsItem * a)
{
   plane->add (a);
}

/* ---------------------------------------------------------------------- */

void AreaView::example ()
{
   Area * a = new Area;
   a->setPen (QPen (Qt::green));
   a->setBrush (Qt::yellow);
   a->setPos (10, 10);
   a->setTitle ("Acko");
   a->setToolTip ("A");

   /*
   ListField * f = new ListField;
   f->setName ("size");
   f->setValue ("small");
   f->list << "small" << "medium" << "large";
   a->addField (f);

   Field * f2 = new Field;
   f2->setName ("color");
   f2->setValue ("blue");
   a->addField (f2);
   */

   Source * a_source = new Source;
   a_source->setPos (90, 0);
   a->add (a_source);

   Target * a_target = new Target;
   a->add (a_target);

   Area * b = new Area;
   b->setPen (QPen (Qt::red));
   b->setBrush (QColor ("cornflowerblue"));
   b->setTitle ("Becko");
   b->setToolTip ("B");
   b->setPos (320, 10);

   Target * b_target = new Target;
   b->add (b_target);

   Source * b_source = new Source;
   b_source->setPos (90, 0);
   b->add (b_source);

   a_source->setTarget (b_target);

   #if 0
   b_source->setTarget (a_target);
   #endif

   Area * s = new Area;
   s->setTitle ("orange");
   s->setPen (QPen (Qt::red));
   s->setBrush (QColor ("orange"));
   s->setToolTip ("Subitem");
   s->setRect (10, 10, 80, 20);
   a->add (s);

   Area * t = new Area;
   t->setTitle ("orange 2");
   t->setPen (QPen (Qt::red));
   t->setBrush (QColor ("orange"));
   t->setToolTip ("Subitem 2");
   t->setRect (10, 10, 80, 20);
   b->add (t);

   #if 0
   Area * u = new Area;
   u->setTitle ("lime");
   u->setPen (QPen (Qt::red));
   u->setBrush (QColor ("lime"));
   u->setToolTip ("Subitem 3");
   u->setRect (10, 40, 80, 20);
   b->add (u);

   Area * v = new Area;
   v->setTitle ("green");
   v->setPen (QPen (Qt::red));
   v->setBrush (QColor ("green"));
   v->setToolTip ("Subitem 3");
   v->setRect (10, 70, 80, 20);
   b->add (v);
   #endif

   add (a);
   add (b);
   this->centerOn (a);
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
