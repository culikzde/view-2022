
/* ---------------------------------------------------------------------- */

struct Item
{
    QString   text;
    int       size;
    /*
    QIcon     icon;
    QColor    color;
    bool      selected;
    QDateTime time;
    */
};

/* ---------------------------------------------------------------------- */

DescriptionModule
{
   Record = Item;
   // Basic = QObject;
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
     QApplication * appl = new QApplication (argc, argv);

     QTableWidget * window = new QTableWidget (null);

     ItemDesc type_desc;
     window->setRowCount (type_desc.fields.count ());
     window->setWindowTitle (type_desc.name);

     int inx = 0;
     for (FieldDesc * field : type_desc.fields)
     {
        QTableWidgetItem * node = new QTableWidgetItem (window);
        node->setText (field->name);
        window->setItem (inx, 0, node);
        inx ++;
     }

     window->show ();
     appl->exec ();
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
