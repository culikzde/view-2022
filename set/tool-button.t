
/* tool-button.t */

const QString toolFormat = "application/x-tool";
const QString shapeFormat = "application/x-shape";
const QString widgetFormat = "application/x-widget";

/* ---------------------------------------------------------------------- */

class ToolButton : public QToolButton
{
   private:
      const QString toolFormat = "application/x-tool"; // !?

   private:
      QString name;
      QIcon icon;
      QString format;

   protected:
      void mousePressEvent (QMouseEvent * event)
      {
         if (event->button() == Qt::LeftButton )
         {
            QMimeData * mimeData = new QMimeData;
            mimeData->setData (format, name.toLatin1());

            QDrag * drag = new QDrag (this);
            drag->setMimeData (mimeData);
            drag->setPixmap (icon.pixmap (24, 24));
            drag->setHotSpot (QPoint (-16, -16));

            Qt::DropAction dropAction = drag->exec (Qt::MoveAction | Qt::CopyAction | Qt::LinkAction);
         }
      }

   public:
      ToolButton (QToolBar * parent, QString p_name, QString p_icon_name = "", QString p_format = "") :
         QToolButton (parent) // ,
         // name (p_name),
      {
         parent->addWidget (this);

         name = p_name;

         #if 0
         if (p_icon_name == "")
              p_icon_name = ":/icons/" + p_name + ".svg";
         icon = QIcon (p_icon_name);
         #endif

         format = p_format;
         if (format == "")
            format = toolFormat;

         #if 0
         setIcon (icon);
         #endif
         setText (name);
         setToolTip (name);
      }
};

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
