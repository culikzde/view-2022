
/* prop-tree-example.t */

/* ---------------------------------------------------------------------- */

typedef QString FileName;

struct Item
{
    QString     name;
    bool        selected;
    int         size;
    double      value;
    QColor      color;
    QFont       font;
    FileName    source;
    QStringList list;
};

/* ---------------------------------------------------------------------- */

PropertyTreeModule
{
   Record = Item;
   Properties = PropertyWindow;
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
     QApplication * appl = new QApplication (argc, argv);

     QVariant v = false;
     bool b = v.var.toBool ();

     Item * item = new Item;
     item->name = "abc";
     item->selected = true;
     item->size = 7;
     item->value = 3.14;
     item->color = QColor ("orange");
     item->source = "examples/examples.cc";
     item->list << "abc" << "def" << "klm";

     PropertyWindow * window = new PropertyWindow (null, item);
     appl->exec ();
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
