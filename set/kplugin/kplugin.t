
/* kplugin.t */

/* ---------------------------------------------------------------------- */

string description = "KPlugin"; // quoted
string icon = "gcolor2";
string id = "kplugin";
string name = "A KPlugin";

file "kplugin.json"
{
{
    "KPlugin": {
        "Id": id,
        "Name": name,
        "Description": description,
        "Icon": icon,
        "ServiceTypes": [
            "KDevelop/Plugin"
        ]
    },
    "X-KDevelop-Category": "Global",
    "X-KDevelop-Mode": "GUI"
}
}

/* ---------------------------------------------------------------------- */

string rc_name = "kplugin.rc"; // no quote
prefix_name = "/kxmlgui5/kplugin"; //quoted

file "kplugin.qrc"
{
<!DOCTYPE RCC>
<RCC version="1.0">
    <qresource prefix=prefix_name>
        <file>rc_name</file>
    </qresource>
</RCC>
}

/* ---------------------------------------------------------------------- */

string gui_name = "kplugin"; // quoted
string action_name = "kplugin_run"; // quoted

file "kplugin.qrc"
{
<!DOCTYPE gui SYSTEM "kpartgui.dtd">
<gui name=gui_name version="12">

  <MenuBar>
    <Menu name="help">
      <Action name=action_name />
    </Menu>
  </MenuBar>

</gui>
}

/* ---------------------------------------------------------------------- */

string plugin_name = "kplugin";
string json_name = "kplugin.json";
string qrc_name = "kplugin.qrc";

file "CMakeLists.txt"
{
cmake_minimum_required(VERSION 2.8.12)

project(plugin_name)

find_package (ECM 1.0.0 REQUIRED NO_MODULE)
set (CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

include (KDEInstallDirs)
include (KDECMakeSettings)
include (FeatureSummary)

find_package(KDevPlatform 5.0 REQUIRED)

feature_summary (WHAT ALL
                 INCLUDE_QUIET_PACKAGES
                 FATAL_ON_MISSING_REQUIRED_PACKAGES
                 )

set (plugin_SRCS
     module.cc
)

set (CMAKE_VERBOSE_MAKEFILE ON)

qt5_add_resources (plugin_SRCS qrc_name)

kdevplatform_add_plugin (plugin_name
                         JSON json_name
                         SOURCES ${plugin_SRCS})

set (CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS}" "-Wl,--no-undefined" )

target_link_libraries (plugin_name
    KDev::Interfaces
    KDev::Language
    KDev::Serialization
    KDev::Sublime
)
}

/* ---------------------------------------------------------------------- */

/*
dnf install extra-cmake-modules
dnf install kdevelop-devel
*/

/* ---------------------------------------------------------------------- */
