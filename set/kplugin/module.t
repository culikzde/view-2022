
namespace plastic [[compile_time]]
{
   void K_PLUGIN_FACTORY_WITH_JSON (a, b, c, d);

   template <class T>
      void registerPlugin ();

   namespace KDevelop
   {
      class IPlugin;
   }

   namespace std
   {
      int cout;
      int endl;
   }
}

