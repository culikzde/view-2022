
/* module.h */

#ifndef MYMODULE_H
#define MYMODULE_H

#include <interfaces/iplugin.h>
#include <interfaces/iuicontroller.h>
#include <interfaces/idocumentcontroller.h>

/* ----------------------------------------------------------------------- */

// namespace MyModule {

/* ----------------------------------------------------------------------- */

class MyPlugin : public KDevelop::IPlugin
{
   Q_OBJECT

   public:
       MyPlugin (QObject *parent, const QVariantList & args = QVariantList ());
       virtual ~ MyPlugin ();

   public:
       virtual void unload ();

   private Q_SLOTS:
       void run ();

   private:
      KDevelop::IToolViewFactory * m_view_factory;
      // KDevelop::IDocumentFactory * m_document_factory;
};

/* ----------------------------------------------------------------------- */

class MyView : public QWidget
{
   Q_OBJECT
   public:
      MyView (QWidget * parent = NULL);
      virtual ~ MyView ();
};

/* ----------------------------------------------------------------------- */

// } // end of namespace

#endif // MYMODULE_H
