
/* module.cc */

#include "module.h"

#include <QObject>
#include <QAction>
#include <QFile>
#include <QTextStream>
#include <QPluginLoader>
#include <QPushButton>

#include <KLocalizedString>

#include <KXmlGuiWindow>
#include <KPluginFactory>
#include <KPluginLoader>
#include <KStandardAction>
#include <KActionCollection>

#include <interfaces/icore.h>
#include <interfaces/idocumentcontroller.h>
#include <interfaces/iuicontroller.h>

#include <iostream>
// using namespace std;

/* ----------------------------------------------------------------------- */

// K_PLUGIN_FACTORY_WITH_JSON (MyPluginFactory, "kview.json", registerPlugin <MyPlugin>(); )

/* ----------------------------------------------------------------------- */

class ViewFactory : public KDevelop::IToolViewFactory
{
   public:
       virtual QWidget * create (QWidget * parent = 0)
       {
           MyView * view = new MyView (parent);
           return view;
       }

       virtual Qt::DockWidgetArea defaultPosition()
       {
           return Qt::BottomDockWidgetArea;
       }

       virtual QString id() const
       {
           return "org.kdevelop.KView.Info";
       }

   private:
       MyPlugin * m_plugin;

   public:
       ViewFactory (MyPlugin * plugin) :
           m_plugin (plugin)
       { }
};

/* ----------------------------------------------------------------------- */

MyView::MyView (QWidget* parent) :
   QWidget (parent)
{
   setWindowTitle (i18n ("KView"));
   setWhatsThis (i18n ("KView"));

}

MyView::~MyView ()
{
}

/* ----------------------------------------------------------------------- */

MyPlugin::MyPlugin (QObject * parent, const QVariantList & args) :
   /* KDevelop:: */ IPlugin ("kview", parent)
   // m_document_factory (NULL)
{
    Q_UNUSED(args)

    std::cout << std::endl;
    std::cout << " **** KVIEW **** " << std::endl;
    std::cout << std::endl;

    setXMLFile ("kview.rc");

    m_view_factory = new ViewFactory (this);
    core()->uiController()->addToolView ("MyView", m_view_factory);


    KActionCollection* actions = actionCollection ();
    QAction *action = actions->addAction ("kview_run");
    action->setText (i18n ("KView Plugin"));
    // action->setShortcut (Qt::CTRL | Qt::ALT | Qt::Key_4);
    connect (action, SIGNAL (triggered (bool)), this, SLOT (run ()));

    // std::cout << "KVIEW - CREATE DOCUMENT FACTORY" << std::endl;
    // m_document_factory = new MyDocumentFactory (this);
    // KDevelop::IDocumentController* idc = core()->documentController();
    // idc->registerDocumentForMimetype("text/x-kview", m_document_factory);
}

MyPlugin::~MyPlugin()
{
}

void MyPlugin::unload()
{
    core()->uiController()->removeToolView (m_view_factory);
    // core()->uiController()->removeToolView (m_tree_factory);
    // core()->uiController()->removeToolView (m_prop_factory);
}

/* ----------------------------------------------------------------------- */

void MyPlugin::run ()
{
    std::cout << std::endl;
    std::cout << " **** KVIEW RUN **** " << std::endl;
    std::cout << std::endl;

    qDebug () << " ---- KVIEW RUN ---- ";
}

/* ----------------------------------------------------------------------- */

#include "module.moc"
// important, otherwise plugin is not loaded
