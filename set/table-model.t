
/* table-model.t */

/* ---------------------------------------------------------------------- */

namespace plastic {

   class var;

   #define Q_OBJECT
   #define slots

   /* ------------------------------------------------------------------- */

   class Record
   {
   };

   class Collection
   {
       QList < Record * > items;
   };

   /* ------------------------------------------------------------------- */

   int COLUMN_COUNT;
   void HEADER_DATA (var result, int column);
   void RECALL_DATA (var result, var item, int column);
   void STORE_DATA (var item, int column, var value);


} // end of namespace plastic

using namespace plastic;

namespace building {

/* ------------------------------------------------------------------- */

class StringIO
{
    // typedef QString T;
    void recall_data (var result, var field)
    {
         result = field;
    }
    void store_data (var field, var value)
    {
         field = value.var.toString ();
    }
    void edit_data (var dlg, var mapper, int column, var layout)
    {
       QLineEdit * name_edit = new QLineEdit (dlg);
       mapper->addMapping (name_edit, column);
       layout->addWidget (name_edit);
    }
};

class StdStringIO
{
    // typedef std::string T;
    void recall_data (var result, var field)
    {
         result = QString::fromStdString (field);
    }
    void store_data (var field, var value)
    {
         field = value.var.toStdString ();
    }
};

class BoolIO
{
    void recall_data (var result, var field)
    {
         result = field;
    }
    void store_data (var field, var value)
    {
         field = value.var.toBool ();
    }
};

class IntIO
{
    // typedef int T;
    void recall_data (var result, var field)
    {
         result = field;
    }
    void store_data (var field, var value)
    {
         field = value.var.toInt ();
    }
};

class UIntIO
{
    // typedef uint T;
    void recall_data (var result, var field)
    {
         result = field;
    }
    void store_data (var field, var value)
    {
         field = value.toUInt ();
    }
};

class LongIO
{
    // typedef qlonglong T;
    void recall_data (var result, var field)
    {
         result = field;
    }
    void store_data (var field, var value)
    {
         field = value.toLongLong ();
    }
};

class ULongIO
{
    // typedef qulonglong T;
    void recall_data (var result, var field)
    {
         result = field;
    }
    void store_data (var field, var value)
    {
         field = value.toULongLong ();
    }
};

class FloatIO
{
    void recall_data (var result, var field)
    {
         result = field;
    }
    void store_data (var field, var value)
    {
         field = value.toFloat ();
    }
};

class DoubleIO
{
    void recall_data (var result, var field)
    {
         result = field;
    }
    void store_data (var field, var value)
    {
         field = value.toDouble ();
    }
};

/* ---------------------------------------------------------------------- */

} // end of namespace building

using namespace building;

namespace set {

/* ---------------------------------------------------------------------- */

class TableModel : public QAbstractTableModel
{
   public:
      TableModel ( QObject * parent, Collection * root);

      int rowCount (const QModelIndex & parent = QModelIndex ()) const override;
      int columnCount (const QModelIndex & parent = QModelIndex ()) const override;
      QVariant headerData (int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

      QVariant data (const QModelIndex & index, int role) const override;
      bool setData(const QModelIndex & index, const QVariant & value, int role = Qt::EditRole) override;
      Qt::ItemFlags flags (const QModelIndex & index) const override;

      // Qt::DropActions supportedDropActions () const override;
      // QStringList mimeTypes() const override;
      // bool dropMimeData (const QMimeData * data, Qt::DropAction action,
      //                   int row, int column, const QModelIndex & parent) override;

   private:
      Collection * store;
};

/* ---------------------------------------------------------------------- */

class ComboBoxDelegate : public QStyledItemDelegate
{
   public:
      ComboBoxDelegate (QStringList p_item_list, QObject *parent = nullptr);

      QWidget * createEditor (QWidget * parent,
                              const QStyleOptionViewItem & option,
                              const QModelIndex & index) const override;

      void setEditorData (QWidget * editor,
                          const QModelIndex & index) const override;

      void setModelData (QWidget * editor,
                         QAbstractItemModel * model,
                         const QModelIndex & index) const override;

      void updateEditorGeometry (QWidget *editor,
                                 const QStyleOptionViewItem & option,
                                 const QModelIndex &index) const override;
   private:
      QStringList item_list;
};

/* ---------------------------------------------------------------------- */

/*
const int name_col = 0;
const int mode_col = 1;
const int flag_col = 2;
const int number_col = 3;
const int real_col = 4;

const int column_count = 5;
*/

/* ---------------------------------------------------------------------- */

ComboBoxDelegate::ComboBoxDelegate (QStringList p_item_list, QObject * parent) :
   QStyledItemDelegate (parent)
{
   item_list = p_item_list;
}

/* ---------------------------------------------------------------------- */

QWidget *ComboBoxDelegate::createEditor(QWidget * parent,
                                        const QStyleOptionViewItem & option,
                                        const QModelIndex & index) const
{
    QComboBox * editor = new QComboBox (parent);
    editor->setFrame (false);
    editor->addItems (item_list);
    return editor;
}

void ComboBoxDelegate::setEditorData (QWidget * param_editor,
                                      const QModelIndex & index) const
{
    QComboBox * editor = static_cast <QComboBox *> (param_editor);
    int value = index.model()->data(index, Qt::EditRole).var.toInt();
    editor->setCurrentIndex (value);
}

void ComboBoxDelegate::setModelData (QWidget * param_editor,
                                     QAbstractItemModel * model,
                                     const QModelIndex & index) const
{
    QComboBox * editor = static_cast <QComboBox *> (param_editor);
    int value = editor->currentIndex ();
    model->setData (index, value, Qt::EditRole);
}

void ComboBoxDelegate::updateEditorGeometry (QWidget *editor,
                                             const QStyleOptionViewItem & option,
                                             const QModelIndex &/* index */) const
{
    editor->setGeometry (option.rect);
}

/* ---------------------------------------------------------------------- */

TableModel::TableModel (QObject * parent, Collection * root) :
   QAbstractTableModel (parent),
   store (root)
{
}

/* ---------------------------------------------------------------------- */

int TableModel::rowCount (const QModelIndex & parent) const
{
    return store->items.size ();
}

int TableModel::columnCount (const QModelIndex & parent) const
{
   return COLUMN_COUNT;
}

QVariant TableModel::headerData (int section, Qt::Orientation orientation, int role) const
{
   QVariant result;

   if (role == Qt::DisplayRole)
   {
      if (orientation == Qt::Horizontal)
      {
          HEADER_DATA (result, section);
          /*
          if (section == name_col)
             result = "name";
          else if (section == mode_col)
             result = "mode";
          else if (section == flag_col)
             result = "flag";
          else if (section == number_col)
             result = "number";
          else if (section == real_col)
             result = "real number";
          */
      }
   }

   return result;
}

/* ---------------------------------------------------------------------- */

QVariant TableModel::data (const QModelIndex & index, int role) const
{
   QVariant result;

   int line = index.row();
   int col = index.column();
   Record * item = store->items [line];

   if (role == Qt::DisplayRole || role == Qt::EditRole )
   {
      RECALL_DATA (result, item, col);
      /*
      if (col == name_col)
         result = QString::fromStdString (item->name);
      else if (col == mode_col)
         result = item->mode;
      else if (col == flag_col)
         result = item->flag;
      else if (col == number_col)
         result = item->number;
      else if (col == real_col)
         result = item->real;
      */
   }
   else if (role == Qt::CheckStateRole)
   {
      /*
      if (col == 0)
         result = item->flag ? Qt::Checked : Qt::Unchecked;
      */
   }

   return result;
}

bool TableModel::setData (const QModelIndex & index, const QVariant & value, int role)
{
    bool result = false;
    if (role == Qt::EditRole)
    {
        int line = index.row();
        int col = index.column();
        Record * item = store->items [line];

        STORE_DATA (item, col, value);
        /*
        if (col == name_col)
            item->name = value.toString().toStdString();
         else if (col == mode_col)
            item->mode = Mode (value.toInt ());
         else if (col == flag_col)
            item->flag = value.toBool ();
         else if (col == number_col)
            item->number = value.toInt ();
         else if (col == real_col)
            item->real = value.toDouble ();
         */

         result = true;
    }
    return result;
}

Qt::ItemFlags TableModel::flags(const QModelIndex & index) const
{
    return Qt::ItemIsSelectable |  Qt::ItemIsEditable | Qt::ItemIsEnabled ;
}

/* ---------------------------------------------------------------------- */

class TableView : public QTableView
{
   Q_OBJECT
   public:
      TableView (QWidget * parent = NULL);
      void setData (Collection * data);
   private:
      Collection * store;
      TableModel * model;
   private slots:
      void onClick (const QModelIndex & index);
};

/* ---------------------------------------------------------------------- */

TableView::TableView (QWidget * parent) :
   QTableView (parent),
   store (NULL),
   model (NULL)
{
   connect (this, SIGNAL (doubleClicked (const QModelIndex &)), this, SLOT(onClick (const QModelIndex &)));
}

void TableView::setData (Collection * data)
{
   store = data;

   model = new TableModel (this, store);
   setModel (model);

   /*
   QStringList list;
   for (int i = 0; i < modeTableLen; i ++)
       list.append (modeTable [i]);
   */

   /*
   ComboBoxDelegate * delegate = new ComboBoxDelegate (list, this);
   setItemDelegateForColumn (mode_col, delegate);
   */
}

void TableView::onClick (const QModelIndex & index)
{
   int line = index.row ();
   Record * item = store->items [line];

   QDialog * dlg = new QDialog (this);
   dlg->setWindowTitle ("Edit Record");

   QVBoxLayout * layout = new QVBoxLayout (dlg);
   dlg->setLayout (layout);

   QDataWidgetMapper * mapper = new QDataWidgetMapper (this);
   mapper->setModel (model);
   mapper->setSubmitPolicy (QDataWidgetMapper::AutoSubmit);

   /*
   QLineEdit * name_edit = new QLineEdit (dlg);
   mapper->addMapping (name_edit, name_col);
   layout->addWidget (name_edit);

   QComboBox * mode_edit = new QComboBox (dlg);
   for (int i = 0; i < modeTableLen; i++)
      mode_edit->addItem (modeTable [i]);
   mapper->addMapping (mode_edit, mode_col, "currentIndex");
   layout->addWidget (mode_edit);

   QCheckBox * flag_edit = new QCheckBox (dlg);
   mapper->addMapping (flag_edit, flag_col);
   layout->addWidget (flag_edit);

   QSpinBox * number_edit = new QSpinBox (dlg);
   mapper->addMapping (number_edit, number_col);
   layout->addWidget (number_edit);

   QDoubleSpinBox * real_edit = new QDoubleSpinBox (dlg);
   mapper->addMapping (real_edit, real_col);
   layout->addWidget (real_edit);
   */

   QDialogButtonBox * buttonBox = new QDialogButtonBox (QDialogButtonBox::Apply | QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
   connect (buttonBox, SIGNAL (accepted ()), dlg, SLOT (accept ()));
   connect (buttonBox, SIGNAL (rejected ()), dlg, SLOT (reject ()));

   QPushButton * firstButton = buttonBox->addButton ("First", QDialogButtonBox::ActionRole);
   QPushButton * prevButton  = buttonBox->addButton ("Prev", QDialogButtonBox::ActionRole);
   QPushButton * nextButton  = buttonBox->addButton ("Next", QDialogButtonBox::ActionRole);
   QPushButton * lastButton  = buttonBox->addButton ("Last", QDialogButtonBox::ActionRole);

   connect (firstButton, SIGNAL (clicked ()), mapper, SLOT (toFirst ()));
   connect (prevButton, SIGNAL (clicked ()), mapper, SLOT (toPrevious ()));
   connect (nextButton, SIGNAL (clicked ()), mapper, SLOT (toNext ()));
   connect (lastButton, SIGNAL (clicked ()), mapper, SLOT (toLast ()));

   QPushButton * applyButton = buttonBox->button (QDialogButtonBox::Apply);
   connect (applyButton, SIGNAL (clicked ()), mapper, SLOT (submit ()));

   layout->addWidget (buttonBox);
   mapper->setCurrentIndex (line);
   dlg->exec ();
}

/* ---------------------------------------------------------------------- */

} // end of namespace set

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
