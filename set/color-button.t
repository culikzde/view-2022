
/* color-button.t */

/* ---------------------------------------------------------------------- */

class ColorButton : public QToolButton
{
   private:
      QString name;
      QColor color;

   private :
      QPixmap getPixmap ()
      {
         QPixmap pixmap (12, 12);
         pixmap.fill (Qt::transparent);

         QPainter painter (&pixmap);
         painter.setPen (Qt::NoPen);
         painter.setBrush (QBrush (color));
         painter.drawEllipse (0, 0, 12, 12);
         painter.end ();

         return pixmap;
      }

   public :
      ColorButton (QToolBar * parent, QString p_name) :
         QToolButton (parent) // ,
         // name (p_name),
         // color (QColor (name))
      {
         parent->addWidget (this);

         name = p_name;
         color = QColor (name);

         setIcon (QIcon (getPixmap ()));
         setToolTip (name);
     }

   protected:
      void mousePressEvent (QMouseEvent * event)
      {
         if (event->button() == Qt::LeftButton )
         {
            QMimeData * mimeData = new QMimeData;
            mimeData->setColorData (color);
            mimeData->setText (name); // !?

            QDrag * drag = new QDrag (this);
            drag->setMimeData (mimeData);
            drag->setPixmap (getPixmap ());
            drag->setHotSpot (QPoint (-16, -16));

            Qt::DropAction dropAction = drag->exec (Qt::MoveAction | Qt::CopyAction | Qt::LinkAction);
         }
      }
};

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
