
/* dialog-example.t */

/* ---------------------------------------------------------------------- */

struct Basic
{
    QString   name;
};

struct Item : public Basic
{
    bool      selected;
    int       size;
    double    value;
};

/* ---------------------------------------------------------------------- */

DialogModule
{
   Record = Item;
   Dialog = DialogWindow;
}

/* ---------------------------------------------------------------------- */

class MainWindow : public QMainWindow
{
     Item * item
     {
        name = "abc";
        selected = true;
        size = 7;
        value = 3.14;
     }

     DialogWindow * dlg; // keep Python reference

     QVBoxLayout * vlayout
     {
        QPushButton * button
        {
            text = "Show Dialog";
            clicked
            {
               dlg = new DialogWindow (item);
            }
        }
     }
};

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
     QApplication * appl = new QApplication (argc, argv);
     MainWindow * win = new MainWindow;
     win->show ();
     appl->exec ();
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
