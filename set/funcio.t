
/* funcio.t */

/* ---------------------------------------------------------------------- */

namespace plastic {

   class Record;

   void CALL (Record * data);

} // end of namespace plastic

using namespace plastic;

/* ---------------------------------------------------------------------- */

namespace building {

/* ---------------------------------------------------------------------- */

void boolParam (bool & v)
{
   v = readBool ();
}

void intParam (bool & v)
{
   v = readInt ();
}

void doubleParam (double & v)
{
   v = readDouble ();
}

void stringParam (QString & v)
{
   v = readString ();
}

void stdStringParam (string & v)
{
   v = readString ().toStdString();;
}

/* ---------------------------------------------------------------------- */

} // end of namespace building

using namespace building;

/* ---------------------------------------------------------------------- */

namespace set {

/* ---------------------------------------------------------------------- */

void functions (Record * data)
{
   CALL (data);
}

/* ---------------------------------------------------------------------- */

} // end of namespace set

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
