#include "prop-tree.h"

#include <QPen>
#include <QColorDialog>
#include <QVariant>

#include <QHeaderView>
#include <QVBoxLayout>

/* ---------------------------------------------------------------------- */

QWidget * CustomDelegate::createEditor (QWidget * parent,
                                        const QStyleOptionViewItem & option,
                                        const QModelIndex & index) const
{
   CustomEditor * editor = new CustomEditor (parent);

   if (index.column () == 1)
   {
      QVariant value = index.data (Qt::EditRole);
      QString mode = index.data (Qt::UserRole).toString ();

      if (mode == "bool")
      {
          editor->enable_checkbox = true;
      }
      else if (mode == "int")
      {
          editor->enable_numeric = true;
      }
      else if (mode == "double")
      {
          editor->enable_real = true;
      }
      else if (mode == "list")
      {
          editor->enable_list = true;
          editor->list_values = value.toStringList ();
      }
      else if (mode == "color")
      {
         editor->enable_dialog = true;
      }
      else
      {
          editor->enable_text = true;
      }

      QHBoxLayout * layout = new QHBoxLayout (editor);
      layout->setMargin (0);
      layout->setSpacing (0);
      editor->setLayout (layout);

      if (editor->enable_list)
      {
         editor->combo_box = new QComboBox (editor);
         layout->addWidget (editor->combo_box);

         int cnt = editor->list_values.count ();
         for (int inx = 0; inx < cnt; inx++)
            editor->combo_box->addItem (editor->list_values [inx]);
         if (cnt > 0)
            editor->combo_box->setCurrentIndex (0);
      }

      if (editor->enable_checkbox)
      {
         editor->check_box = new QCheckBox (editor);
         layout->addWidget (editor->check_box);
      }

      if (editor->enable_text)
      {
         editor->line_edit = new QLineEdit (editor);
         layout->addWidget (editor->line_edit);
      }

      if (editor->enable_numeric)
      {
          editor->numeric_edit = new QSpinBox (editor);
          editor->numeric_edit->setMaximum (1000);
          layout->addWidget (editor->numeric_edit);
      }

      if (editor->enable_real)
      {
         editor->real_edit = new QDoubleSpinBox (editor);
         layout->addWidget (editor->real_edit);
      }

      if (editor->enable_dialog)
      {
         editor->button = new QPushButton (editor);
         editor->button->setText ("...");
         editor->button->setMaximumWidth (32);
         layout->addWidget (editor->button);

         connect (editor->button, &QPushButton::clicked, editor, &CustomEditor::onDialogClick);
      }
   }

   return editor;
}

void CustomDelegate::setEditorData (QWidget * param_editor,
                                    const QModelIndex & index) const
{
   CustomEditor * editor = dynamic_cast < CustomEditor * > (param_editor);
   editor->node = tree->treeItem (index);

   QVariant value = index.data (Qt::EditRole);

   if (editor->check_box != nullptr)
   {
      editor->check_box->setChecked (value.toBool ());
   }

   if (editor->line_edit != nullptr)
   {
      editor->line_edit->setText (value.toString ());
   }

   if (editor->numeric_edit != nullptr)
   {
      editor->numeric_edit->setValue (value.toInt ());
   }

   if (editor->real_edit != nullptr)
   {
      editor->real_edit->setValue (value.toDouble ());
   }
}

void CustomDelegate::setModelData (QWidget * param_editor,
                                   QAbstractItemModel * model,
                                   const QModelIndex & index) const
{
   CustomEditor * editor = dynamic_cast < CustomEditor * > (param_editor);
   // assert (editor != nullptr);

   if (editor->check_box != nullptr)
   {
      bool val = editor->check_box->isChecked ();
      model->setData (index, val);
   }

   if (editor->line_edit != nullptr)
   {
      QString txt = editor->line_edit->text ();
      model->setData (index, txt);
   }

   if (editor->numeric_edit != nullptr)
   {
      int val = editor->numeric_edit->value ();
      model->setData (index, val);
   }

   if (editor->real_edit != nullptr)
   {
      double val = editor->real_edit->value ();
      model->setData (index, val);
   }
}

/*
void CustomDelegate::updateEditorGeometry (QWidget * param_editor,
                                           const QStyleOptionViewItem & option,
                                           const QModelIndex &index) const
{
    QStyledItemDelegate::updateEditorGeometry (param_editor, option, index);
    // param_editor->setGeometry (param_editor->geometry().adjusted(0, 0, -1, -1));
}

QSize CustomDelegate::sizeHint (const QStyleOptionViewItem & option,
                               const QModelIndex & index) const
{
    return QStyledItemDelegate::sizeHint (option, index) + QSize (16, 16);
}
*/

void CustomEditor::onDialogClick ()
{
    QVariant value = node->data (1, Qt::EditRole);
    QColor color = value.value <QColor> ();

    color = QColorDialog::getColor (color);
    if (color.isValid ())
    {
        // node->setData (Qt::DisplayRole, color);
        node->setData (1, Qt::EditRole, color);
    }
}

/* ---------------------------------------------------------------------- */

PropertyTree::PropertyTree (QWidget * parent) :
   QTreeWidget (parent)
{
   QStringList labels;
   labels << "name" << "value";
   setHeaderLabels (labels);

   setItemDelegateForColumn (1, new CustomDelegate (this));
   setEditTriggers (QAbstractItemView::AllEditTriggers);

   connect (this, SIGNAL (itemChanged), this, SLOT (onItemChanged));
   #if 0
      connect (this, &QTreeWidget::itemChanged, this, &PropertyTree::onItemChanged);
   #endif
}

QTreeWidgetItem * PropertyTree::addTreeItem (QString name)
{
    QTreeWidgetItem * node = new QTreeWidgetItem;
    node->setText (0, name);
    node->setFlags(node->flags() | Qt::ItemIsEditable);
    addTopLevelItem (node);

    return node;
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
