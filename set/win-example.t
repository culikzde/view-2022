/* win-example.t */

#define AREA
#define BUILDER
#define EDIT

#if defined (AREA) || defined (BUILDER)
   #include "color-button.t"
   #include "tool-button.t"
#endif

#include "info.t"
#include "grep.t"
#include "files.t"

#ifdef EDIT
   // #include "edit.t"
   EditModule { }
#endif

#ifdef AREA
   #include "area.t"
#endif

#ifdef BUILDER
   #include "builder.t"
#endif

// #include "construction.t"

/* ---------------------------------------------------------------------- */

#if 0
QAction * openMenuItem
{
    text = "&Open...";
    shortcut = "Ctrl+O";
    // triggered = openFile;
}

QAction * saveMenuItem
{
    text = "&Save...";
    shortcut = "Ctrl+S";
    // triggered { saveFile (); }
}

QAction * grepMenuItem
{
    text = "&Search...";
    shortcut = "F2";
    // triggered = findInFiles;
    // triggered { findInFiles (); }
}

QAction * quitMenuItem
{
    text = "&Quit";
    shortcut = "Ctrl+Q";
    // triggered { this->close (); }
}
#endif

/* ---------------------------------------------------------------------- */

#if 0
QMenuBar * mainMenu
{
    QMenu * fileMenu
    {
        title = "&File";
        openMenuItem;
        saveMenuItem;
        grepMenuItem;
        quitMenuItem;
    }

    QMenu * editMenu
    {
        title = "&Edit";
    }

    QMenu * windowMenu
    {
        title = "&Window";
    }

    QMenu * settingsMenu
    {
        title = "&Setting";
    }

    QMenu * toolsMenu
    {
        title = "&Tools";
    }

    QMenu * helpMenu
    {
        title = "&Help";
    }
}
#endif

/* ---------------------------------------------------------------------- */

#if defined (AREA) || defined (BUILDER)
QToolBar * toolbar
{
    QToolButton * selectButton
    {
        text = "select";
        icon = "window-new";
    }

    QToolButton * resizeButton
    {
        text = "resize";
        icon = "view-refresh";
    }

    QToolButton * connectButton
    {
        text = "connect";
        icon = "go-jump";
    }

    QTabWidget * palette
    {
       QToolBar * colorToolBar
       {
          // text = "Colors";
          setTabText (0, "Colors");
          setTabIcon (0, QIcon::fromTheme ("color-fill"));

          new ColorButton (colorToolBar, "red");
          new ColorButton (colorToolBar, "blue");
          new ColorButton (colorToolBar, "green");
          new ColorButton (colorToolBar, "yellow");
          new ColorButton (colorToolBar, "orange");
          new ColorButton (colorToolBar, "silver");
          new ColorButton (colorToolBar, "gold");
          new ColorButton (colorToolBar, "goldenrod");
          new ColorButton (colorToolBar, "lime");
          new ColorButton (colorToolBar, "lime green");
          new ColorButton (colorToolBar, "yellow green");
          new ColorButton (colorToolBar, "green yellow");
          new ColorButton (colorToolBar, "forest green");
          new ColorButton (colorToolBar, "coral");
          new ColorButton (colorToolBar, "cornflower blue");
          new ColorButton (colorToolBar, "dodger blue");
          new ColorButton (colorToolBar, "royal blue");
          new ColorButton (colorToolBar, "wheat");
          new ColorButton (colorToolBar, "chocolate");
          new ColorButton (colorToolBar, "peru");
          new ColorButton (colorToolBar, "sienna");
          new ColorButton (colorToolBar, "brown");
       }

       QToolBar * shapeToolBar
       {
          // text = "Shapes";
          new ToolButton (shapeToolBar, "area", "", shapeFormat);
          new ToolButton (shapeToolBar, "sub-area", "", shapeFormat);
          new ToolButton (shapeToolBar, "source", "", shapeFormat);
          new ToolButton (shapeToolBar, "target", "", shapeFormat);
          new ToolButton (shapeToolBar, "ellipse", "", shapeFormat);
       }

       #ifdef BUILDER
       QToolBar * componentToolBar
       {
          // text = "Components";
          new ToolButton (componentToolBar, "QPushButton", "", widgetFormat);
          new ToolButton (componentToolBar, "QCheckBox", "", widgetFormat);
          new ToolButton (componentToolBar, "QComboBox", "", widgetFormat);
          new ToolButton (componentToolBar, "QLabel", "", widgetFormat);
          new ToolButton (componentToolBar, "QLineEdit", "", widgetFormat);
          new ToolButton (componentToolBar, "QPlainTextEdit", "", widgetFormat);
          new ToolButton (componentToolBar, "QTextEdit", "", widgetFormat);
          new ToolButton (componentToolBar, "QSpinBox", "", widgetFormat);
          new ToolButton (componentToolBar, "QDoubleSpinBox", "", widgetFormat);
          new ToolButton (componentToolBar, "QTreeWidget", "", widgetFormat);
          new ToolButton (componentToolBar, "QListWidget", "", widgetFormat);
          new ToolButton (componentToolBar, "QTableWidget", "", widgetFormat);
          new ToolButton (componentToolBar, "QTabWidget", "", widgetFormat);
          new ToolButton (componentToolBar, "QWidget", "", widgetFormat);
       }
       #endif
    }
}
#endif

/* ---------------------------------------------------------------------- */

QTreeWidget * tree
{
   QTreeWidgetItem * branch
   {
      QTreeWidgetItem * node1  { text = "red"; foreground = "red"; }
      QTreeWidgetItem * node2  { text = "green"; foreground = "green"; }
      QTreeWidgetItem * node3  { text = "blue"; foreground = "blue"; }

      text = "tree branch";
      foreground = "brown";
      background = "orange";
      icon = "folder";
   }
   expandAll ();
}

/* ---------------------------------------------------------------------- */

#ifdef AREA
AreaView * area
{
}
#endif

#ifdef BUILDER
Builder * builder
{
}
#endif

#ifdef EDIT
Edit * edit
#else
QTextEdit * edit
#endif
{
   int inx = middleTabs->indexOf (edit);
   middleTabs->setTabIcon (inx, QIcon::fromTheme ("window-close"));
   // setPlainText ("abc");
}

GrepView * grep
{
}

FileView * files
{
}

QTableWidget * prop
{
}

// QPlainTextEdit * info
Info * info
{
   // plainText = "info";
}

QStatusBar * status
{
    QLabel * lab
    {
       text = "status bar";
    }
}

/* ---------------------------------------------------------------------- */

QSplitter * vsplitter
{
   orientation = Qt::Vertical;
   QSplitter * hsplitter
   {
      QTabWidget * leftTabs
      {
          setTabPosition (QTabWidget::West);
          grep;
          files;
          tree;
      }
      QTabWidget * middleTabs
      {
          #ifdef AREA
          area;
          #endif

          #ifdef BUILDER
          builder;
          #endif

          edit;
      }
      QTabWidget * rightTabs
      {
          setTabPosition (QTabWidget::East);
          prop;
      }
      setStretchFactor (0, 1);
      setStretchFactor (1, 4);
      setStretchFactor (2, 1);
   }
   info;
   setStretchFactor (0, 3);
   setStretchFactor (1, 1);
}

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
   // windowTitle = "Example";

   #if 1
   QMenuBar * mainMenuStd
   {
       QMenu * fileMenuStd
       {
           title = "&File";
           QAction * openMenuItemStd
           {
               text = "&Open...";
               shortcut = "Ctrl+O";
               triggered = openFile;
           }

           QAction * saveMenuItemStd
           {
               text = "&Save...";
               shortcut = "Ctrl+S";
               triggered { saveFile (); }
           }

           QAction * grepMenuItemStd
           {
               text = "&Search...";
               shortcut = "F2";
               // triggered = findInFiles;
               triggered { findInFiles (); }
           }

           QAction * runMenuItemStd
           {
               text = "&Run...";
               shortcut = "F5";
               triggered = runCommand;
           }

           QAction * quitMenuItemStd
           {
               text = "&Quit";
               shortcut = "Ctrl+Q";
               triggered { close (); }
           }
       }
   }
   #endif

   #if 0
   QMenuBar * mainMenu
   {
       QMenu * fileMenu
       {
           title = "&File";
           openMenuItem;
           saveMenuItem;
           grepMenuItem;
           quitMenuItem;
       }
       QMenu * editMenu
       {
           title = "&Edit";
       }
   }
   #endif

   #if 0
   mainMenu;
   #endif

   #if defined (AREA) || defined (BUILDER)
      toolbar;
   #endif
   vsplitter;
   status;

   public:
     void readFile (QTextEdit * edit, QString fileName);
     QTextEdit * openEditor (QString fileName);

     void openFile ();
     void saveFile ();

     void findInFiles (); // before menu
     void runCommand (); // before menu

};

/* ---------------------------------------------------------------------- */

void Window::readFile (QTextEdit * edit, QString fileName)
{
   QFile f (fileName);
   if (f.open (QFile::ReadOnly))
   {
      QByteArray code = f.readAll ();
      edit->setPlainText (code);
   }
   else
   {
      QMessageBox::warning (null, "Open File Error", "Cannot read file: " + fileName);
   }
}

QTextEdit * Window::openEditor (QString fileName)
{
   QTextEdit * edit = null;
   QFileInfo fi (fileName);
   fileName = fi.absoluteFilePath();
   /*
   if (sourceEditors.contains (fileName))
   {
      edit = sourceEditors [fileName];
   }
   else
   {
   }
   */
   #ifdef EDIT
      edit = new Edit (this);
   #else
      edit = new QTextEdit (this);
   #endif
   edit->setToolTip (fileName);
   middleTabs->addTab (edit, fi.fileName ());
   // sourceEditors [fileName] = edit;
   readFile (edit, fileName);

   return edit;
}

void Window::openFile ()
{
    QString fileName = QFileDialog::getOpenFileName (this, "Open text file");
    if (fileName != "")
    {
       openEditor (fileName);
    }
}

void Window::saveFile ()
{
    QString fileName = QFileDialog::getSaveFileName (this, "Save text file");
    if (fileName != "")
    {
    }
}

void Window::findInFiles ()
{
    new GrepDialog (grep);
}

void Window::runCommand ()
{
    info->runCommand ("gcc -c _output/win_output.cpp `pkg-config Qt5Widgets --cflags`");
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
   // cout << "Hello"; //  << "Qt";
   QIcon::setThemeName ("oxygen");
   QApplication * appl = new QApplication (argc, argv);
   Window * win = new Window ();
   win->show ();
   appl->exec ();
}


// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
