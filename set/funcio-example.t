
/* funcio-example.t */

/* ---------------------------------------------------------------------- */

struct Item
{
    void add (int i);
    void mult (double x, double y);
};

/* ---------------------------------------------------------------------- */

FuncIO
{
    Record = Item;
}

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
