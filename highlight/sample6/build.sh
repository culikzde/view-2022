#!/bin/sh

use_sip6=false

if test -f /etc/arch-release ; then
   use_sip6=true
   # use_sip6=false
fi

if $use_sip6 ; then

   # sip-build --verbose
   sip-install --verbose --no-distinfo --target-dir=.
   # required file pyproject.toml
   python run.py

   # pacman -S sip (python-pyqt6-sip) python-pyqt6
   # conflict with sip4 python-sip4


else

   test -f Makefile && make clean
   python configure.py || exit 1
   make || exit 1
   python run.py

   # pacman -S sip4 python-sip4 (python-pyqt6-sip) python-pyqt6 pkgconf

fi
