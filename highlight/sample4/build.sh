#!/bin/sh

test -f Makefile && make clean
python2 configure.py || exit 1
# python configure.py || exit 1
make || exit 1
python2 run.py
# python run.py

# dnf install sip-devel PyQt4-devel

# apt-get install qt4-dev python-qt4-dev pkg-config

# ArchLinux AUR : sip4 python2-sip qt4 (pyqt4-common) (python2-sip-pyqt4) python2-pyqt4 pkgconf

# ArchLinux AUR, Python 3, PyQt4: sip4 python-sip4 qt4 (pyqt4-common) (python-sip-pyqt4) python-pyqt4 pkgconf

# git clone https://aur.archlinux.org/yay.git
# cd yay/
# makepkg -si

# yay -S qt4
# yay -S python2-dbus
# yay -S python2-xlib

# yay -S python2-pyqt4
# yay -S python2-sip-pyqt4
# yay -S python2-sip
