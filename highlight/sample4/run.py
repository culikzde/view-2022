#!/bin/env python

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import sys
import sample

class Window (QTextEdit) :

   def __init__ (self, parent = None) :
       super (Window, self).__init__ (parent)

       obj = sample.Sample ()
       text = "Answer is " + str (obj.question ())

       self.setText (text)

app = QApplication (sys.argv)
win = Window ()

win.show ()

obj = sample.Sample ()
print ("Answer is", obj.question ())

app.exec_ ()
