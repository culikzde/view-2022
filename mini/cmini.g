
/* cmini.g */

/* --------------------------------------------------------------------- */

< struct CmmBasic { 
                     int src_file;
                     int src_line;
                     int src_column;
                     // int src_pos;
                     // int src_end;
                  } >

< struct CmmExpr { 
                    enum CmmExprKind { };
                    CmmExprKind kind; 
                    CmmDecl * item_decl;
                    ptr item_type; // !?
                  } >

< struct CmmName : CmmExpr { } >
< struct CmmUnaryExpr : CmmExpr {  } >
< struct CmmBinaryExpr : CmmExpr {  } >

< struct CmmStat { enum CmmStatMode { }; CmmStatMode mode; } >

< struct CmmDecl { enum CmmDeclMode { }; CmmDeclMode mode; } >

< struct CmmDeclarator { enum CmmDeclaratorKind { }; CmmDeclaratorKind kind; } >
< struct CmmPtrSpecifier { enum CmmPtrKind { }; CmmPtrKind kind; } >
< struct CmmContSpecifier { enum CmmContKind { }; CmmContKind kind; } >

/* --------------------------------------------------------------------- */

< group expr rewrite ( CmmExpr ) >

< group stat reuse ( CmmStat )
             include ( stat )
             exclude ( nested_stat ) >
             
< group decl reuse ( CmmDecl )
             include ( declaration )
             exclude ( parameter_declaration ) >

/* --------------------------------------------------------------------- */

< literals // identifiers for literals
   LPAREN     : '(' ;
   RPAREN     : ')' ;
   LBRACKET   : '[' ;
   RBRACKET   : ']' ;
   LBRACE     : '{' ;
   RBRACE     : '}' ;

   DOT        : '.'  ;
   ARROW      : "->" ;
   SCOPE      : "::" ;

   LOG_NOT    : '!' ;
   BIT_NOT    : '~' ;

   INC        : "++"  ;
   DEC        : "--"  ;

   DOT_STAR   : ".*"  ;
   ARROW_STAR : "->*" ;

   STAR       : '*' ;
   SLASH      : '/' ;
   MOD        : '%' ;

   PLUS       : '+' ;
   MINUS      : '-' ;

   SHL        : "<<" ;
   SHR        : ">>" ;

   LESS             : '<'  ;
   GREATER          : '>'  ;
   LESS_OR_EQUAL    : "<=" ;
   GREATER_OR_EQUAL : ">=" ;

   EQUAL            : "==" ;
   UNEQUAL          : "!=" ;

   BIT_AND    : '&' ;
   BIT_XOR    : '^' ;
   BIT_OR     : '|' ;

   LOG_AND    : "&&" ;
   LOG_OR     : "||" ;

   ASSIGN         : '='   ;
   MUL_ASSIGN     : "*="  ;
   DIV_ASSIGN     : "/="  ;
   MOD_ASSIGN     : "%="  ;
   PLUS_ASSIGN    : "+="  ;
   MINUS_ASSIGN   : "-="  ;
   SHL_ASSIGN     : "<<=" ;
   SHR_ASSIGN     : ">>=" ;
   BIT_AND_ASSIGN : "&="  ;
   BIT_XOR_ASSIGN : "^="  ;
   BIT_OR_ASSIGN  : "|="  ;

   QUESTION   : '?' ;
   COMMA      : ',' ;

   COLON      : ':' ;
   SEMICOLON  : ';' ;
   DOTS       : "..." ;
>

/*
identifier
number
real_number
character_literal
string_literal
*/

/* --------------------------------------------------------------------- */

/* simple name */

simple_name <CmmSimpleName:CmmName, kind=simpleName>:
   id:identifier ; // only identifier

/* --------------------------------------------------------------------- */

/* primary expression */

primary_expr <select CmmExpr>:
   ident_expr |
   int_value |
   real_value |
   char_value |
   string_value |
   subexpr_expr ;

ident_expr <return CmmName>:
   simple_name ;

int_value <CmmIntValue:CmmExpr, kind=intValue>:
   value:number ;

real_value <CmmRealValue:CmmExpr, kind=realValue>:
   value:real_number ;

char_value <CmmCharValue:CmmExpr, kind=charValue>:
   value:character_literal ;

string_value <CmmStringValue:CmmExpr, kind=stringValue>:
   value:string_literal
   ( <add> string_value_cont )* ; // !?

string_value_cont <CmmStringCont>:
   value:string_literal ;

subexpr_expr <CmmSubexprExpr:CmmExpr, kind=subexprExp>:
   '(' param:expr ')' ;

/* postfix expression */

postfix_expr <choose CmmExpr>:
   primary_expr
   (
      index_expr |
      call_expr |
      field_expr |
      ptr_field_expr |
      post_inc_expr |
      post_dec_expr
   )* ;

index_expr <CmmIndexExpr:CmmExpr, kind=indexExp>:
   <store left:CmmExpr>
   '[' param:assignment_expr ']' ; // !?

call_expr <CmmCallExpr:CmmExpr, kind=callExp>:
   <store left:CmmExpr>
   '(' param_list:expr_list ')' ;

field_expr <CmmFieldExpr:CmmExpr, kind=fieldExp>:
   <store left:CmmExpr>
   <no_space> '.' <no_space>
   simp_name:simple_name ;

ptr_field_expr <CmmPtrFieldExpr:CmmExpr, kind=ptrFieldExp>:
   <store left:CmmExpr>
   <no_space> "->" <no_space>
   simp_name:simple_name ;

post_inc_expr <CmmPostIncExpr:CmmExpr, kind=postIncExp>:
   <store left:CmmExpr>
   "++" ;

post_dec_expr <CmmPostDecExpr:CmmExpr, kind=postDecExp>:
   <store left:CmmExpr>
   "--" ;

expr_list <CmmExprList>:
   (
       <add> assignment_expr
       ( ',' <add> assignment_expr )*
   )? ;

/* unary expression */

unary_expr <select CmmExpr>:
   postfix_expr |
   inc_expr |
   dec_expr |
   deref_expr |
   addr_expr |
   plus_expr |
   minus_expr |
   bit_not_expr |
   log_not_expr |
   sizeof_expr |
   allocation_expr |
   deallocation_expr ;

inc_expr <CmmIncExpr:CmmUnaryExpr, kind=incExp>:
   "++" param:cast_expr ;

dec_expr <CmmDecExpr:CmmUnaryExpr, kind=decExp>:
   "--" param:cast_expr ;

deref_expr <CmmDerefExpr:CmmUnaryExpr, kind=derefExp>:
   '*' param:cast_expr ;

addr_expr <CmmAddrExpr:CmmUnaryExpr, kind=addrExp>:
   '&' param:cast_expr ;

plus_expr <CmmPlusExpr:CmmUnaryExpr, kind=plusExp>:
   '+' param:cast_expr ;

minus_expr <CmmMinusExpr:CmmUnaryExpr, kind=minusExp>:
   '-' param:cast_expr ;

bit_not_expr <CmmBitNotExpr:CmmUnaryExpr, kind=bitNotExp>:
   '~' param:cast_expr ;

log_not_expr <CmmLogNotExpr:CmmUnaryExpr, kind=logNotExp>:
   '!' param:cast_expr ;

sizeof_expr <CmmSizeofExp:CmmUnaryExpr, kind=sizeofExp>:
   "sizeof"
    value:unary_expr ;

/* new */

allocation_expr <CmmNewExpr:CmmUnaryExpr, kind=newExp>:
   "new"
   type_spec:type_specifiers
   ptr:ptr_specifier_list
   ( <add> allocation_array_limit )* ;

allocation_array_limit <CmmNewArrayLimit>:
   '[' value:expr ']'  ;

/* delete */

deallocation_expr <CmmDeleteExpr:CmmUnaryExpr, kind=deleteExp>:
   "delete"
   ( '[' ']' <set a_array = true> )?
   param:cast_expr ;

/* cast expression */

cast_expr <select CmmExpr>:
      cast_formula
   |
      unary_expr ;

cast_formula <CmmTypeCastExpr: CmmExpr, kind=typecastExp>:
   "type_cast" // !?
   '<' type:type_id '>'
   '(' param:cast_expr ')' ;

/* pm expression */

pm_expr <return CmmExpr>:
   cast_expr ;

/* arithmetic and logical expressions */

multiplicative_expr <choose CmmExpr>:
   pm_expr
   (
      <new CmmMulExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      ( '*' <set kind=mulExp> |
        '/' <set kind=divExp> |
        '%' <set kind=modExp> )
      right:pm_expr
   )* ;

additive_expr <choose CmmExpr>:
   multiplicative_expr
   (
      <new CmmAddExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      ( '+' <set kind=addExp> |
        '-' <set kind=subExp> )
      right:multiplicative_expr
   )* ;

shift_expr <choose CmmExpr>:
   additive_expr
   (
      <new CmmShiftExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      ( "<<" <set kind=shlExp> |
        ">>" <set kind=shrExp> )
      right:additive_expr
   )* ;

relational_expr <choose CmmExpr>:
   shift_expr
   (
      <new CmmRelExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      ( '<'  <set kind=ltExp> |
        '>'  <set kind=gtExp> |
        "<=" <set kind=leExp> |
        ">=" <set kind=geExp> )
      right:shift_expr
   )* ;

equality_expr <choose CmmExpr>:
   relational_expr
   (
      <new CmmEqExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      ( "==" <set kind=eqExp> |
        "!=" <set kind=neExp> )
      right:relational_expr
   )* ;

and_expr <choose CmmExpr>:
   equality_expr
   (
      <new CmmAndExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      '&' <set kind=bitAndExp>
      right:equality_expr
   )* ;

exclusive_or_expr <choose CmmExpr>:
   and_expr
   (
      <new CmmXorExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      '^' <set kind=bitXorExp>
      right:and_expr
   )* ;

inclusive_or_expr <choose CmmExpr>:
   exclusive_or_expr
   (
      <new CmmOrExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      '|' <set kind=bitOrExp> 
      right:exclusive_or_expr
   )* ;

logical_and_expr <choose CmmExpr>:
   inclusive_or_expr
   (
      <new CmmAndAndExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      "&&" <set kind=logAndExp>
      right:inclusive_or_expr
   )* ;

logical_or_expr <choose CmmExpr>:
   logical_and_expr
   (
      <new CmmOrOrExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      "||" <set kind=logOrExp>
      right:logical_and_expr
   )* ;

/* conditional and assignment expression */

assignment_expr <choose CmmExpr>:
   logical_or_expr // !?
   (
      <new CmmAssignExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      ( '='   <set kind=assignExp>    |
        "+="  <set kind=addAssignExp> |
        "-="  <set kind=subAssignExp> |
        "*="  <set kind=mulAssignExp> |
        "/="  <set kind=divAssignExp> |
        "%="  <set kind=modAssignExp> |
        "<<=" <set kind=shlAssignExp> |
        ">>=" <set kind=shrAssignExp> |
        "&="  <set kind=andAssignExp> |
        "^="  <set kind=xorAssignExp> |
        "|="  <set kind=orAssignExp>  |
        '?'   <set kind=condExp> middle:assignment_expr ':' )
      right:logical_or_expr // !?
   )? ;

/* expression */

expr <choose CmmExpr>:
   assignment_expr
   (
      <new CmmCommaExpr:CmmBinaryExpr>
      <store left:CmmExpr>
      ',' <set kind=commaExp>
      right:assignment_expr
   )* ;

/* constant expression */

const_expr <return CmmExpr>:
   assignment_expr ;

/* --------------------------------------------------------------------- */

/* statement */

stat <select CmmStat>:

   local_declaration |
   simple_stat |
   empty_stat |
   compound_stat |
   if_stat |
   while_stat |
   do_stat |
   for_stat |
   switch_stat |
   case_stat |
   default_stat |
   break_stat |
   continue_stat |
   return_stat |
   goto_stat ;

nested_stat <return CmmStat>: /* statement with different output indentation */
   <indent>
   stat
   <unindent> ;

empty_stat <CmmEmptyStat:CmmStat, mode=emptyStat>:
   ';' ;

compound_stat <CmmCompoundStat:CmmStat, mode=compoundStat>:
   '{'
   <indent>
   ( <new_line> <add> stat )*
   <unindent>
   '}' ;

if_stat <CmmIfStat:CmmStat, mode=ifStat>:
   "if"
   '(' cond:expr ')'
   then_stat:nested_stat
   (
      <silent>
      <new_line>
      "else"
      else_stat:nested_stat
   )? ;

while_stat <CmmWhileStat:CmmStat, mode=whileStat>:
   "while"
   '(' cond:expr ')'
   body:nested_stat ;

do_stat <CmmDoStat:CmmStat, mode=doStat>:
   "do"
   body:nested_stat
   "while" '(' cond_expr:expr ')' ';' ;

for_stat <CmmForStat:CmmStat, mode=forStat>:
   "for"
   '('
   ( from_expr:expr )?
   (
      ':'
      iter_expr:expr
   |
      ';'
      (cond_expr:expr)?
      ';'
      (step_expr:expr)?
   )
   ')'
   body:nested_stat ;

switch_stat <CmmSwitchStat:CmmStat, mode=switchStat>:
   "switch"
   '(' cond:expr ')'
   body:nested_stat ;

case_stat <CmmCaseStat:CmmStat, mode=caseStat>:
   "case" case_expr:expr ':'
   <new_line>
   body:nested_stat ;

default_stat <CmmDefaultStat:CmmStat, mode=defaultStat>:
   "default" ':'
   <new_line>
   body:nested_stat ;

break_stat <CmmBreakStat:CmmStat, mode=breakStat>:
   "break" ';' ;

continue_stat <CmmContinueStat:CmmStat, mode=continueStat>:
   "continue" ';' ;

return_stat <CmmReturnStat:CmmStat, mode=returnStat>:
   "return" (return_expr:expr)? ';' ;

goto_stat <CmmGotoStat:CmmStat, mode=gotoStat>:
   "goto" goto_lab:identifier ';' ;

/* simple statement */

simple_stat <CmmSimpleStat:CmmStat, mode=simpleStat>:
   inner_expr:expr
   ';' ;

local_declaration <CmmLocalStat:CmmStat, mode=localStat> :
   "dcl"
   inner_decl:simple_declaration ;

/* ---------------------------------------------------------------------- */

/* declaration specifiers */

decl_specifiers <CmmDeclSpec:CmmExpr, kind=declSpec>:
   (
      "extern"   <set a_extern = true>   |
      "static"   <set a_static = true>   |
      "auto"     <set a_auto = true>     |
      "register" <set a_register = true>
   )* 
   param:type_specifiers ;

/* const / volatile */

const_specifier <CmmConstSpecifier:CmmExpr, kind=constSpec>:
   "const"
   param:type_specifiers ;

volatile_specifier <CmmVolatileSpecifier:CmmExpr, kind=volatileSpec>:
   "volatile"
   param:type_specifiers ;

/* type specifier */

type_specifier <CmmTypeSpec:CmmExpr, kind=typeSpec>:
   (
      "signed" <set a_signed = true>
   |
      "unsigned" <set a_unsigned = true>
   )?
   (
      "bool"     <set a_bool = true>   |
      "char"     <set a_char = true>   |
      "wchar_t"  <set a_wchar = true>  |
      "short"    <set a_short=true>    |
      "int"      <set a_int = true>    |
      "long"     <set a_long=true>     |
      "float"    <set a_float = true>  |
      "double"   <set a_double = true> |
      "void"     <set a_void = true>
   ) ;

/* type specifiers */

type_specifiers <select CmmExpr>:
   ident_expr | 
   type_specifier |
   const_specifier |
   volatile_specifier ;

/* ---------------------------------------------------------------------- */

ptr_specifier_list <CmmPtrSpecifierSect>:
   ( <add> ptr_specifier )* ;

ptr_specifier <select CmmPtrSpecifier>:
   pointer_specifier |
   reference_specifier ;

/* pointer specifiers */

pointer_specifier <CmmPointerSpecifier:CmmPtrSpecifier, kind=pointerSpec>:
   '*'
   cv_spec:cv_specifier_list ;

/* reference specifiers */

reference_specifier <CmmReferenceSpecifier:CmmPtrSpecifier, kind=referenceSpec>:
   '&'
   cv_spec:cv_specifier_list ;

cv_specifier_list <CmmCvSpecifier>:
   (
      "const" <set cv_const = true>
   |
      "volatile" <set cv_volatile = true>
   )* ;

/* ---------------------------------------------------------------------- */

cont_specifier_list <CmmContSpecifierSect>:
   ( <add> cont_specifier )* ;

cont_specifier <select CmmContSpecifier>:
   array_specifier |
   function_specifier ;

/* array specifier */

array_specifier <CmmArraySpecifier:CmmContSpecifier, kind=arraySpec>:
   '['
   ( lim:expr )?
   ']' ;

/* function specifier */

function_specifier <CmmFunctionSpecifier:CmmContSpecifier, kind=functionSpec>:
   '('
   parameters:parameter_declaration_list
   ')' ;

/* parameter declarations */

parameter_declaration_list <CmmParamSect>:
   (
      <add> parameter_declaration
      ( ',' <add> parameter_declaration )*
   )? ;

parameter_declaration <CmmParam>:
      type_spec:type_specifiers
      declarator:common_declarator
      ( '=' init:expr )? 
   ;

/* ---------------------------------------------------------------------- */

/* declarator */

basic_declarator <CmmBasicDeclarator:CmmDeclarator, kind=basicDeclarator>:
   <store ptr_spec:CmmPtrSpecifierSect>
   qual_name:simple_name 
   cont_spec:cont_specifier_list ;

empty_declarator <CmmEmptyDeclarator:CmmDeclarator, kind=emptyDeclarator>:
   <store ptr_spec:CmmPtrSpecifierSect>
   cont_spec:cont_specifier_list ;

nested_declarator <CmmNestedDeclarator:CmmDeclarator, kind=nestedDeclarator>:
   <store ptr_spec:CmmPtrSpecifierSect>
   '(' 
   inner_declarator:common_declarator 
   ')' 
   cont_spec:cont_specifier_list ;

/* simple declarator */

simple_declarator <choose CmmDeclarator>:
   ptr_specifier_list
   (
      basic_declarator
   );

/* common declarator */

common_declarator <choose CmmDeclarator>:
   ptr_specifier_list
   (
      basic_declarator |
      empty_declarator 
   );

/* typedef declarator */

typedef_declarator <choose CmmDeclarator>:
   ptr_specifier_list
   (
      basic_declarator |
      nested_declarator 
   );

/* ---------------------------------------------------------------------- */

/* type id */

type_id <CmmTypeId>:
   type_spec:type_specifiers
   declarator:common_declarator ;

/* ---------------------------------------------------------------------- */

/* simple declaration */

simple_declaration <CmmSimpleDecl:CmmDecl, mode=simpleDecl>:
   type_spec:type_specifiers
   <add> simple_item
   <execute on_simple_item>
   (
      ','
      <add> simple_item
      <execute on_simple_item>
   )*
   (
      <execute on_open_function>
      <new_line>
      body:compound_stat
      <execute on_close_function>
   |
      ';'
   ) ;

simple_item <CmmSimpleItem>:
   declarator:simple_declarator
   ( '=' init:expr )? ;

/* ---------------------------------------------------------------------- */

/* declaration */

declaration_list <CmmDeclSect> <start>:
   ( <add> declaration <new_line> )* ;

declaration <select CmmDecl>:

   simple_declaration |
   empty_stat |

   struct_declaration |
   enum_declaration |

   typedef_declaration ;

/* ---------------------------------------------------------------------- */

/* typedef */

typedef_declaration <CmmTypedefDecl:CmmDecl, mode=typedefDecl>:
   "typedef"
   type_spec:type_specifiers
   declarator:typedef_declarator
   <execute on_typedef>
   ';' ;

/* ---------------------------------------------------------------------- */

/* enum */

enum_declaration <CmmEnumDecl:CmmDecl, mode=enumDecl>:
   "enum"
   simp_name:simple_name
   <execute on_enum>
   <new_line>
   '{'
      <execute on_open_enum>
      <indent>
      enum_items:enum_list
      <unindent>
      <new_line>
      <execute on_close_enum>
   '}'
    ';' ;

enum_list <CmmEnumSect>:
   (
      <add> enumerator
      ( ',' <add> enumerator )*
   )? ;

enumerator <CmmEnumItem>:
   <new_line>
   simp_name:simple_name
   <execute on_enum_item> ;

/* ---------------------------------------------------------------------- */

/* struct */

struct_declaration <CmmStructDecl:CmmDecl, mode=structDecl>:
   "struct"
   simp_name:simple_name
   <execute on_class>
   (
      <new_line>
     '{'
        <execute on_open_class>
        <indent>
        members:declaration_list
        <unindent>
        <execute on_close_class>
     '}'
   )?
   ';' ;

/* ---------------------------------------------------------------------- */

/* kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all */
