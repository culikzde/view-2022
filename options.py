#!/usr/bin/env python

from __future__ import print_function

import sys, os, re, inspect

from util import import_qt_modules
import_qt_modules (globals ())

from util import findColor, findIcon, use_qt4, use_pyqt6, use_pyside6, qstring_to_str, variant_to_str, dialog_to_str, menu_exec, setResizeMode

import util

from prop import ObjectBrowser

from tools import SettingsData

# --------------------------------------------------------------------------

class OptionsBox (QWidget) :
   def __init__ (self) :
       super (OptionsBox, self).__init__ ()
       self.tab_layout = QFormLayout ()
       self.setLayout (self.tab_layout)

   def addWidgetItem (self, title, widget) :
       self.tab_layout.addRow (title, widget)

   def addLayoutItem (self, title, layout) :
       self.tab_layout.addRow (title, layout)

# --------------------------------------------------------------------------

class OptionsLineEditBox (QHBoxLayout) :
   def __init__ (self, win, dlg, settings_id, default_value, file_dialog = False) :
       super (OptionsLineEditBox, self).__init__ ()
       self.win = win
       self.settings_id = settings_id

       self.edit = QLineEdit ()

       self.addWidget (self.edit)
       self.setStretch (0, 1)

       self.button = None
       if file_dialog :
          self.button = QPushButton ()
          self.button.setText ("...")
          self.addWidget (self.button)
          self.button.clicked.connect (self.buttonClick)
          self.setStretch (1, 0)

       # recall value
       value = self.win.settings.string (settings_id, default_value)
       self.edit.setText (value)

       dlg.accepted.connect (self.storeValue)

   def buttonClick (self) :
       value = dialog_to_str (QFileDialog.getOpenFileName ())
       if value != "" :
          self.edit.setText (value)

   def storeValue (self) :
       value = str (self.edit.text ())
       value = value.strip ()
       self.win.settings.setValue (self.settings_id, value)

# --------------------------------------------------------------------------

class OptionsComboBox (QHBoxLayout) :
   def __init__ (self, win, dlg, settings_id, default_value, values, file_dialog = False) :
       super (OptionsComboBox, self).__init__ ()
       self.win = win
       self.settings_id = settings_id

       self.combo = QComboBox ()
       self.combo.setEditable (True)
       self.combo.setInsertPolicy (self.combo.InsertAtBottom)
       self.combo.sizeAdjustPolicy ()
       self.addWidget (self.combo)
       self.setStretch (0, 1)

       self.button = None
       if file_dialog :
          self.button = QPushButton ()
          self.button.setText ("...")
          self.addWidget (self.button)
          self.button.clicked.connect (self.buttonClick)
          self.setStretch (1, 0)

       # recall value
       value = self.win.settings.string (settings_id, default_value)
       self.combo.setCurrentText (value)
       self.combo.addItem (value)

       for value in values :
           self.combo.addItem (value)

       dlg.accepted.connect (self.storeValue)

   def buttonClick (self) :
       value = dialog_to_str (QFileDialog.getOpenFileName ())
       if value != "" :
          self.combo.setCurrentText (value)

   def storeValue (self) :
       value = str (self.combo.currentText ())
       value = value.strip ()
       self.win.settings.setValue (self.settings_id, value)

       for inx in range (self.combo.count()) :
           value = str (self.combo.itemText (inx))
           value = value.strip ()
           self.win.settings.setValue (self.settings_id + "." + str (inx), value)

# --------------------------------------------------------------------------

class OptionsCheckBox (QCheckBox) :
   def __init__ (self, win, dlg, settings_id, default_value) :
       super (OptionsCheckBox, self).__init__ (dlg)
       self.win = win
       self.settings_id = settings_id

       # recall value
       value = self.win.settings.boolean (settings_id, bool (default_value))
       self.setChecked (value)

       dlg.accepted.connect (self.storeValue)

   def storeValue (self) :
       value = self.isChecked ()
       self.win.settings.setValue (self.settings_id, value)

# --------------------------------------------------------------------------

class OptionDialog (QDialog) :
   def __init__ (self, win) :
       super (OptionDialog, self).__init__ (win)

       self.win = win
       self.page_count = 0
       self.current_tab = None
       self.page_dict = { }

       "toolBar with buttons"
       self.toolBar = QToolBar ()
       self.toolBar.setOrientation (Qt.Vertical)

       "stackWidget for option pages"
       self.stackWidget = QStackedWidget ()

       "toolBar and stackWidget - central dialog area"
       self.hlayout = QHBoxLayout ()
       self.hlayout.addWidget (self.toolBar)
       self.hlayout.addWidget (self.stackWidget)

       self.buttonBox = QDialogButtonBox (QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
       self.buttonBox.accepted.connect (self.accept)
       self.buttonBox.rejected.connect (self.reject)

       "ok and cancel at the bottom"
       self.vlayout = QVBoxLayout ()
       self.vlayout.addLayout (self.hlayout)
       self.vlayout.addWidget (self.buttonBox)
       self.setLayout (self.vlayout)

   def addPage (self, name, icon, widget, create_widget = None) :

       if isinstance (icon, str) :
          icon = findIcon (icon)

       if widget != None :
          self.stackWidget.addWidget (widget)

       button = QPushButton ()
       button.setText (name)
       if icon != None :
          button.setIcon (icon)
          button.setIconSize (QSize (24, 24))
       # button.setMinimumSize (80, 80)
       button.setMinimumSize (80, 64)

       button.widget = widget
       button.create_widget = create_widget

       button.clicked.connect (lambda param, self=self, button=button: self.openPage (button))
       self.toolBar.addWidget (button)
       self.page_dict [name] = widget

   def pagePresent (self, name) :
       return name in self.page_dict

   def openPage (self, button) :
       if button.create_widget != None :
          if button.widget == None  :
             button.widget = button.create_widget ()
             self.stackWidget.addWidget (button.widget)
          else :
             self.stackWidget.setCurrentWidget (button.widget) # show empty page
             self.show ()
             button.create_widget (button.widget)
          button.create_widget = None # do not call again
       self.stackWidget.setCurrentWidget (button.widget)

   def addSimplePage (self, name, icon) :
       box = OptionsBox ()
       box.win = self.win
       self.addPage (name, icon, box)
       self.current_page = None
       self.current_tab = box
       return box

   def addTabPage (self, name, icon) :
       page = QTabWidget ()
       self.addPage (name, icon, page)
       self.current_page = page
       self.current_tab = None
       return page

   def addTab (self, name) :
       box = OptionsBox ()
       box.win = self.win
       self.current_page.addTab (box, name)
       self.current_tab = box
       return box

   def addLineEdit (self, settings_id, title, default_value, file_dialog = False) :
       layout = OptionsLineEditBox (self.win, self, settings_id, default_value, file_dialog)
       self.current_tab.addLayoutItem (title, layout)

   def addComboBox (self, settings_id, title, default_value, values, file_dialog = False) :
       layout = OptionsComboBox (self.win, self, settings_id, default_value, values, file_dialog)
       self.current_tab.addLayoutItem (title, layout)

   def addCheckBox (self, settings_id, title, default_value) :
       widget = OptionsCheckBox (self.win, self, settings_id, default_value)
       self.current_tab.addWidgetItem (title, widget)

# --------------------------------------------------------------------------

class Options (OptionDialog) :
   def __init__ (self, win) :
       super (Options, self).__init__ (win)

       self.setWindowTitle ("Options")

       # self.examplePage1 ()
       # self.examplePage2 ()

       page_list = self.win.conf.page_list


       for page in page_list :
           tabs = 0
           widgets = 0
           for item in page.items :
               if hasattr (item, "_tab_") :
                  tabs = tabs + 1
               else :
                  widgets = widgets + 1

           if tabs != 0 and widgets == 0 :
              self.addTabPage (page.name, page.icon)
              for tab in page.items :
                  self.addTab (tab.name)
                  self.addItems (page, tab)

           if widgets != 0 and tabs == 0:
              self.addSimplePage (page.name, page.icon)
              for item in page.items :
                  self.addItem (page, None, item)

           if tabs != 0 and widgets != 0 :
              self.addTabPage (page.name, page.icon)
              common = self.addTabPage ("", "")
              for item in page.items :
                  if hasattr (item, "_tab_") :
                     tab = item
                     self.addTab (tab.name)
                     self.addItems (page, tab)
                  else :
                     self.addItem (page, common, item) # !?

   def addItems (self, page, tab) :
       for item in tab.items :
           self.addItem (page, tab, item)

   def addItem (self, page, tab, item) :
           if tab == None or tab.name == "" :
              settings_id = page.name + "/" + item.name
           else :
              settings_id = page.name + "/" + tab.name + "." + item.name
           if hasattr (item, "_check_box_") :
              self.addCheckBox (settings_id, item.name, item.value)
           elif hasattr (item, "_combo_box_") :
              self.addComboBox (settings_id, item.name, item.value, item.values, file_dialog = item.file_dialog)
           else :
              self.addLineEdit (settings_id, item.name, item.value, file_dialog = item.file_dialog)

   def examplePage1 (self) :

       page = self.addTabPage  ("edit", "edit-undo")

       tab1 = self.addTab  ("first")

       edit = QLineEdit ()
       tab1.addWidgetItem ("edit", edit)

       checkBox = QCheckBox ()
       tab1.addWidgetItem ("something", checkBox)

       tab2 = self.addTab ("second")

   def examplePage2 (self) :
       page = self.addSimplePage ("other", "folder-new")

       edit = QLineEdit ()
       page.addWidgetItem ("edit", edit)

       checkBox = QCheckBox ()
       page.addWidgetItem ("something", checkBox)

   """
       ini = self.win.commands # read options from static tools.ini
       self.readPages (ini)

   def readPages (self, ini) :
       for group in ini.groupsWithPrefix ("page") :
           name = ini.removePrefix ("page", group)
           ini.beginGroup (group)
           name = ini.string ("name", name)
           icon = ini.string ("icon")
           self.addTabPage (name, icon)
           self.readTabs (ini)
           ini.endGroup ()

   def readTabs (self, ini) :
       for group in ini.groupsWithPrefix ("tab") :
           name = ini.removePrefix ("tab", group)
           ini.beginGroup (group)
           name = ini.string ("name", name)
           tab = self.addTab (name)
           self.readItems (ini)
           ini.endGroup ()

   def readItems (self, ini) :
       for group in ini.childGroups () :
           ini.beginGroup (group)
           name = ini.string ("name", group)
           value = ini.string ("value")
           file_dialog = ini.boolean ("file_dialog")
           settings_id = ini.group () + "/" + "value"
           settinds_id = ini.string ("settings_id", settings_id) # write options to options.ini
           check_box = ini.boolean ("check_box")
           if check_box :
              self.addCheckBox (settings_id, name, value)
           else :
              self.addLineEdit (settings_id, name, value, file_dialog = file_dialog)
           ini.endGroup ()
   """

   """
   name = Edit
   icon = edit-undo

   [page-edit/tab-first]
   name = First

   [page-edit/tab-first/abc]
   value = abc.txt
   file_dialog = 1

   [page-edit/tab-first/something]
   value = 1
   check_box = true

   [page-edit/tab-second]
   name = Second

   [page-other]
   name = Other
   icon = folder-new
   """

   """
   OptionPage
   {
      name = Edit
      icon = edit-undo

      OptionTab
      {
         name = First

         OptionEdit
         {
            name = abc
            value = abc.txt
            file_dialog = 1
         }

         OptionCheckBox
         {
            name = something
            value = 1
         }
      }

      OptionTab
      {
         name = Second
      }
   }

   OptionPage
   {
      name = Other
      icon = folder-new
   }
   """

# --------------------------------------------------------------------------

class Thread (QThread) :
   def __init__(self, page) :
       super (Thread, self).__init__()
       self.page = page
       self.moveToThread (QCoreApplication.instance().thread())
       self.finished.connect (self.onfinish)
       self.start ()

   def run (self) :
       self.page.collectAllIcons ()

   def onfinish (self) :
       self.page.displayAllIcons ()
       self.page.ready = True

class IconList (QDialog) :
   def __init__ (self, win) :
       super (IconList, self).__init__ (win)

       self.setWindowTitle ("Icons")
       self.ready = False

       self.orig_theme = QIcon.themeName ()
       if not use_qt4 :
          self.orig_fallback = QIcon.fallbackThemeName ()

       themes = ["<current theme>", "<no theme>"]

       path_list = QIcon.themeSearchPaths ()
       if not use_qt4 :
          path_list = path_list + QIcon.fallbackSearchPaths ()

       for path_dir in path_list :
           path = str (path_dir) # PyQt4
           if os.path.isdir (path) :
              for name in os.listdir (path) :
                  if name not in themes :
                     fileName = os.path.join (path, name)
                     if os.path.isdir (fileName) :
                           themes.append (name)

       self.lineEdit = QLineEdit ()
       self.lineEdit.setToolTip ("Name filter")
       self.lineEdit.textChanged.connect (self.onTextChanged)
       self.filter = self.lineEdit.text ()

       self.themeBox = QComboBox ()
       self.lineEdit.setToolTip ("Icon theme")
       self.themeBox.addItems (themes)
       self.themeBox.currentIndexChanged.connect (self.onThemeChanged)

       self.comboBox = QComboBox ()
       self.comboBox.addItems (["16", "22", "24", "32", "36", "48"])
       self.comboBox.setCurrentIndex (2) # 24, setCurrentText exists only in Qt5
       self.comboBox.currentIndexChanged.connect (self.onComboBoxChanged)

       self.listView = QListWidget ()
       self.listView.setIconSize (QSize (24, 24))
       self.listView.setFlow (QListView.LeftToRight)
       self.listView.setResizeMode (QListView.Adjust)
       self.listView.setWrapping (True)

       self.listView.setContextMenuPolicy (Qt.CustomContextMenu)
       self.listView.customContextMenuRequested.connect (self.onContextMenu)

       self.buttonBox = QDialogButtonBox (QDialogButtonBox.Cancel)
       self.buttonBox.rejected.connect (self.reject)

       self.vlayout = QVBoxLayout ()
       self.vlayout.addWidget (self.lineEdit)
       self.vlayout.addWidget (self.themeBox)
       self.vlayout.addWidget (self.comboBox)
       self.vlayout.addWidget (self.listView)
       self.vlayout.addWidget (self.buttonBox)
       self.setLayout (self.vlayout)

       self.rejected.connect (self.onClose)

       self.show ()

       self.icon_dict = { }
       if use_qt4 :
          self.collectAllIcons ()
          self.displayAllIcons ()
          self.ready = True
       else :
          self.my_thread = Thread (self) # keep thread object

   def onClose (self) :
       QIcon.setThemeName (self.orig_theme )
       if not use_qt4 :
          QIcon.setFallbackThemeName (self.orig_fallback)

   def onTextChanged (self) :
       self.filter = self.lineEdit.text ()
       if self.ready :
          self.listView.clear ()
          self.displayAllIcons ()

   def onThemeChanged (self) :
       if self.ready :
          name = self.themeBox.currentText ()
          self.listView.clear ()
          if name == "<current theme>" :
             QIcon.setThemeName (self.orig_theme)
             if not use_qt4 :
                QIcon.setFallbackThemeName (self.orig_fallback)
          elif name == "<no theme>" :
             QIcon.setThemeName ("")
             if not use_qt4 :
                QIcon.setFallbackThemeName ("")
          else :
             QIcon.setThemeName (name)
             if not use_qt4 :
                QIcon.setFallbackThemeName ("")
          self.collectAllIcons ()
          self.displayAllIcons ()

   def onComboBoxChanged (self) :
       size = int (self.comboBox.currentText())
       self.listView.setIconSize (QSize (size, size))
       self.listView.scrollToItem (self.listView.currentItem ())

   def onContextMenu (self, pos) :
       item = self.listView.itemAt (pos)
       menu = QMenu (self.listView)

       name = item.toolTip ()
       act = menu.addAction ("copy " + name + " name")
       act.triggered.connect (lambda param, self=self, name=name: self.copyName (name))

       menu_exec (menu, self.mapToGlobal (QPoint (pos)))

   def copyName (self, name) :
       cb = QApplication.clipboard ()
       # mode = cb.Clipboard
       mode = cb.Selection
       cb.clear (mode)
       cb.setText (name, mode)

   def collectAllIcons (self) :
       # theme = str (QIcon.themeName ())
       for path in QIcon.themeSearchPaths() :
           self.collectFromDir (path)

   def collectFromDir (self, path) :
       qdir = QDir (path)

       for entry in qdir.entryInfoList (QDir.Dirs | QDir.NoDotAndDotDot) :
           self.collectFromDir (entry.absoluteFilePath ())

       for entry in qdir.entryInfoList (QDir.Files) :
           name = entry.baseName ()
           if name not in self.icon_dict :
              icon = QIcon.fromTheme (name)
              self.icon_dict [name] = icon

   def displayAllIcons (self) :
       name_list = self.icon_dict.keys ()
       for name in sorted (name_list) :
           # if self.filter == "" or self.filter in name :
           if self.filter in name :
              icon = self.icon_dict [name]
              if not icon.isNull () :
                 node = QListWidgetItem (self.listView)
                 # node.setText (name)
                 node.setToolTip (name)
                 node.setIcon (icon)

# --------------------------------------------------------------------------

class ActionList :
   def __init__ (self, win) :
       self.win = win
       self.action_dict = { }

       for item in self.win.menuBar().actions() :
           prefix = "Menu " + item.text () + " / "
           if not use_pyside6 and not use_pyqt6 : # !?
              for action in item.menu().actions() :
                 self.addAction (action, prefix)

       for action in self.win.actions() :
           self.addAction (action)

       for desc in self.win.conf.shortcut_list :
           self.getAction (desc)

       # print (self.action_dict)
       # print (sorted (self.action_dict.keys()))

       """
       ini = self.win.commands
       for group in ini.groupsWithPrefix ("toolbar") :
           action = self.getAction (group)
           if action != None :
              # print ("TOOLBAR ", action.text ())
              self.win.toolbar.addAction (action)


       for group in ini.groupsWithPrefix ("shortcut") :
           self.getAction (group)
       """

   def simplifyName (self, name) :
       name = qstring_to_str (name)
       # print ("simplifyName", name)
       name = name.lower ()
       name = name.replace ("&", "")
       name = name.replace ("/", "")
       name = " ".join (name.split())
       # print ("result simplifyName", name, "...", type (name))
       return name

   def addAction (self, action, prefix = "") :
       # name = qstring_to_str (action.text ())
       name = qstring_to_str (action.text ())
       if prefix != "" :
          self.addSimpleAction (action, prefix + name)
       self.addSimpleAction (action, name)

   def addSimpleAction (self, action, name) :
       name = self.simplifyName (name)
       if name != None and name != "" :
          # print ("ADD ", name, "...", type (name))
          self.action_dict [name] = action

   def getAction (self, desc) :

       name = self.simplifyName (desc.name)
       action = self.action_dict.get (name, None)
       if action == None :
          print ("Unknown action:", name)
       else :
          if desc.icon != "" :
             icon_obj = findIcon (desc.icon)
             if icon_obj != None :
                action.setIcon (icon_obj)
          if desc.shortcut != "" :
             shortcuts = action.shortcuts ()
             shortcuts.append (desc.shortcut)
             action.setShortcuts (shortcus)

          desc._action_ = action # store action

       return action

       """
       ini = self.win.commands
       ini.beginGroup (group)
       name = ini.string ("name")
       icon = ini.string ("icon")
       shortcut = ini.string_shortcut ("shortcut")
       ini.endGroup ()

       name = self.simplifyName (name)
       action = self.action_dict.get (name, None)
       if action == None :
          print ("Unknown action:", name,  "in", group)
       else :
          # print ("ACTION ", name) #  + " in " + group)
          if icon != "" :
             icon_obj = findIcon (icon)
             if icon_obj != None :
                action.setIcon (icon_obj)
          if shortcut != "" :
             shortcut_list = action.shortcuts ()
             shortcut_list.append (shortcut)
             action.setShortcuts (shortcut_list)

       return action
       """

# --------------------------------------------------------------------------

class Notes (OptionDialog) :
   def __init__ (self, win) :
       super (Notes, self).__init__ (win)

       self.setWindowTitle ("Notes")
       self.buttonBox.setStandardButtons (QDialogButtonBox.Cancel)

       self.shortcutPage ()
       self.colorCachePage ()
       self.colorNamesPage ()
       self.iconCachePage ()
       # self.iconPathPage ()
       self.pathPage ()
       self.modulePage ()
       self.globalVariablesPage ()
       self.winVariablePage ()
       self.configPage ()
       # self.toolIniPage ()
       self.iniPage ("options.ini", "clementine", self.win.settings)
       self.iniPage ("edit.ini", "text-editor", self.win.session)

   # -----------------------------------------------------------------------

   def winVariablePage (self) :
       listView = ObjectBrowser (self, self.win)
       self.addPage ("win object", "code-class", listView)

   # -----------------------------------------------------------------------

   def globalVariablesPage (self) :
       listView = QListWidget ()
       self.addPage ("global variables", "variable", listView)
       for item in globals () :
          listView.addItem (item)
       listView.itemDoubleClicked.connect (self.showVariable)

   def showVariable (self, item) :
       name = qstring_to_str (item.text ())
       ObjectBrowser (self.win, globals () [name])

   # -----------------------------------------------------------------------

   def pathPage (self) :
       listView = QListWidget ()
       self.addPage ("sys path", "rosegarden", listView)
       for item in sys.path :
          listView.addItem (item)

   def modulePage (self) :
       listView = QListWidget ()
       self.addPage ("modules", "kdf", listView)
       # for item in self.win.loaded_modules :
       #    listView.addItem (item)
       for item in sorted (sys.modules) :
          listView.addItem (item)
       listView.itemDoubleClicked.connect (self.showModule)
       listView.itemClicked.connect (self.alternativeShowModule)

   def showModule (self, item) :
       name = qstring_to_str (item.text ())
       ObjectBrowser (self.win, sys.modules [name])

   def alternativeShowModule (self, item) :
       mask = Qt.ShiftModifier | Qt.ControlModifier | Qt.AltModifier | Qt.MetaModifier
       mod = QApplication.keyboardModifiers () & mask
       if mod == Qt.ControlModifier :
          # print ("CTRL CLICK")
          self.showModule (item)

   # -----------------------------------------------------------------------

   def resizeFirstColumn (self, treeView) :
       setResizeMode (treeView.header(), 0, QHeaderView.ResizeToContents)

   def resizeSecondColumn (self, treeView) :
       setResizeMode (treeView.header(), 1, QHeaderView.ResizeToContents)

   def colorCachePage (self) :
       treeView = QTreeWidget (self)
       treeView.setHeaderLabels (["Name", "Value"])
       self.resizeFirstColumn (treeView)
       self.addPage ("color cache",  "color-management", treeView)

       for name in util.color_cache :
          self.displayColor (treeView, name, util.color_cache [name])

       treeView.sortByColumn (0, Qt.AscendingOrder)

   def colorNamesPage (self) :
       treeView = QTreeWidget (self)
       treeView.setHeaderLabels (["Name", "Value"])
       self.resizeFirstColumn (treeView)
       self.addPage ("color names", "gimp", treeView)

       for name in util.color_map :
          self.displayColor (treeView, name, util.color_map [name])

       treeView.sortByColumn (0, Qt.AscendingOrder)

   def displayColor (self, branch, name, color) :
       node = QTreeWidgetItem (branch)
       node.setText (0, name)
       node.setToolTip (0, name)
       if color.lightness () < 220 and color.alpha () != 0 :
          node.setForeground (0, color)
       node.setData (0, Qt.DecorationRole, color)
       node.setText (1, str (color.red ()) + ", " + str (color.green ())+ ", " + str (color.blue ()))

   # -----------------------------------------------------------------------

   def iconCachePage (self) :
       treeView = QTreeWidget (self)
       self.addPage ("icon cache", "choqok", treeView)

       for name in util.icon_cache :
           icon = util.icon_cache [name]
           self.displayIcon (treeView, name, icon)

       treeView.sortByColumn (0, Qt.AscendingOrder)

   def displayIcon (self, branch, name, icon) :
       node = QTreeWidgetItem (branch)
       node.setText (0, name)
       node.setToolTip (0, name)
       node.setIcon (0, icon)

   # -----------------------------------------------------------------------

   def iconPathPage (self) :
       listView = QListWidget ()
       self.addPage ("icon path", "rosegarden", listView)
       for item in util.icon_path :
          listView.addItem (item)

   # -----------------------------------------------------------------------

   def iniPage (self, name, icon, ini) :
       treeView = QTreeWidget (self)
       treeView.setHeaderLabels (["Name", "Value"])
       self.resizeFirstColumn (treeView)
       self.addPage (name, icon, treeView)
       for group in ini.childGroups () :
           branch = QTreeWidgetItem (treeView)
           branch.setText (0, "[" + group + "]")
           branch.setIcon (0, findIcon ("folder"))
           ini.beginGroup (group)
           for key in ini.childKeys () :
               value = ini.value (key)
               item = QTreeWidgetItem (branch)
               item.setText (0, key)
               item.setText (1, variant_to_str (value))
           ini.endGroup ()

   # -----------------------------------------------------------------------

   def configPage (self) :
       treeView = QTreeWidget (self)
       treeView.setHeaderLabels (["Name", "Value"])
       self.resizeFirstColumn (treeView)
       self.addPage ("tools.cfg", "tools-wizard", treeView)
       for member in inspect.getmembers (self.win.conf) :
           name, value = member
           if name.endswith ("_list") :
              branch = QTreeWidgetItem (treeView)
              branch.setText (0, name)
              branch.setIcon (0, findIcon ("folder"))
              self.showConfigList (branch, value)

   def showConfigList (self, branch, data) :
       for value in data :
           self.showConfigItem (branch, "", value)

   def showConfigDict (self, branch, data) :
       for name in data :
           value = data [name]
           self.showConfigItem (branch, name, value)

   def showConfigItem (self, branch, name, value) :
       if isinstance (value, bool) or isinstance (value, int) or isinstance (value, float) or isinstance (value, str) :
          if name == "" :
             name = "(item)"
             self.showConfigValue (branch, name, value)
       else :
          self.showConfigValue (branch, name, value)

   def showConfigValue (self, branch, name, value) :
       if isinstance (value, bool) or isinstance (value, int) or isinstance (value, float) or isinstance (value, str) :
          item = QTreeWidgetItem (branch)
          item.setText (0, name)
          item.setText (1, str (value))
       elif isinstance (value, SettingsData) :
          self.showConfigData (branch, name, value)

   def showConfigData (self, branch, title, data) :
       item = QTreeWidgetItem (branch)
       item.setText (0, data.__class__.__name__)
       item.setIcon (0, findIcon ("folder"))

       if title == "" :
          if hasattr (data, "_name_field_") :
             title = getattr (data, data._name_field_, "")
       if title == "" :
          if hasattr (data, "name") :
             title = str (data.name)
       item.setText (1, title)

       for member in inspect.getmembers (data) :
           name, value = member
           if not inspect.iscode (value) and not name.startswith ("__"):
              if name == "items" :
                 if isinstance (value, list) :
                    self.showConfigList (item, value)
                 elif isinstance (value, dict) :
                    self.showConfigDict (item, value)
              else :
                 self.showConfigValue (item, name, value)

   # -----------------------------------------------------------------------

   """
   def toolIniPage (self) :
       treeView = QTreeWidget (self)
       treeView.setHeaderLabels (["Name", "Value"])
       self.resizeFirstColumn (treeView)
       self.addPage ("tools.ini", "tools-wizard", treeView)
       ini = self.win.commands
       section = None
       start = ""
       for group in ini.childGroups () :
           m = re.match ("(\w\w*)-.*", group)
           if m :
              new_start = m.group (1)
              if new_start != start :
                 start = new_start
                 section = QTreeWidgetItem (treeView)
                 section.setText (0, start);
                 section.setIcon (0, findIcon ("folder-yellow"))
              branch = QTreeWidgetItem (section)
           else :
              start = ""
              section = None
              branch = QTreeWidgetItem (treeView)

           branch.setText (0, "[" + group + "]")
           branch.setIcon (0, findIcon ("folder"))
           ini.beginGroup (group)
           for key in ini.childKeys () :
               value = ini.value (key)
               item = QTreeWidgetItem (branch)
               item.setText (0, key)
               item.setText (1, variant_to_str (value))
           ini.endGroup ()
   """

# --------------------------------------------------------------------------

   def shortcutPage (self) :

       treeView = QTreeWidget ()
       treeView.setHeaderLabels (["Name", "Shortcut", "Invisible"])
       self.name_column = 0
       self.key_column = 1
       self.invisible_column = 2
       self.resizeFirstColumn (treeView)
       self.resizeSecondColumn (treeView)
       header = treeView.header ()
       header.hideSection (self.invisible_column)
       # name_column = 0
       # header.setSectionResizeMode (name_column, QHeaderView.ResizeToContents)
       self.addPage ("shortcuts", "key-enter", treeView)

       for item in self.win.menuBar().actions() :
           self.displayMainMenuAction (treeView, item)
           prefix = "Menu " + item.text () + " / "
           for action in item.menu().actions() :
               self.displayAction (treeView, action, prefix)

       for action in self.win.actions() :
           self.showItem (branch, action)
       for item in self.win.children() :
           if isinstance (item, QShortcut) :
              self.displayShortcut (treeView, item)

       for inx in [1, 2, 3, 4, 5] :
           self.formalShortcut (treeView, "Go to bookmark " + str (inx), "Ctrl+" + str (inx))
           self.formalShortcut (treeView, "Set bookmark " + str (inx), "Ctrl+Shift+" + str (inx))
           self.formalShortcut (treeView, "Set bookmark " + str (inx), "Win+" + str (inx))

       if 0 :
          # remove items without shortcuts
          inx = treeView.topLevelItemCount () - 1
          while inx > 0 :
             item = treeView.topLevelItem (inx)
             if item.text (self.key_column) == "" : # no shortcut
                treeView.takeTopLevelItem (inx)
             inx = inx - 1

       self.setKeys (treeView)
       treeView.sortByColumn (self.invisible_column, Qt.AscendingOrder)

   def displayMainMenuAction (self, treeView, item) :
       node = QTreeWidgetItem (treeView)
       text = str (item.text ())
       node.setText (self.name_column, "Menu " +text)
       inx = text.find ('&')
       if inx >= 0 and inx+1 < len (text) :
          c = text [inx+1]
          node.setText (1, "Alt+" + c.upper ())
       icon  = item.icon ()
       node.setIcon (self.name_column, icon)

   def displayAction (self, treeView, action, prefix = "") :
       for shortcut in action.shortcuts () :
           node = QTreeWidgetItem (treeView)
           node.setText (self.name_column, prefix + action.text())
           node.setIcon (self.name_column, action.icon ())
           node.setText (self.key_column,  self.keySequenceToString (shortcut))

   def displayShortcut (self, treeView, shortcut) :
       node = QTreeWidgetItem (treeView)
       node.setText (self.name_column, shortcut.objectName ())
       node.setText (self.key_column,  self.keySequenceToString (shortcut.key()))

   def formalShortcut (self, treeView, name, shortcut) :
       node = QTreeWidgetItem (treeView)
       node.setText (self.name_column, name)
       node.setText (self.key_column,  shortcut)

   def keySequenceToString (self, shortcut) :
       text = shortcut.toString ()
       text = qstring_to_str (text)
       text = text.replace ("Meta+", "Win+")
       return text

   def setKeys (self, treeView) :
       cnt = treeView.topLevelItemCount ()
       for inx in range (cnt) :
          item = treeView.topLevelItem (inx)
          text = qstring_to_str (item.text (self.key_column))

          key = "T"
          if text == "" :
             key = "Z"

          if text.startswith ("Win+") :
             key = key + "W"
             text = text [4:]
          else :
             key = key + "Z"

          if text.startswith ("Alt+") :
             key = key + "A"
             text = text [4:]
          else :
             key = key + "Z"

          if text.startswith ("Ctrl+") :
             key = key + "C"
             text = text [5:]
          else :
             key = key + "Z"

          if text.startswith ("Shift+") :
             key = key + "S"
             text = text [6:]
          else :
             key = key + "Z"

          if text == "Left" :
             key = key + "a"
          elif text == "Right" :
             key = key + "b"
          elif text == "Up" :
             key = key + "c"
          elif text == "Down" :
             key = key + "d"
          elif text == "Ins" :
             key = key + "e"
          elif text == "Del" :
             key = key + "f"
          elif text == "Home" :
             key = key + "g"
          elif text == "End" :
             key = key + "h"
          elif text == "PgUp" :
             key = key + "i"
          elif text == "PgDown" :
             key = key + "j"
          elif text.startswith ("F") and len (text) > 1 :
             if len (text) == 2 : # F1 .. F9
                key = key + "l"
             else :
                key = key + "m" # F10 .. F12
          elif len (text) > 1 :
             key = key + "k" # before F1 and F12
          elif text >= "0" and text <= "9":
             key = key + "o"
          elif text >= "A" and text <= "Z":
             key = key + "p" # before digits
          elif text == "-" :
             key = key + "na"
          elif text == "=" :
             key = key + "nb"
          elif text == "\\" :
             key = key + "nc"
          elif text == "[" :
             key = key + "nd"
          elif text == "]" :
             key = key + "ne"
          elif text == ";" :
             key = key + "nf"
          elif text == "'" :
             key = key + "ng"
          elif text == "<" :
             key = key + "nh"
          elif text == ">" :
             key = key + "ni"
          elif text == "/" :
             key = key + "nj"
          else :
             key = key + "nk" # before digits

          key = key + text
          item.setText (self.invisible_column, key)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
