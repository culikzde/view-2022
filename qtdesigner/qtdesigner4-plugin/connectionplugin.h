
#ifndef CONNECTIONPLUGIN_H
#define CONNECTIONPLUGIN_H


#include <QDesignerCustomWidgetInterface>

#include <QDesignerFormEditorInterface>
// #include <abstractintegration.h>
#include "internals/qdesigner_integration_p.h"

class ConnectionPlugin : public QObject, public QDesignerCustomWidgetInterface
{
    Q_OBJECT
    // Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QDesignerCustomWidgetInterface")
    Q_INTERFACES(QDesignerCustomWidgetInterface)
public:
    explicit ConnectionPlugin(QObject * parent = 0);

    bool isContainer() const;
    bool isInitialized() const;
    QIcon icon() const;
    QString domXml() const;
    QString group() const;
    QString includeFile() const;
    QString name() const;
    QString toolTip() const;
    QString whatsThis() const;
    QWidget *createWidget(QWidget *parent);
    void initialize(QDesignerFormEditorInterface *p_core);

private:
    QDesignerFormEditorInterface * core;
    // QDesignerIntegrationInterface * integration;
    qdesigner_internal::QDesignerIntegration * integration;
    bool initialized;
    QString message;

private slots:
    void setup ();
    void slotNavigateToSlot(const QString &objectName, const QString &signalSignature, const QStringList &parameterNames);
};

#endif
