#!/bin/sh

test -f Makefile && make clean
python2 configure.py || exit 1
make || exit 1
python2 run.py

# Fedora 30 : O.K.
# requires QtCore.PYQT_CONFIGURATION["sip_flags"]
# -n PyQt4.sip -x VendorID -t WS_X11 -x PyQt_NoPrintRangeBug -t Qt_4_8_6 -x Py_v3

# Fedora 31 : replace python with python2

# gdb -ex r --args python run.py

# dnf install sip-devel
# dnf install PyQt4-devel

# apt-get install pkg-config qt4-dev
# apt-get install python-qt4-dev
