#!/usr/bin/env python

from __future__ import print_function

from PyQt4.QtCore import *
from PyQt4.QtGui import *

# from PyQt4.QtDesigner import QPyDesignerCustomWidgetPlugin
from PyQt4.QtDesigner import *
from PyQt4 import QtCore, QtGui, QtDesigner

import dbus

from connectionwidget import ConnectionWidgetPyQt

class ConnectionPlugin (QPyDesignerCustomWidgetPlugin) :

    def __init__(self, parent=None):
        super (ConnectionPlugin, self).__init__(parent)
        self.initialized = False
        self.core = None
        self.message = ""

    def initialize (self, core):
        self.core = core
        if not self.initialized:
            manager = core.extensionManager()
            if manager:
                self.factory = ConnectionTaskMenuFactory (manager, self.core)
                manager.registerExtensions(self.factory, "com.trolltech.Qt.Designer.TaskMenu")
            self.initialized = True
            self.message = "initialized"
            self.core.parent().initialized.connect (self.setup)

    def setup (self) :
      self.message = "setup"
      try :
        obj = self.core.parent ()
        for item in obj.findChildren (QObject) :
           name = item.metaObject().className ()
           if name.find ("QDesignerIntegration") >= 0 :
              self.message = "found object"
              integration = item
              self.integration = integration
              # integration.navigateToSlot.connect (self.navigateToSlot);
              integration.setObjectName ("integration")
              integration.setParent (self)
              QMetaObject.connectSlotsByName (self)
              self.message = "ready"
      except Exception, e :
        self.message = str (e)

    @pyqtSlot ("QString", "QString", "QStringList")
    def on_integration_navigateToSlot (self, objectName, signalSignature, parameterNames) :
        self.message = "navigate"
        try :
           bus = dbus.SessionBus ()
           remote_object = bus.get_object ("org.example.receiver", "/org/example/ReceiverObject")
           ifc = dbus.Interface (remote_object, "org.example.ReceiverInterface")
           names = [ str (item) for item in parameterNames ]
           if len (names) == 0 :
              names = [ "" ]
           ifc.navigateToSlot (str (objectName), str (signalSignature), names)
           # ifc.hello (signalSignature)
           self.message = "sent"
        except Exception, e :
           self.message = str (e)

    def isInitialized (self) :
        return self.initialized

    def createWidget (self, parent):
        """
        self.message = "" # Invoke some context Menu and create new connection widget
        try :
          obj = self.core.parent ()
          for item in obj.findChildren (QObject) :
             name = item.metaObject().className ()
             if hasattr (item, "text") :
                txt = item.text ()
                if txt.startsWith ("Go") :
                   self.message += txt + "\n"
        except Exception, e :
          self.message = str (e)
        """
        widget = ConnectionWidgetPyQt (parent)
        widget.setText (self.message)
        return widget

    def name(self) :
        return "ConnectionWidgetPyQt"

    def group (self) :
        return "Designer Connection"

    def icon (self) :
        return QIcon ()

    def toolTip (self) :
        return "Qt Designer plugin with DBus connection (PyQt4)"

    def whatsThis (self) :
        return ""

    def isContainer (self) :
        return False

    def domXml (self) :
        return '<widget class="ConnectionWidgetPyQt" name="connectionWidget">\n' \
               '</widget>\n'

    def includeFile (self) :
        return "designerconnection"

# PyQt4-4.12.3/examples/designer/plugins/widgets/highlightedtextedit.py

class ConnectionTaskMenuFactory(QExtensionFactory):

    def __init__(self, parent, core):
        QExtensionFactory.__init__(self, parent)
        self.core = core

    def createExtension(self, obj, iid, parent):
        return ConnectionTaskMenu (parent, self.core)

class ConnectionTaskMenu(QPyDesignerTaskMenuExtension):

    def __init__(self, parent, core):
        super (ConnectionTaskMenu, self).__init__(parent)
        self.core = core
        self.editStateAction = QAction ("Edit Text...", self, triggered=self.editText)

    def preferredEditAction(self):
        return self.editStateAction

    def taskActions(self):
        self.action = None
        try :
          obj = self.core.parent ()
          for item in obj.findChildren (QObject) :
             name = item.metaObject().className ()
             if hasattr (item, "text") :
                txt = item.text ()
                if txt.startsWith ("Go") :
                   self.action = item
        except :
           pass

        if self.action != None :
           return [self.editStateAction, self.action]
        else :
           return [self.editStateAction]

    def editText(self):
        pass

