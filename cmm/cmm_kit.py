
# cmm_kit.py

from __future__ import print_function

import sys
# import util
# import cmm_plugin

# --------------------------------------------------------------------------

def compileColorButton () :
    print ("COLOR BUTTON")

    util = sys.modules ["util"]
    win = util.get_win ()

    # win.info.disableCleaning ()
    win.initProject ("examples/win.t")

    cmm_plugin = win.loadModule ("cmm_plugin")
    plugin = cmm_plugin.Plugin (win)
    # plugin.reuse_compiler = True
    # plugin.ignore_all_includes = True

    # plugin.comp ("../kit/qview/colorbutton.h")
    # plugin.comp ("../kit/qview/colorbutton.cc")
    # plugin.comp ("examples/win.t")

    # plugin.try_to_cpp ()
    # plugin.try_to_python ()

    settings = sys.modules ["settings"]
    desc = settings.CommandData ()
    desc.sourceFileNames = [
       "../kit/qview/colorbutton.h",
       "../kit/qview/colorbutton.cc",
       "examples/win.t",
    ]
    desc.ignore_all_includes = True
    plugin.runPluginCommand (desc)

    # win.info.enableCleaning ()

# --------------------------------------------------------------------------

class StructModule (object) :
   def __init__ (self, compiler) :
       super (StructModule, self).__init__ ()
       self.compiler = compiler
       self.name = "structure"

   def func (self, txt) :
       print ("STRUCT Model FUNC", txt)

   def compile (self) :
       print ("STRUCT MODEL", self.name)
       compiler = self.compiler

       compiler.info ("BEFORE OPEN, token = " + compiler.tokenToText () + " ... " + str (compiler.token))
       compiler.openNestedFile ("orig/orig-examples/entry-simple.t")
       compiler.info ("AFTER OPEN, token = " + compiler.tokenToText () + " ... " + str (compiler.token))

       while not compiler.isEndOfSource () :
          # compiler.info ("token = " + compiler.tokenToText ())
          compiler.nextToken ()

       compiler.info ("BEFORE CLOSE, token = " + compiler.tokenToText () + " ... " + str (compiler.token))
       compiler.closeNested ()
       compiler.info ("AFTER CLOSE, token = " + compiler.tokenToText () + " ... " + str (compiler.token))

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
