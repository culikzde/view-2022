#!/usr/bin/env python

from __future__ import print_function

import gcc
# import gccutils

import os, sys

if __name__ == '__main__' :
   work_dir = os.getcwd ()
   sys.path.insert (1, os.path.join (work_dir, "code"))

   from code import *
   from code_print import print_declaration

# --------------------------------------------------------------------------

class Declarations :

   def __init__ (self) :
       self.types = { }

   # -----------------------------------------------------------------------

   def showType (self, node) :
       result = NamedType () # !?
       result.type_label = str (node)
       return result

   def showDecl (self, target, node, level) :

      if level > 10 :
         return

      if getattr (node, 'is_builtin', False) :
         if level != 1 : # enable global namespace
            return

      if hasattr (node, 'is_artificial') :
         if not isinstance (node, gcc.TypeDecl) :
            if node.is_artificial :
               return

      if isinstance (node, gcc.NamespaceDecl) :
         module = Namespace ()
         module.item_name = node.name
         target.add (module)
         for t in node.namespaces :
             self.showDecl (module, t, level+1)
         for t in node.declarations :
             self.showDecl (module, t, level+1)
      elif isinstance (node, gcc.TypeDecl) :
         type = node.type
         if isinstance (type, gcc.RecordType) :
            name = str (type) # !? namespace
            if name not in self.types :
               self.types [name] = True
               cls =  Class ()
               cls.item_name = node.name
               target.add (cls)
               for t in type.fields :
                   self.showDecl (cls, t, level+1)
               for t in type.methods :
                   self.showDecl (cls, t, level+1)
         elif isinstance (type, gcc.EnumeralType) :
            enum = Enum ()
            enum.item_name = node.name
            target.add (enum)
            # for t in type.values :
            #    self.showDecl (enum, t, level+1)
      elif isinstance (node, gcc.FunctionDecl) :
         func = Function ()
         func.item_name = node.name
         # func.comment = node.fullname
         target.add (func)
         # if node.type != None :
         func.result_type = self.showType (node.type.type)
         for t in node.arguments :
             var = Variable ()
             if t.name != None :
                var.item_name = t.name
             var.item_type = self.showType (t.type)
             func.parameters.add (var)
      elif isinstance (node, gcc.VarDecl) or isinstance (node, gcc.ConstDecl) or isinstance (node, gcc.FieldDecl) :
         # if isinstance (node, gcc.ConstDecl) :
         #    self.put ("const ")
         if node.name != None :
            var = Variable ()
            var.item_name = node.name
            var.item_type = self.showType (node.type)
            target.add (var)

   # -----------------------------------------------------------------------

   def showDeclarations (self, node) :
       module = Namespace ()
       self.showDecl (module, node, 1)
       # gccutils.pprint (node)
       print_declaration (module)

# --------------------------------------------------------------------------

def on_pass_execution (p, fn) :
    # if p.name == '*warn_function_return':
        # print('fn: %r' % fn)
        # print('fn.decl.name: %r' % fn.decl.name)
    if p.name == "*free_lang_data" :
       d = Declarations ()
       d.showDeclarations (gcc.get_global_namespace ())

# --------------------------------------------------------------------------

gcc.register_callback (gcc.PLUGIN_PASS_EXECUTION, on_pass_execution)

# see generate-tree-c.py from gcc-python-plugin source

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
