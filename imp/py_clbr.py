#!/usr/bin/env python

from __future__ import print_function

import os

import pyclbr

# --------------------------------------------------------------------------

def py_clbr (win, fileName) :

    subdir = os.path.dirname (fileName)
    name, ext = os.path.splitext (os.path.basename (fileName))
    data = pyclbr.readmodule_ex (name, path = [ subdir ])

    result = win.treeBranch ("pyclbr " + name)
    result.src_file = win.fileNameToIndex (fileName)
    result.addIcon ("file")

    for object_name, object_item in data.items () :
        branch = win.treeItem (result, object_name)
        branch.addIcon ("class")
        if hasattr (object_item, "file") :
           branch.src_file = win.fileNameToIndex (object_item.file)
        if hasattr (object_item, "lineno") :
           branch.src_line = object_item.lineno

        if hasattr (object_item, "methods") :
           for method_name, method_line in object_item.methods.items ():
               node = win.treeItem (branch, method_name)
               node.src_line = method_line
               node.addIcon ("function")

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
