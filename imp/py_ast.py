#!/usr/bin/env python

from __future__ import print_function

import os, sys

import ast

# --------------------------------------------------------------------------

def showTree (win, target, t) :
    name = getattr (t, "name", "")
    icon = ""

    if isinstance (t, ast.FunctionDef) :
       name = "function " + name
       icon = "fuction"

    if isinstance (t, ast.ClassDef) :
       name = "class " + name
       icon = "class"

    if name == "" :
       name = str (t)

    branch = win.treeItem (target, name)
    branch.obj = t
    if icon != "" :
       branch.addIcon (icon)

    if hasattr (t, "lineno") :
       branch.src_line = t.lineno

    if hasattr (t, "body") :
       for e in t.body :
          showTree (win, branch, e)

def showAstTree (win, fileName) :
    source = win.readFile (fileName)
    # print (source)
    t = ast.parse (source)

    name = os.path.basename (fileName)
    branch = win.treeBranch ("AST " + name)
    branch.src_file = win.fileNameToIndex (fileName)
    showTree (win, branch, t)

# --------------------------------------------------------------------------

# http://docs.python.org/2/library/ast.html
# http://greentreesnakes.readthedocs.io

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
