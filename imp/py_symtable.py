#!/usr/bin/env python

from __future__ import print_function

import os

import symtable

# --------------------------------------------------------------------------

def displaySymtable (win, branch, item) :
    text = item.get_name ()
    if hasattr (item, "get_type") :
       text = item.get_type () + " " + text
    node = win.treeItem (branch, text)
    node.obj = item

    if isinstance (item, symtable.Function) :
       displayAttributes (win, node, "parameters", item.get_parameters ())
       displayAttributes (win, node, "locals", item.get_locals ())
       displayAttributes (win, node, "globals", item.get_globals ())
       displayAttributes (win, node, "frees", item.get_frees ())
    elif isinstance (item, symtable.Class) :
       if hasattr (item, "get_identifiers") :
          for t in item.get_identifiers () :
              displaySymtable (win, node, item.lookup (t))
    else :
       if hasattr (item, "get_children") :
          for t in item.get_children () :
              displaySymtable (win, node, t)

       if hasattr (item, "get_symbols") :
          for t in item.get_symbols () :
              displaySymtable (win, node, t)

       if hasattr (item, "get_namespaces") :
          for t in item.get_namespaces () :
              displaySymtable (win, node, t)


def displayAttributes (win, target, name, values) :
    branch = win.treeItem (target, name)
    for t in values :
        node = win.treeItem (branch, t)
        node.obj = t

def py_symtable (win, fileName) :
    source = win.readFile (fileName)
    table = symtable.symtable (source, fileName, "exec")

    name, ext = os.path.splitext (os.path.basename (fileName))
    result = win.treeBranch ("symtable " + name)
    result.src_file = win.fileNameToIndex (fileName)

    displaySymtable (win, result, table)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
