
#pragma gcc_options
#pragma pkg "Qt5Widgets"

// #include <QtCore>
// #include <QtGui>
// #include <QtWidgets>

#include <QApplication>
#include <QMainWindow>
#include <QTreeWidget>

int main (int argc, char * * argv)
{
     QApplication appl (argc, argv);

     QMainWindow * window = new QMainWindow;

     QTreeWidget * tree = new QTreeWidget;

     QTreeWidgetItem * root = new QTreeWidgetItem;
     root->setText (0, "root");
     root->setForeground (0, QColor ("brown"));
     tree->addTopLevelItem (root);

     QTreeWidgetItem * branch = new QTreeWidgetItem;
     branch->setText (0, "branch");
     branch->setForeground (0, QColor ("lime"));
     root->addChild (branch);

     for (int i = 1; i <= 3; i += 1)
     {
        QTreeWidgetItem * item = new QTreeWidgetItem;
        item->setText (0, "item " + QString::number (i));
        QString color_name = "";
        if (i == 1)
           color_name = "red";
        else if (i == 2)
           color_name = "blue";
        else if (i == 3)
           color_name = "orange";
        QColor color = QColor (color_name);
        item->setForeground (0, color);
        branch->addChild (item);
     }

     tree->expandAll ();
     window->setCentralWidget (tree);
     window->show ();

     appl.exec ();
}

// kate: indent-width 1; show-tabs true; replace-tabs true;
