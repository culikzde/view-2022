## Zobrazení pomocí HTML

<http://gitlab.fjfi.cvut.cz/culikzde/view/-/blob/master/tutorial/plain-grammar/plain_tohtml.py>

``` C++
while_stat :  "while" "(" expr ")" stat   ;

if_stat    :  "if" "(" expr ")" stat ( "else" stat  )?   ;

compound_stat : "{" ( stat )* "}" ;

simple_stat :  expr ";" ;

empty_stat :  ";" ;

stat : while_stat | if_stat | compound_stat | simple_stat | empty_stat ;


simple_expr : identifier | number  | "(" expr ")" ;

mult_expr :  simple_expr ( ("*"|"/") simple_expr )* ;

add_expr :  mult_expr ( ("+"|"-") mult_expr )* ;

expr : add_expr ( "," expr )? ;


program : stat;
```

Výsledek <http://kmlinux.fjfi.cvut.cz/~culikzde/sos/gram.html>
![gram.html](/prekl//gram.html)
