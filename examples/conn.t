/* conn.t */

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
private :
     void addTableItem ();

public :
     // Window (QWidget * parent = null);
     void setup ();
     void put (QString s);
     void onButtonClick ();
     void onEditChanged ();

public:
    QSplitter * vsplitter
    {
       orientation = Qt::Vertical;
       QSplitter * hsplitter
       {
          // QTreeWidget  tree1 { }
          // QTableWidget table1 {  }
          QTreeWidget * tree
          {
            QTreeWidgetItem * branch
            {
               text = "tree branch";
               foreground = "brown";
               background = "orange";
               icon = "folder";
            }
          }
          QTableWidget * table
          {
             QTableWidgetItem * item
             {
                text = "abc";
             }
             setRowCount (2);
             setColumnCount (2);
             setItem (0, 0, item);
          }
          QTabWidget * tab { }
          QWidget * vbox
          {
             QVBoxLayout * vlayout
             {
                QLineEdit * pageName {  /* text = "first page"; */ }
                pageName.text = "First Page";
                pageName.textChanged = onEditChanged;
                QPushButton * button
                {
                   text = "Add Page";
                   clicked = onButtonClick;
                }
                QCheckBox * check_box
                {
                   text = "On";
                   checked = true;
                   toggled { if (checked) text = "On"; else text = "Off"; }
                }
                QGridLayout * grid
                {
                   QLabel * label1 { text = "Line"; }
                   QSpinBox * lineNumber { value = 1; }
                   QLabel * label2 { text = "Column"; }
                   QSpinBox * colNumber { value = 1; }
                   QLabel * label3 { text = "Value"; }
                   QLineEdit * fieldValue { text = "value"; }
                   QPushButton * fieldButton { text = "Add Table Item"; }
                   addWidget (label1, 0, 0);
                   addWidget (lineNumber, 0, 1);
                   addWidget (label2, 1, 0);
                   addWidget (colNumber, 1, 1);
                   addWidget (label3, 2, 0);
                   addWidget (fieldValue, 2, 1);
                   addWidget (fieldButton, 3, 0, 1, 2);
                   fieldButton.clicked = addTableItem;
                }
             }
          }
       }
       QTextEdit * info { }
       setStretchFactor (0, 4);
       setStretchFactor (1, 1);
    } 
   
    tree
    {
        doubleClicked () /* (QTreeWidgetItem * branch) */
        {
            QTreeWidgetItem * item = new QTreeWidgetItem (tree);
            item { text = "New Item"; }
            tree.addTopLevelItem (item);
            put ("Tree double click");
        }
    }

    // setup ();

};

void Window::put (QString s)
{
   info.append (s);
}

void Window::onEditChanged ()
{
    setWindowTitle (pageName->text);
    setWindowTitle (fieldValue->text);
}

void Window::onButtonClick ()
{
    button { text = "Click"; }
    QTextEdit * edit { }
    tab { addTab (edit, pageName.text); }
    put ("Add Page: " + pageName->text);
}

void Window::addTableItem ()
{
    QTableWidgetItem * item
    {
        text = fieldValue.text;
    }
    table
    {
        setItem (lineNumber.value, colNumber->value, item);
    }
    put ("Add table item");
}

/*
void modify (Window * window)
{
    window->tree
    {
         QTreeWidgetItem * branch
         {
            QTreeWidgetItem * node1 { text = "red"; foreground = "red"; }
            QTreeWidgetItem * node2 { text = "green"; foreground = "green"; }
            QTreeWidgetItem * node3 { text = "blue"; foreground = "blue"; }

            text = "tree branch";
            foreground = "brown";
            background = "orange";
            icon = "folder";
            expanded = true;
         }
         expandAll ();
         // itemExpanded { window->put ("Tree item expanded"); }
         // void itemEntered (QTreeWidgetItem *item, int column) { window->put ("Tree item entered"); }
    }
    window->tree->expandAll ();
}
*/

Window::setup ()
{
    table
    {
        // setRowCount (3);
        // itemSelectionChanged { put ("Table selection changed"); }
    }
    // table.setColumnCount (3);
    // table.itemClicked { put ("Table item clicked"); }
    // table->itemDoubleClicked(QTableWidgetItem *item) { put ("Table double click"); }

    // modify (this);
}

/*
Window::Window (QWidget * parent) :
   QMainWindow (parent)
{
    setup ();
    tab { }
    edit
    {
        text = "Tab Name";
        textChanged = onEditChanged;
    }
}
*/

int main (int argc, char * * argv)
{
     QApplication * appl = new QApplication (argc, argv);
     Window * window = new Window;
     window->show ();
     // window->setup ();
     // modify (window);
     appl->exec ();
}

/* ---------------------------------------------------------------------- */
