
/* init.t */

/* ---------------------------------------------------------------------- */

struct Basic
{
};

struct Note
{
    int cnt;
    char chr;
    Note (int p_cnt, char p_chr) : cnt (p_cnt), chr (p_chr) { }
};

struct Item : public Basic
{
    QString   name;
    bool      selected;
    int       size;
    double    value;
    double    eps = 0.1;
    Note      note (7, '*');
};

/* ---------------------------------------------------------------------- */

class Window : public QMainWindow
{
   public:

   Item * item
   {
      name = "abc";
      selected = true;
      size = 2;
      value = 3.14;
   }

   Item another
   {
      name = "another data";
      selected = true;
      size = 5;
      value = 3.14159;
   }

   another
   {
      name = "modified data";
   }

   QVBoxLayout * vlayout
   {
      QLineEdit * edit { text = item->name; item->name = text; }

      QString store;
      edit { store = text; text = store; }

      QPushButton * button
      {
          text = "store";
          clicked { edit { item->name = text; } }
      }
   }

   void displayData ()
   {
       edit { text = item->name; }
   }

   void storeData ()
   {
      edit { item->name = text; }
   }
};

/* ---------------------------------------------------------------------- */

int g = 1;

Note global_note (3, '.');

Item glob
{
    name = "global";
    value = 1.1;
};

Item * glob_ptr
{ 
    name = "global pointer";
    value = 1.2;
};

/* ---------------------------------------------------------------------- */

int main (int argc, char * * argv)
{
     int m = 2;
     Note loc_note (3, '*');

     Item loc
     {
         name = "loc";
         value = 2.1;
     }

     Item * loc_ptr
     {
         name = "local pointer";
         value = 2.2;
     };

     QApplication * appl = new QApplication (argc, argv);
     Window * win = new Window ();
     win->show ();
     appl->exec ();
}

/* ---------------------------------------------------------------------- */

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
