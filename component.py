#!/usr/bin/env python

from __future__ import print_function

import sys

from lexer import Lexer
from env import CodePlugin

# --------------------------------------------------------------------------

class Group (object) :
   def __init__ (self) :
      self.name = ""
      self.groups = [ ]
      self.packages = [ ]

class Package (object) :
   def __init__ (self) :
      self.name = ""
      self.packed_size = 0
      self.install_size = 0
      self.files = [ ]

class File (object) :
   def __init__ (self) :
      self.name = ""
      self.size = 0

# --------------------------------------------------------------------------

def readSource (fileName) :
    inp = Lexer ()
    inp.open (fileName, with_sections = True)
    dcl = object ()
    dcl.item_name = ""
    dcl.item_qual = ""
    while not inp.isEndOfSource () :
       readObject (inp, above)
    inp.close ()

# --------------------------------------------------------------------------

class ComponentPlugin (CodePlugin) :

   def __init__ (self, main_window) :
       super (Plugin, self).__init__ (main_window)

   def pluginFunction (self) :
       print ("COMPONENT PLUGIN", self.sourceFileName)
       readSource (self.sourceFileName)

# --------------------------------------------------------------------------

def style (dcl) :
    if "ink" in dcl.item_dict :
       dcl.item_ink = dcl.item_dict ["ink"].item_value
    if "paper" in dcl.item_dict :
       dcl.item_paper = dcl.item_dict ["paper"].item_value
    if "icon" in dcl.item_dict :
       dcl.item_icon = dcl.item_dict ["icon"].item_value
    if "tooltip" in dcl.item_dict :
       dcl.item_tooltip = dcl.item_dict ["tooltip"].item_value

    for item in dcl.item_list :
        style (item)

# --------------------------------------------------------------------------

def readValue (inp, above) :
    obj = None
    if inp.isKeyword ("None") :
       obj = None
    elif inp.isKeyword ("True") or inp.isKeyword ("true") :
       obj = True
    elif inp.isKeyword ("False") or inp.isKeyword ("false")  :
       obj = False
    elif inp.isNumber () :
       obj = int (inp.TokenText)
    elif inp.isReal () :
       obj = float (inp.TokenText)
    elif inp.isString () or inp.isCharacter () :
       obj = inp.TokenText
    elif inp.isSeparator ('[') :
       obj = [ ]
       inp.nextToken ()
       if not inp.isSeparator (']') :
          item = readValue (inp)
          obj.append (item)
          while inp.isSeparator (',') :
             inp.nextToken ()
             item = readValue (inp)
             obj.append (item)
       inp.checkSeparator (']')
    elif inp.isSeparator ('(') :
       obj = ( )
       inp.nextToken ()
       if not inp.isSeparator (')') :
          item = readValue (inp)
          obj = obj + ( item, )
          while inp.isSeparator (',') :
             inp.nextToken ()
             item = readValue (inp)
             obj = obj + ( item, )
       inp.checkSeparator (')')
    elif inp.isSeparator ('{') :
       obj = ( )
       inp.nextToken ()
       if not inp.isSeparator ('}') :
          key = readValue (inp)
          inp.checkSeparator (":")
          val = readValue (inp)
          obj [key] = val
          while inp.isSeparator (',') :
             inp.nextToken ()
             key = readValue (inp)
             inp.checkSeparator (":")
             val = readValue (inp)
             obj [key] = val
       inp.checkSeparator ('}')
    elif inp.isIdentifier () :
       obj = readObject (inp, above)
    else :
       inp.error ("Value expected");
    return obj

def readObject (inp, above) :
    obj = None
    name = inp.readIdentifier ("Type name expected")
    obj = eval (name + "()")

    inp.checkSeparator ("{")
    inp.openCompletion (above)
    while not inp.isSeparator ('}') :
       item_name = inp.readIdentifier ("Item name expected");

       dcl = object ()
       dcl.item_name = item_name
       dcl.item_list = [ ]
       dcl.item_dict = { }
       dcl.src_file = inp.prevFileInx
       dcl.src_line = inp.prevLineNum
       dcl.src_column = inp.prevColNum
       dcl.src_pos = inp.prevByteOfs
       dcl.src_end = inp.prevEndOfs
       if above.item_qual == "" :
          dcl.item_qual = dcl.item_name
       else :
          dcl.item_qual = above.item_qual + "." + dcl.item_name
       above.item_list.append (dcl)
       above.item_dict [item_name] = dcl
       inp.markDefn (dcl)
       inp.markOutline (dcl)

       inp.checkSeparator ("=")
       dcl.edit_pos = inp.tokenByteOfs
       item_value = readValue (inp, above)
       setattr (obj, item_name, item_value)
       dcl.item_value = item_value
       dcl.edit_end = inp.prevEndOfs
       inp.checkSeparator (";")
    inp.closeCompletion ()
    inp.checkSeparator ("}")
    return obj

# --------------------------------------------------------------------------

def writeValue (out, obj) :
    out.simpleSection (obj)
    if obj == None :
       out.put ("None")
    elif ( isinstance (obj, bool) or
           isinstance (obj, int) or
           isinstance (obj, long) or
           isinstance (obj, float) or
           isinstance (obj, complex) or
           isinstance (obj, str) ) :
       out.put (str (obj))
    elif isinstance (obj, list) :
       out.putLn ("[")
       out.indent ()
       any = False
       for item in obj :
          if any :
             out.putLn (",")
          any = True
          writeValue (out, item)
       out.unindent ()
       out.putLn ("]")
    elif isinstance (obj, tuple) :
       out.putLn ("(")
       out.indent ()
       any = False
       for item in obj :
          if any :
             out.putLn (",")
          any = True
          writeValue (out, item)
       out.unindent ()
       out.putLn (")")
    elif isinstance (obj, dict) :
       out.putLn ("{")
       out.indent ()
       any = False
       for key in obj :
          if any :
             out.putLn (",")
          any = True
          out.put (key)
          out.put (": ")
          writeValue (out, obj [key])
       out.unindent ()
       out.putLn ("}")
    else :
       writeObject (out, obj)

def writeObject (out, obj) :
    out.openSection (obj)
    out.putLn (type (obj).__name__)
    out.putLn ("{")
    out.indent ()

    properties = obj.__dict__
    if hasattr (obj, "_properties_") :
       properties = obj._properties_

    for item_name in properties :
        item_obj = getattr (obj, item_name, None)
        out.put (item_name)
        out.put (" = ")
        writeValue (out, item_obj)
        out.putLn (";")

    out.unindent ()
    out.putLn ("}")
    out.closeSection ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
