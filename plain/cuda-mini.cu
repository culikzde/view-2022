#include <stdlib.h>
#include <stdio.h>

#include <cuda.h>
#include <cuda_runtime.h>

#define BLOCKSIZE 16

/*******************/
/* iDivUp FUNCTION */
/*******************/
int iDivUp(int a, int b){ return ((a % b) != 0) ? (a / b + 1) : (a / b); }

/********************/
/* CUDA ERROR CHECK */
/********************/
// --- Credit to http://stackoverflow.com/questions/14038589/what-is-the-canonical-way-to-check-for-errors-using-the-cuda-runtime-api
void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
    if (code != cudaSuccess)
    {
	fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
	if (abort) { exit(code); }
    }
}

void gpuErrchk(cudaError_t ans) { gpuAssert((ans), __FILE__, __LINE__); }

/*******************/
/* addCPU FUNCTION */
/*******************/
void addCPU(float *h_a, float *h_b, float *h_c, int N) {

    for (int k = 0; k < N; k++) h_c[k] = h_a[k] + h_b[k];

}

/*******************/
/* addGPU FUNCTION */
/*******************/
__global__ void addGPU(float *d_a, float *d_b, float *d_c, int N) {

    int tid = threadIdx.x + blockIdx.x * blockDim.x; 

    if (tid >= N) return;

    d_c[tid] = d_a[tid] + d_b[tid];

}

/********/
/* MAIN */
/********/
int main() {

    const int N = 256;

    // --- Allocating host memory for data and results
    float *h_a		= (float *)malloc(N * sizeof(float));
    float *h_b		= (float *)malloc(N * sizeof(float));
    float *h_c		= (float *)malloc(N * sizeof(float));
    float *h_c_device = (float *)malloc(N * sizeof(float));

    // --- Allocating device memory for data and results
    float *d_a, *d_b, *d_c;
    gpuErrchk(cudaMalloc(&d_a, N * sizeof(float)));
    gpuErrchk(cudaMalloc(&d_b, N * sizeof(float)));
    gpuErrchk(cudaMalloc(&d_c, N * sizeof(float)));

    // --- Filling the input vectors on host memory
    for (int k = 0; k < N; k++) {
	h_a[k] = k;
	h_b[k] = 2 * k;
    }

    // --- Moving data from host to device
    gpuErrchk(cudaMemcpy(d_a, h_a, N * sizeof(float), cudaMemcpyHostToDevice));
    gpuErrchk(cudaMemcpy(d_b, h_b, N * sizeof(float), cudaMemcpyHostToDevice));

    addCPU(h_a, h_b, h_c, N);

    addGPU << <iDivUp(N, BLOCKSIZE), BLOCKSIZE >> >(d_a, d_b, d_c, N);
    gpuErrchk(cudaDeviceSynchronize());
    gpuErrchk(cudaPeekAtLastError());

    gpuErrchk(cudaMemcpy(h_c_device, d_c, N * sizeof(float), cudaMemcpyDeviceToHost));
    
    for (int k = 0; k < N; k++) 
    {
        printf("%f ... %f \n", h_c[k], h_c_device[k]);
	if (h_c_device[k] != h_c[k]) {
	    printf("Host and device results do not match for k = %d: h_c[%d] = %f; h_c_device[%d] = %f\n", k, k, h_c[k], k, h_c_device[k]);
	}
    }

    printf("No errors found.\n");
    
    return 0;
}
/* ---------------------------------------------------------------------- */

// compile: /opt/cuda-10.2/bin/nvcc plain/cuda-mini.cu -o _output/cuda-mini.bin

// pacman -S cuda ( nvidia-470xx-dkms nvidia-470xx-utils )
// apt-get install nvidia-driver nvidia-cuda-toolkit

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

