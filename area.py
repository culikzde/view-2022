#!/usr/bin/env python

from __future__ import print_function

import sys

if __name__ == '__main__' :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
else :
   from util import import_qt_modules
   import_qt_modules (globals ())

   from util import findColor, bytearray_to_str, sort_list
   from tree import TreeItem

factories = { }

# --------------------------------------------------------------------------

class DropProperties (object) :

   def __init__ (self) :
       self.setAcceptDrops (True)

   def dragEnterEvent (self, event) :

       mime = event.mimeData ()

       if (mime.hasColor () and not getattr (self, "topLevel", False) or
           mime.hasFormat ("application/x-view-component") ) :
           event.acceptProposedAction ()
       else :
           event.setAccepted (False)

   def dropEvent (self, event) :
       mime = event.mimeData ()

       if mime.hasColor() and not getattr (self, "topLevel", False) :
          color = findColor (mime.colorData())
          if event.dropAction () == Qt.CopyAction : # Ctlr mouse
             self.setPen (color)
          else :
             self.setBrush (color)

       if mime.hasFormat ("application/x-view-component") :
           name = bytearray_to_str (mime.data ("application/x-view-component"))
           item = self.createComponent (name)
           if item != None :
              item.setParentItem (self)
              item.setPos (event.scenePos() - self.scenePos())

   def createComponent (self, name) :
       item = None
       if name in factories :
          func = factories [name]
          item = func ()
          if isinstance (item, QWidget) :
             proxy = QGraphicsProxyWidget ()
             proxy.setWidget (item)
             proxy.setWindowFlags (Qt.Dialog)
             item = proxy
             item.setFlag (QGraphicsItem.ItemIsMovable)
             item.setFlag (QGraphicsItem.ItemIsSelectable)
       return item

# --------------------------------------------------------------------------

class ItemProperties (DropProperties) :

   _properties_ = [ "x", "y", "pen", "brush" ]

   def __init__ (self) :
       super (ItemProperties, self).__init__ ()

   def x (self) :
       return  self.pos().x()

   def y (self) :
       return  self.pos().y()

   def setX (self, value) :
       p = self.pos ()
       p.setX (value)
       self.setPos (p)

   def setY (self, value) :
       p = self.pos ()
       p.setY (value)
       self.setPos (p)

   def _items_ (self) :
       return self.childItems ()

# --------------------------------------------------------------------------

class RectangleProperties (ItemProperties) :

   _properties_ = [ "x", "y", "width", "height", "pen", "brush" ]

   def __init__ (self) :
       super (RectangleProperties, self).__init__ ()

   def width (self) :
       return  self.rect().width()

   def height (self) :
       return  self.rect().height()

   def setWidth (self, value) :
       r = self.rect ()
       r.setWidth (value)
       self.setRect (r)

   def setHeight (self, value) :
       r = self.rect ()
       r.setHeight (value)
       self.setRect (r)

# --------------------------------------------------------------------------

class Area (QGraphicsRectItem, RectangleProperties):

   def __init__ (self, parent=None) :
       super (Area, self).__init__ (parent)

       self.Title = ""
       self.topLevel = False

       self.setRect (0, 0, 100, 100)

       self.setFlag (QGraphicsItem.ItemIsMovable)
       self.setFlag (QGraphicsItem.ItemIsSelectable)
       self.setFlag (QGraphicsItem.ItemSendsScenePositionChanges)

       # self.setAcceptDrops (True)

   def setTitle (self, text) :
       self.Title = text

   def addArea (self, area) :
       area.setParentItem (self)

   def addSource (self, source) :
       source.setParentItem (self)

   def addTarget (self, target) :
       target.setParentItem (self)

   def addItem (self, item) :
       self.addArea (item)

   def addWidget (self, item) :
       proxy = QGraphicsProxyWidget ()
       proxy.setWidget (item)
       proxy.setWindowFlags (Qt.Dialog)
       item = proxy
       item.setFlag (QGraphicsItem.ItemIsMovable)
       item.setFlag (QGraphicsItem.ItemIsSelectable)
       return item

   def updateAllConnections (self) :
       for item in self.childItems () :
           if isinstance (item, Area) :
              item.updateAllConnections ()
           if isinstance (item, Source) :
              for conn in item.connections :
                 conn.updateConnection ()
           if isinstance (item, Target) :
              for conn in item.connections :
                 conn.updateConnection ()

   def itemChange (self, change, value) :
       # if not use_python3 or use_pyqt5 or use_pyside2 : # !? not working with python3 and qt4
       if change == QGraphicsItem.ItemPositionChange :
              # self.setBrush (findColor ("red"))

              # update connections with source in this area
              for src in self.childItems () :
                  if isinstance (src, Source) :
                     for conn in src.connections :
                        conn.updateConnection ()
                  if isinstance (src, Target) :
                     for conn in src.connections :
                        conn.updateConnection ()

       return super (Area, self).itemChange (change, value)

# --------------------------------------------------------------------------

class BlockItem (Area):

   def __init__ (self, parent=None) :
       super (BlockItem, self).__init__ (parent)
       self.moving = False
       # self.blocked = False
       self.origBrush = None

   def mousePressEvent (self, event) :
       if not self.moving :

          self.origBrush = self.brush ()
          self.setBrush (findColor ("blue"))
          self.moving = True
          self.setZValue (0)

          # parent = self.parentItem ()
          # if parent != None :
          #    for item in parent.childItems () :
          #        if isinstance (item, BlockItem) and not item.moving :
          #           item.blocked = True

       return super (BlockItem, self).mousePressEvent (event)

   def mouseReleaseEvent (self, event) :
       if self.moving :

          self.setBrush (self.origBrush)

          # parent = self.parentItem ()
          # if parent != None :
          #    for item in parent.childItems () :
          #        if isinstance (item, BlockItem) :
          #           item.blocked = False

          self.moving = False
          self.arrangeItems ()

       return super (BlockItem, self).mouseReleaseEvent (event)

   # def itemChange (self, change, value) :
   #     if change == QGraphicsItem.ItemPositionChange :
   #        pass
   #     return super (BlockItem, self).itemChange (change, value)

   def arrangeItems (self) :
       parent = self.parentItem ()
       if parent != None :
          space = getattr (parent, "arrangeSpace", 10)

          item_list = [ ]
          for item in parent.childItems () :
              if isinstance (item, BlockItem) :
                item_list.append (item)
          # item_list.sort (cmp=compare)
          sort_list (item_list, compare)

          # print ()
          # for item in item_list :
          #    print (item.Title, item.x(), item.y())

          y = getattr (parent, "arrangeTitle", 0)
          for item in item_list :
              if isinstance (item, BlockItem) and not item.moving :
                 y = y + space
                 item.setPos (space, y)
                 y = y + item.rect().height()

# --------------------------------------------------------------------------

def compare (a, b) :
    ay = a.y ()
    by = b.y ()
    if ay < by :
       return -1
    elif ay == by :
       return 0
    else :
       return 1

# --------------------------------------------------------------------------

class Source (QGraphicsRectItem, RectangleProperties) :
   def __init__ (self, parent=None) :
       super (Source, self).__init__ (parent)
       self.connections = [ ]
       self.setRect (0, 0, 10, 10)
       self.setPen (findColor ("red"))
       self.setBrush (findColor ("lime"))
       self.setFlag (QGraphicsItem.ItemIsMovable)
       self.setFlag (QGraphicsItem.ItemIsSelectable)

   def getArea (self) :
       return self.parentItem ()

# --------------------------------------------------------------------------

class Target (QGraphicsRectItem, RectangleProperties) :
   def __init__ (self, parent=None) :
       super (Target, self).__init__ (parent)
       self.connections = [ ]
       self.setRect (0, 0, 10, 10)
       self.setPen (findColor ("red"))
       self.setBrush (findColor ("yellow"))
       self.setFlag (QGraphicsItem.ItemIsMovable)
       self.setFlag (QGraphicsItem.ItemIsSelectable)

   def getArea (self) :
       return self.parentItem ()

# --------------------------------------------------------------------------

class Connection (object) :
   def __init__ (self) :
      self.source = None
      self.target = None

      self.x1 = 0
      self.y1 = 0
      self.group = None
      self.pen = None

   def setSource (self, item) :
       self.source = item
       if item != None :
          item.connections.append (self)

   def setTarget (self, item) :
       self.target = item
       item.connections.append (self)

   def addLine (self, x2, y2) :
      if self.x1 != x2 or self.y1 != y2 :

         line = QGraphicsLineItem ()
         line.setLine (self.x1, self.y1, x2, y2)
         line.setPen (self.pen)
         line.setParentItem (self.group)

         self.x1 = x2
         self.y1 = y2

   def horizLine (self, x2) :
       self.addLine (x2, self.y1)

   def vertLine (self, y2) :
       self.addLine (self.x1, y2)

   def updateConnection (self) :

       scene = self.source.scene ()

       # delete previous group
       if self.group != None and scene != None:
          scene.removeItem (self.group)
          self.group = None

       # create new group
       self.group = QGraphicsItemGroup ()
       self.group.setFlag (QGraphicsItem.ItemHasNoContents)

       pt1 = self.source.mapToScene (self.source.rect().center ())
       pt2 = self.target.mapToScene (self.target.rect().center ())

       self.x1 = pt1.x()
       self.y1 = pt1.y()

       x2 = pt2.x()
       y2 = pt2.y()

       self.pen = QPen (findColor ("red"))

       a1 = self.source.getArea ()
       r1 = a1.rect()
       tr1 = a1.mapToScene (r1.topRight())
       br1 = a1.mapToScene (r1.bottomRight())
       src_right = tr1.x()
       src_top = tr1.y()
       src_bottom = br1.y()

       a2 = self.target.getArea ()
       r2 = a2.rect()
       tr2 = a2.mapToScene (r2.topRight())
       br2 = a2.mapToScene (r2.bottomRight())
       tgt_right = tr2.x()
       tgt_top = tr2.y()
       tgt_bottom = br2.y()

       delta = 20

       if self.x1 + 2*delta < x2 :
          x = (self.x1 + x2) / 2
          self.horizLine (x) # half way in horizontal direction

          self.vertLine (y2)
          self.horizLine (x2)
       else :
          # real target
          x3 = x2
          y3 = y2

          # point behind target
          x2 -= delta
          y2 -= delta

          self.horizLine (self.x1 + delta) # small starting line

          y = (self.y1 + y2) / 2

          if src_bottom + delta <= tgt_top :
             y = (src_bottom + tgt_top)/2
             self.vertLine (y) # half way in vertical direction
             self.horizLine (x2) # line in horizontal direction

          elif tgt_bottom + delta <= src_top :
             y = (tgt_bottom + src_top)/2
             self.vertLine (y) # half way in vertical direction
             self.horizLine (x2) # line in horizontal direction

          elif self.y1 < y2 :
             # go below target
             if tgt_right > src_right :
                self.horizLine (tgt_right + delta)
             else :
                self.horizLine (src_right + delta)
             self.vertLine (tgt_bottom + delta)
             self.horizLine (x2)

          else :
             # go above target
             if tgt_right > src_right :
                self.horizLine (tgt_right + delta)
             else :
                self.horizLine (src_right + delta)
             self.vertLine (tgt_top - delta)
             self.horizLine (x2)

          # to center of target
          self.vertLine (y3)
          self.horizLine (x3)

       if scene != None :
          scene.addItem (self.group)

# --------------------------------------------------------------------------

class Palette (QTabWidget):

   def __init__ (self, parent=None) :
       super (Palette, self).__init__ (parent)

   def addPage (self, name, page) :
       self.addTab (page, name)

class PalettePage (QToolBar):

   def __init__ (self, parent=None) :
       super (PalettePage, self).__init__ (parent)

   def addItem (self, name, item) :
       item.setText (name)
       item.setToolTip (name)
       self.addWidget (item)

class PaletteItem (QToolButton):

   def __init__ (self, parent=None) :
       super (PaletteItem, self).__init__ (parent)

   def setupPixmap (self, drag) :
       pass

   def mousePressEvent (self, event) :

       if event.button() == Qt.LeftButton :
          drag =  QDrag (self)

          mimeData = QMimeData ()
          self.setupMimeData (mimeData)
          drag.setMimeData (mimeData)

          self.setupPixmap (drag)

          dropAction = drag.exec_ (Qt.MoveAction | Qt.CopyAction | Qt.LinkAction)

# --------------------------------------------------------------------------

class ColorItem (PaletteItem):

   def __init__ (self, page, name, color, parent=None) :
       super (ColorItem, self).__init__ (parent)
       self.name = name
       self.setColor (color)
       page.addItem (self.name, self)

   def setColor (self, color) :
       self.color = color

       p = self.palette () # Qt color palette
       p.setColor (QPalette.ButtonText, self.color)
       self.setPalette (p)

       pixmap = QPixmap (12, 12)
       pixmap.fill (Qt.transparent)

       painter = QPainter (pixmap)
       painter.setPen (Qt.NoPen)
       painter.setBrush (QBrush (self.color))
       painter.drawEllipse (0, 0, 12, 12)
       painter.end ()

       self.setIcon (QIcon (pixmap))

   def setupMimeData (self, data) :
       data.setColorData (self.color)

   def setupPixmap (self, drag) :

       pixmap = QPixmap (12, 12)
       pixmap.fill (Qt.transparent)

       painter = QPainter (pixmap)
       painter.setPen (Qt.NoPen)
       painter.setBrush (QBrush (self.color))
       painter.drawEllipse (0, 0, 12, 12)
       painter.end ()

       drag.setPixmap (pixmap)
       drag.setHotSpot (QPoint (6, 6))

# --------------------------------------------------------------------------

class ComponentItem (PaletteItem):

   def __init__ (self, page, name, func, parent=None) :
       super (ComponentItem, self).__init__ (parent)
       self.name = name
       factories [self.name] = func
       page.addItem (self.name, self)

   def setupMimeData (self, data) :
       data.setData ("application/x-view-component", bytearray (self.name, "ascii"))

# --------------------------------------------------------------------------

def mix (obj, cls) :
    base_cls = obj.__class__
    # if isinstance (cls, list) :
    #    base_list = (item for item in cls)
    #    obj.__class__ = type (base_cls.__name__, base_list, {})
    obj.__class__ = type (base_cls.__name__, (base_cls, cls), {})
    if "__init__" in cls.__dict__ :
       cls.__init__ (obj)

def ellipse () :
    item = QGraphicsEllipseItem ()
    mix (item, RectangleProperties)
    item.setRect (0, 0, 100, 50)
    item.setPen (findColor ("blue"))
    item.setBrush (findColor ("yellow"))
    item.setFlag (QGraphicsItem.ItemIsMovable)
    item.setFlag (QGraphicsItem.ItemIsSelectable)
    return item

def triangle () :
    item = QGraphicsPolygonItem ()
    mix (item, ItemProperties)
    polygon = QPolygonF ()
    polygon.append (QPointF (25, 0))
    polygon.append (QPointF (0,  50))
    polygon.append (QPointF (50, 50))
    item.setPolygon (polygon)

    item.setPen (findColor ("red"))
    item.setBrush (findColor ("lime"))
    item.setFlag (QGraphicsItem.ItemIsMovable)
    item.setFlag (QGraphicsItem.ItemIsSelectable)
    return item

def wave () :
    item = QGraphicsPathItem ()
    mix (item, ItemProperties)
    path = QPainterPath ()
    path.cubicTo (QPointF (50, 0), QPointF (0,  50), QPointF (50, 50));
    item.setPath (path)

    item.setPen (findColor ("brown"))
    brush = QLinearGradient (QPointF (0, 0), QPointF (50,  50))
    brush.setColorAt (0, findColor ("yellow"))
    brush.setColorAt (1, findColor ("orange"))
    item.setBrush (brush)
    item.setFlag (QGraphicsItem.ItemIsMovable)
    item.setFlag (QGraphicsItem.ItemIsSelectable)
    return item

def createArea () :
    item = Area ()
    item.setPen (findColor ("lime"))
    item.setBrush (findColor ("yellow"))
    item.setTitle ("C")
    item.setToolTip ("C")
    return item

def createSubArea () :
    item = Area ()
    item.setTitle ("orange");
    item.setPen (findColor ("red"))
    item.setBrush (findColor ("orange"))
    item.setToolTip ("Subitem")
    item.setPos (10, 10)
    item.setRect (0, 0, 80, 20)
    return item

def createSource () :
    return Source ()

def createTarget () :
    return Target ()

def createBlockItem () :
    item = BlockItem ()
    item.setPen (findColor ("red"))
    item.setBrush (findColor ("wheat"))
    item.setPos (10, 10)
    item.setRect (0, 0, 80, 20)
    item.setTitle ("block_item")
    item.setToolTip ("Block Item")
    return item

def createButton () :
    return QPushButton ()

def createEdit () :
    return QLineEdit ()

def createTree () :
    widget = QTreeWidget ()
    branch = QTreeWidgetItem ()
    branch.setText (0, "abc")
    widget.addTopLevelItem (branch)
    node = QTreeWidgetItem ()
    node.setText (0, "def")
    branch.addChild (node)
    return widget

def createList () :
    return QListWidget ()

def initPalette (palette) :
    page = PalettePage ()

    colors = [
       "red",
       "blue",
       "green",
       "yellow",
       "orange",
       "silver",
       "gold",
       "goldenrod",
       "lime",
       "lime green",
       "yellow green",
       "green yellow",
       "forest green",
       "coral",
       "cornflower blue",
       "dodger blue",
       "royal blue",
       "wheat",
       "chocolate",
       "peru",
       "sienna",
       "brown"
    ]

    for color_name in colors :
        ColorItem (page, color_name, findColor (color_name.replace (" ", "")))

    palette.addPage ("Colors", page)

    page = PalettePage ()

    ComponentItem (page, "ellipse", ellipse)
    ComponentItem (page, "triangle", triangle)
    ComponentItem (page, "wave", wave)

    palette.addPage ("Shapes", page)

    page = PalettePage ()

    ComponentItem (page, "area", createArea)
    ComponentItem (page, "sub-area", createSubArea)
    ComponentItem (page, "source", createSource)
    ComponentItem (page, "target", createTarget)

    ComponentItem (page, "block-item", createBlockItem)

    ComponentItem (page, "button", createButton)
    ComponentItem (page, "edit", createEdit)
    ComponentItem (page, "tree", createTree)
    ComponentItem (page, "list", createList)

    palette.addPage ("Components", page)

# --------------------------------------------------------------------------

def example (scene) :

       # example 1

       a = Area ()
       a.setPen (findColor ("green"))
       a.setBrush (findColor ("yellow"))
       a.setPos (10, 10)
       a.setTitle ("A")
       a.setToolTip ("A")
       scene.addItem (a)

       a_source = Source ()
       a_source.setPos (90, 0)
       a.addSource (a_source)

       a_target = Target ()
       a.addTarget (a_target)

       b = Area ()
       b.setPen (findColor ("red"))
       b.setBrush (findColor ("cornflowerblue"))
       b.setPos (320, 10)
       b.setTitle ("B")
       b.setToolTip ("B")
       scene.addItem (b)

       b_target = Target ()
       b.addTarget (b_target)

       b_source = Source ()
       b_source.setPos (90, 0)
       b.addSource (b_source)


       c = Connection ()
       c.setSource (a_source)
       c.setTarget (b_target)
       c.updateConnection ()

       s = Area ()
       s.setTitle ("orange")
       s.setPen (findColor ("red"))
       s.setBrush (findColor ("orange"))
       s.setToolTip ("Subitem")
       s.setPos (10, 10)
       s.setRect (0, 0, 80, 20)
       a.addArea (s)

       t = Area ()
       t.setTitle ("orange 2")
       t.setPen (findColor ("red"))
       t.setBrush (findColor ("orange"))
       t.setToolTip ("Subitem 2")
       t.setPos (10, 10)
       t.setRect (0, 0, 80, 20)
       b.addArea (t)

       # example 2

       d = Area ()
       d.setPen (findColor ("lime"))
       d.setBrush (findColor ("yellow"))
       d.setPos (10, 200)
       d.setRect (0, 0, 140, 200)
       scene.addItem (d)

       for i in range (3) :
          e = BlockItem ()
          e.setPen (findColor ("red"))
          e.setBrush (findColor ("orange"))
          e.setPos (10, 10 + 30*i)
          e.setRect (0, 0, 120, 20)
          e.setTitle ("column" + str (i+1))
          d.addArea (e)

          f = QGraphicsSimpleTextItem ()
          f.setText ("column" + str (i+1) + ": type" + str (i+1))
          # f.setPen (findColor ("blue"))
          # f.setBrush (findColor ("yellow"))
          f.setPos (10, 5)
          f.setParentItem (e)

       # example 3

       pixmap = QPixmap (12, 12)
       pixmap.fill (Qt.transparent)
       painter = QPainter (pixmap)
       painter.setPen (Qt.NoPen)
       painter.setBrush (findColor ("orange"))
       painter.drawEllipse (0, 0, 12, 12)
       painter.end ()
       icon = QIcon (pixmap)

       tree = QTreeWidget ()
       branch = QTreeWidgetItem ()
       branch.setText (0, "abc")
       branch.setIcon (0, icon)
       node = QTreeWidgetItem ()
       node.setText (0, "def")
       branch.addChild (node)
       tree.addTopLevelItem (branch)

       treeWidget = scene.addWidget (tree)
       pushButton = scene.addWidget (QPushButton ())

       layout = QGraphicsLinearLayout ()
       layout.setOrientation (Qt.Vertical)
       layout.addItem (treeWidget)
       layout.addItem (pushButton)

       form = QGraphicsWidget ()
       form.setLayout (layout)
       form.setWindowFlags (Qt.Dialog | Qt.CustomizeWindowHint | Qt.WindowSystemMenuHint | Qt.WindowTitleHint | Qt.WindowMinMaxButtonsHint | Qt.WindowCloseButtonHint)
       form.setWindowTitle ("Window")
       form.setPos (320, 200)
       scene.addItem (form)

# --------------------------------------------------------------------------

class GraphicsWin (QGraphicsView):

   def __init__ (self, win = None) :
       super (GraphicsWin, self).__init__ (win)
       self.win = win

       scene = QGraphicsScene (self)
       scene.setSceneRect (0, 0, 800, 600)
       self.setScene (scene)

       # connect

       scene.selectionChanged.connect (self.onSelectionChanged)

       # background

       n = 16
       texture = QBitmap (n, n)
       texture.clear ()

       painter = QPainter (texture)
       painter.drawLine (0, 0, n-1, 0)
       painter.drawLine (0, 0, 0, n-1)
       painter.end ()

       brush = QBrush (findColor ("cornflowerblue"))
       brush.setTexture (texture)
       scene.setBackgroundBrush (brush)

       # plane

       plane = Area ()
       plane.topLevel = True
       plane.setPen (QPen (Qt.NoPen))
       plane.setBrush (QBrush (Qt.NoBrush))
       # plane.setPen (QPen (findColor ("cornflowerblue")))
       # plane.setBrush (brush)
       plane.setRect (0, 0, 640, 480)
       scene.addItem (plane)

       # example

       example (plane)

   def onSelectionChanged (self) :
       items = self.scene().selectedItems()
       if self.win != None :
          if len (items) == 1 :
             item = items [0]
          else :
             item = None
          self.win.showProperties (item)

# --------------------------------------------------------------------------

class GraphicsWithPalette (QWidget) :

   def __init__ (self, win) :
       super (GraphicsWithPalette, self).__init__ (win)
       self.win = win

       layout = QVBoxLayout ()
       self.setLayout (layout)

       self.palette = Palette ()
       initPalette (self.palette)
       layout.addWidget (self.palette)
       layout.setStretchFactor (self.palette, 0)

       self.design = GraphicsWin (self.win)
       layout.addWidget (self.design)
       layout.setStretchFactor (self.design, 1)

   def addArea (self, item) :
       scene = self.design.scene ()
       scene.addItem (item)

   def showNavigator (self, tree) :
       for item in self.design.scene ().items () :
           if item.parentItem () == None :
              self.showBranch (tree, item)

   def showBranch (self, above, item) :
       text = getattr (item, "Title", "")
       if text == "" :
          text = str (item)
       node = TreeItem (above, text)
       node.obj = item
       for subitem in item.childItems () :
           self.showBranch (node, subitem)

   def clear (self) :
       scene = self.design.scene()
       for item in scene.items () :
           scene.removeItem (item)

# --------------------------------------------------------------------------

if __name__ == '__main__' :
   class Tree (QTreeWidget):
      def __init__ (self, win = None) :
          super (Tree, self).__init__ (win)
          self.win = win
          self.itemClicked.connect (self.onItemClicked)
          self.item_list = [ ]
          self.item_dict = { }
          self.item_value = None

      def onItemClicked (self, node, column) :
          self.win.prop.showProperties (node)

   class TreeItem (QTreeWidgetItem):
      def __init__ (self, above = None, text = "") :
          super (TreeItem, self).__init__ (above)
          if text != "" :
             self.setText (0, text)

# --------------------------------------------------------------------------

if __name__ == '__main__' :
   class Property (QTableWidget) :

      def __init__ (self, win = None) :
          super (Property, self).__init__ (win)
          self.win = win
          self.setColumnCount (2)
          self.setRowCount (0)
          self.setHorizontalHeaderLabels (["Name", "Value"])
          self.horizontalHeader().setStretchLastSection (True)

      def showProperty (self, name, value) :
          inx = self.rowCount ()
          self.setRowCount (inx+1)

          item = QTableWidgetItem ()
          item.setText (name)
          item.setFlags (item.flags () & ~ Qt.ItemIsEditable)
          self.setItem (inx, 0, item)

          item = QTableWidgetItem ()
          item.setText (str (value))
          self.setItem (inx, 1, item)

          return item

      def showProperties (self, dcl) :
          self.setRowCount (0)
          for name in dcl.item_dict :
              data = dcl.item_dict [name]
              item = self.showProperty (name, data.item_value)
              item.edit_pos = data.edit_pos
              item.edit_end = data.edit_end

# --------------------------------------------------------------------------

if __name__ == '__main__' :
   class SimpleWindow (QMainWindow) :

      def __init__ (self, parent = None) :
          super (SimpleWindow, self).__init__ (parent)

          # left
          self.tree = Tree (self)

          # middle
          self.builder = GraphicsWithPalette (self)

          # right
          self.prop = Property (self)

          # bottom
          self.info = QPlainTextEdit (self)

          # layout

          self.hsplitter = QSplitter ()
          self.hsplitter.addWidget (self.tree)
          self.hsplitter.addWidget (self.builder)
          self.hsplitter.addWidget (self.prop)
          self.hsplitter.setStretchFactor (0, 1)
          self.hsplitter.setStretchFactor (1, 4)
          self.hsplitter.setStretchFactor (2, 1)

          self.vsplitter = QSplitter ()
          self.vsplitter.setOrientation (Qt.Vertical)
          self.vsplitter.addWidget (self.hsplitter)
          self.vsplitter.addWidget (self.info)
          self.vsplitter.setStretchFactor (0, 3)
          self.vsplitter.setStretchFactor (1, 1)

          self.setCentralWidget (self.vsplitter)

          # file menu

          self.fileMenu = self.menuBar().addMenu ("&File")
          menu = self.fileMenu

          act = QAction ("&Quit", self)
          act.setShortcut ("Ctrl+Q")
          act.setShortcutContext (Qt.ApplicationShortcut)
          act.setIcon (QIcon ("application-exit"))
          act.triggered.connect (self.quit)
          menu.addAction (act)

          self.toolbar = self.addToolBar ("main toolbar").addAction (act)
          self.status = self.statusBar ()

      def showProperties (self, data) :
          self.prop.showProperties (data)

      def quit (self) :
          QApplication.instance().quit()

# --------------------------------------------------------------------------

if __name__ == '__main__' :

   def bytearray_to_str (b) :
       return str (b, "ascii")

   def findColor (name) :
       return QColor (name)

   if 0 :
      app = QApplication (sys.argv)
      win = GraphicsWithPalette (None)
      win.show ()
      app.exec_ ()
   else :
      app = QApplication (sys.argv)
      win = SimpleWindow ()
      win.show ()
      app.exec_ ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
