#!/usr/bin/env python

import sys

from util import use_pyside2, use_pyqt5, qstringref_to_str
if use_pyside2 :
   from PySide2.QtCore import *
   from PySide2.QtGui import *
   from PySide2.QtWidgets import *
   from PySide2.QtWidgets import *
   from PySide2.QtNetwork import *
elif use_pyqt5 :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
   from PyQt5.QtSvg import *
   from PyQt5.QtNetwork import *
else :
   from PyQt4.QtCore import *
   from PyQt4.QtGui import *
   from PyQt4.QtSvg import *
   from PyQt4.QtNetwork import *

# --------------------------------------------------------------------------

class SvgView (QWidget) :

   def __init__ (self, win) :
       super (SvgView, self).__init__ (win)
       self.win = win

       layout = QVBoxLayout ()
       self.setLayout (layout)

       self.image = QSvgWidget ()
       layout.addWidget (self.image)

       # self.loadFromFile ("/abc/tiger.svg")

       if 1 :
          manager = QNetworkAccessManager (self)
          manager.finished.connect (self.replyFinished)

          request = QNetworkRequest ()
          request.setUrl (QUrl ("https://dev.w3.org/SVG/tools/svgweb/samples/svg-files/tiger.svg"))

          reply = manager.get (request)

   def replyFinished (self, reply) :
       print ("replyFinished")
       answer = reply.readAll ()
       self.loadByteArray (answer)
       self.showTree (answer)

   def loadByteArray (self, source) :
       self.image.load (source)
       self.showTree (source)

   def loadFromFile (self, fileName) :
       self.image.load (fileName)

       f = QFile (fileName)
       if f.open (QFile.ReadOnly) :
          self.showTree (f.readAll ())
          f.close ()

   def showTree (self, source) :
       reader = QXmlStreamReader (source)

       top = QTreeWidgetItem ()
       top.setExpanded (True)

       current = top

       while not reader.atEnd() :
          if reader.isStartElement () :
             item = QTreeWidgetItem ()
             item.setText (0, qstringref_to_str (reader.name ()))
             item.setForeground (0, QColor (0, 0, 255))
             # item.line = reader.lineNumber ()
             current.addChild (item)
             item.setExpanded (True)
             current = item

             attrs = reader.attributes ()
             cnt = attrs.size ()
             for i in range (cnt) :
                a = attrs.at (i) # QXmlStreamAttribute
                node = QTreeWidgetItem ()
                node.setText (0, qstringref_to_str (a.name ()) + " = " + qstringref_to_str (a.value ()))
                node.setForeground (0, QColor (218, 165, 32))
                item.addChild (node)

          elif reader.isEndElement () :
             current = current.parent ()

          elif reader.isCharacters () and not reader.isWhitespace () :
             item = QTreeWidgetItem ()
             item.setText (0, qstringref_to_str (reader.text ()))
             item.setForeground (0, QColor (0, 192, 0))
             item.line = reader.lineNumber ()
             current.addChild (item)

          reader.readNext ()

       if reader.hasError() :
          item = QTreeWidgetItem ()
          item.setText (0, reader.errorString ())
          item.setForeground (0, QColor (255, 0, 0))
          top.addChild (item)

       self.win.addView ("SVG", self)
       self.win.addNavigatorData (self, top)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
