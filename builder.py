#!/usr/bin/env python

from __future__ import print_function

import sys

if __name__ == '__main__' :
   from PyQt5.QtCore import *
   from PyQt5.QtGui import *
   from PyQt5.QtWidgets import *
else :
   from util import import_qt_modules
   import_qt_modules (globals ())

   from util import findIcon, bytearray_to_str, empty_qstringlist, mouse_event_pos
   from tree import TreeItem

# --------------------------------------------------------------------------

class Adapter :

   def __init__ (self, builder, target) :
       self.builder = builder
       self.target = target
       self.point = QPoint (0, 0)
       self.button = Qt.NoButton
       self.down = False
       target.mousePressEvent = self.mousePressEvent
       target.mouseMoveEvent = self.mouseMoveEvent
       target.mouseReleaseEvent = self.mouseReleaseEvent

   def mousePressEvent (self, event):
       self.down = True
       self.point = mouse_event_pos (event)
       self.button = event.button ()

       if event.button == Qt.LeftButton :
          self.builder.showProperties (self.target)

       if self.button == Qt.MiddleButton :
          if event.modifiers () == Qt.ControlModifier :
             self.target.lower ()
          else:
             self.target.raise_ ()

   def mouseMoveEvent (self, event):
       if self.down :
          if self.button == Qt.LeftButton :
             pos = self.target.pos () + mouse_event_pos (event) - self.point
             self.target.move (pos)
          if self.button == Qt.RightButton :
             delta = mouse_event_pos  (event) - self.point
             delta2 = QSize (delta.x(), delta.y())
             s = self.target.size () + delta2
             self.target.resize (s)
             self.point = mouse_event_pos (event)

   def mouseReleaseEvent (self, event):
       self.down = False

# --------------------------------------------------------------------------

# factories = { }

class Builder (QWidget):

   # _properties_ = [ "pen", "brush" ]

   def __init__ (self, win = None) :
       super (Builder, self).__init__ (win)
       self.win = win
       self.factories = { }
       self.setAcceptDrops (True)

   def dragEnterEvent (self, event) :
       mime = event.mimeData ()
       if mime.hasFormat ("application/x-view-widget") :
           event.acceptProposedAction ()
       else :
           event.setAccepted (False)

   def dropEvent (self, event) :
       mime = event.mimeData ()
       if mime.hasFormat ("application/x-view-widget") :
          name = bytearray_to_str (mime.data ("application/x-view-widget"))
          self.addComponent (name, event.pos(), None)

   def addComponent (self, name, pos, target) :
       item = self.createComponent (name)
       if item != None :
          Adapter (self, item)
          item.setParent (self)
          if pos != None :
             item.move (pos)
          item.show ()
          self.addTreeNode (item, target)
          self.showProperties (item)

   def createComponent (self, name) :
       if name in self.factories :
          item = self.factories [name] ()
       else :
          item = eval (name + " ()")

       if name == "QLabel" :
          item.setText ("label")

       if name == "QCheckBox" :
          item.setText ("check box")

       if name == "QWidget" :
          item.setStyleSheet("background-color: rgb(255,255,0); margin:5px; border:1px solid rgb(0, 255, 0); ")

       if item.width () < 8 and item.height () < 8 :
          item.resize (8, 8)

       return item

   def addTreeNode (self, data, target = None) :
       if self.win != None and getattr (self.win, "tree") != None :
          text = data.metaObject().className()
          if target == None :
             target = self.win.tree
          node = QTreeWidgetItem (target)
          node.setText (0, text)
          node.obj = data

   def showProperties (self, data) :
       if self.win != None and getattr (self.win, "prop") != None :

          lines = [ ]

          typ = data.metaObject ()
          cnt = typ.propertyCount ()
          for inx in range (cnt) :
              prop = typ.property (inx)
              key = prop.name ()
              value = prop.read (data)
              lines.append ([key, value])

          cnt = typ.methodCount ()
          for inx in range (cnt) :
              method = typ.method (inx)
              if method.methodType () == QMetaMethod.Signal :
                 if use_qt4 :
                    key = method.signature ()
                 else :
                    key = bytearray_to_str (method.methodSignature ())
                 value = "..."
                 lines.append ([key, value])

          prop = self.win.prop
          prop.setRowCount (0)
          cnt = len (lines)
          for inx in range (cnt) :
              name, value = lines[inx]
              prop.showProperty (lines[inx][0], lines[inx][1])

   def addClasses (self, ns, module) :
       page = PalettePage ()
       for item in ns.item_list :
           if isinstance (item, Class) :
              cls = item
              name = cls.item_name
              # page = self.palette.currentWidget ()
              ComponentItem (page, name, "pushbutton")
              self.factories [name] = getattr (module, name)
              print ("COMPONENT", name)
       self.palette.addPage ("Classes", page)

# --------------------------------------------------------------------------

class Palette (QTabWidget):

   def __init__ (self, parent=None) :
       super (Palette, self).__init__ (parent)

   def addPage (self, name, page) :
       self.addTab (page, name)

class PalettePage (QToolBar):

   def __init__ (self, parent=None) :
       super (PalettePage, self).__init__ (parent)

   def addItem (self, name, item) :
       item.setText (name)
       item.setToolTip (name)
       self.addWidget (item)
       # print ("ITEM", name)

class PaletteItem (QToolButton):

   def __init__ (self, parent=None) :
       super (PaletteItem, self).__init__ (parent)

   def setupPixmap (self, drag) :
       pass

   def mousePressEvent (self, event) :

       if event.button() == Qt.LeftButton :
          drag =  QDrag (self)

          mimeData = QMimeData ()
          self.setupMimeData (mimeData)
          drag.setMimeData (mimeData)

          self.setupPixmap (drag)

          dropAction = drag.exec_ (Qt.MoveAction | Qt.CopyAction | Qt.LinkAction)

# --------------------------------------------------------------------------

class ComponentItem (PaletteItem):

   def __init__ (self, page, widget_name, icon_name = None) :
       super (ComponentItem, self).__init__ (page)
       self.name = widget_name
       if icon_name != None :
          self.icon = findIcon (icon_name)
          if self.icon != None :
             # print ("Found icon", icon_name, str (self.icon.actualSize(QSize (32, 32))))
             self.setIcon (self.icon)
       page.addItem (self.name, self)

   def setupMimeData (self, data) :
       data.setData ("application/x-view-widget", bytearray (self.name, "ascii"))

   def setupPixmap (self, drag) :
       size = self.icon.actualSize (QSize (32, 32))
       pixmap = self.icon.pixmap (size);
       drag.setPixmap (pixmap)
       drag.setHotSpot (QPoint (-8, -8))

# --------------------------------------------------------------------------

def initPalette (palette) :
    page = PalettePage ()

    ComponentItem (page, "QPushButton", "pushbutton")
    ComponentItem (page, "QCheckBox", "checkbox")
    ComponentItem (page, "QComboBox", "combobox")
    ComponentItem (page, "QLabel", "label")
    ComponentItem (page, "QLineEdit", "lineedit")
    ComponentItem (page, "QPlainTextEdit", "plaintextedit")
    ComponentItem (page, "QTextEdit", "textedit")
    ComponentItem (page, "QSpinBox", "spinbox")
    ComponentItem (page, "QDoubleSpinBox", "doublespinbox")
    ComponentItem (page, "QTreeWidget", "listview")
    ComponentItem (page, "QListWidget", "listbox")
    ComponentItem (page, "QTableWidget", "table")
    ComponentItem (page, "QTabWidget", "tabwidget")
    ComponentItem (page, "QWidget", "widget")

    palette.addPage ("Components", page)

# --------------------------------------------------------------------------

class BuilderWithPalette (QWidget) :

   def __init__ (self, win) :
       super (BuilderWithPalette, self).__init__ (win)
       self.win = win

       layout = QVBoxLayout ()
       self.setLayout (layout)

       self.palette = Palette ()
       initPalette (self.palette)
       layout.addWidget (self.palette)
       layout.setStretchFactor (self.palette, 0)

       self.builder = Builder (self.win)
       layout.addWidget (self.builder)
       layout.setStretchFactor (self.builder, 1)

       self.builder.palette = self.palette # !?

   def showNavigator (self, tree) :
       for item in self.builder.children () :
           self.showBranch (tree, item)

   def showBranch (self, above, item) :
       text = getattr (item, "Title", "")
       if text == "" :
          text = str (item)
       node = TreeItem (above, text)
       node.obj = item

       if hasattr (item, "__icon__") : # !?
          node.setIcon (0, findIcon (item.__icon__))

       for subitem in item.children () :
           self.showBranch (node, subitem)

# --------------------------------------------------------------------------

if __name__ == '__main__':
   class Tree (QTreeWidget):

       def __init__ (self, win = None) :
           super (Tree, self).__init__ (win)
           self.win = win

           self.header ().hide ()
           self.setAlternatingRowColors (True)

           self.setContextMenuPolicy (Qt.CustomContextMenu)
           self.customContextMenuRequested.connect (self.onContextMenu)

           # self.setDragDropMode (self.DragDrop)
           # self.setDragDropMode (self.InternalMove)

           self.setDragEnabled (True)
           self.viewport().setAcceptDrops (True) # <-- accept draging from another widgets
           self.setDropIndicatorShown (True)
           self.setSelectionMode (QAbstractItemView.SingleSelection)

           self.setAcceptDrops (True)

           self.itemActivated.connect (self.onItemActivated)

       def mimeTypes (self) :
           result = empty_qstringlist ()
           result.append ("application/x-view-widget")
           return result

       def supportedDropActions (self) :
           return Qt.CopyAction | Qt.MoveAction

       def dropMimeData (self, target, index, mime, action) :
           result = False
           if mime.hasFormat ("application/x-view-widget") :
              name = bytearray_to_str (mime.data ("application/x-view-widget"))
              print ("drop", name)
              self.win.builder.builder.addComponent (name, QPoint (0, 0), target)
              # target == None ... top of tree
              result = True
           return result

       # http://sector.ynet.sk/qt4-tutorial/dnd.html

       """
       def dragEnterEvent (self, event) :
           # if event.source () == self :
           #    self.setDragDropMode (QAbstractItemView.InternalMove)
           #    QTreeWidget.dragEnterEvent (self, event)
           # else:
              # self.setDragDropMode (QAbstractItemView.DragDrop)
              mime = event.mimeData ()
              if mime.hasFormat ("application/x-view-widget") :
                 print ("accept")
                 event.setAccepted (True)

       def dropEvent (self, event) :
           # if event.source () == self :
           #    self.setDragDropMode (QAbstractItemView.InternalMove)
           #    QTreeWidget.dropEvent(self, event)
           # else :
              # self.setDragDropMode (QAbstractItemView.DragDrop)
              mime = event.mimeData ()
              if mime.hasFormat ("application/x-view-widget") :
                 name = str (mime.data ("application/x-view-widget"))
                 target = self.currentItem ()
                 print ("drop")
                 self.win.builder.builder.addComponent (target, name)
                 event.accept ()
        """
           # stackoverflow.com/questions/25222906/how-to-stop-qtreewidget-from-creating-the-item-duplicates-on-drag-and-drop

       def onItemActivated (self, node, column) :
           if node != None and hasattr (node, "obj") :
              self.win.showProperties (node.obj)

       def onContextMenu (self, pos) :
           node = self.itemAt (pos)
           menu = QMenu (self)

           if hasattr (node, "obj") :
              act = menu.addAction ("context menu")

           menu.exec_ (self.mapToGlobal (QPoint (pos)))

# --------------------------------------------------------------------------

if __name__ == '__main__':
   class Property (QTableWidget) :

      def __init__ (self, win = None) :
          super (Property, self).__init__ (win)
          self.win = win
          self.setColumnCount (2)
          self.setRowCount (0)
          self.setHorizontalHeaderLabels (["Name", "Value"])
          self.horizontalHeader().setStretchLastSection (True)
          self.verticalHeader().setVisible (False)

      def showProperty (self, name, value) :
          inx = self.rowCount ()
          self.setRowCount (inx+1)

          item = QTableWidgetItem ()
          item.setText (name)
          item.setFlags (item.flags () & ~ Qt.ItemIsEditable)
          self.setItem (inx, 0, item)

          item = QTableWidgetItem ()
          item.setText (str (value))
          self.setItem (inx, 1, item)

          return item

# --------------------------------------------------------------------------

if __name__ == '__main__':
   class SimpleWindow (QMainWindow) :

      def __init__ (self, parent = None) :
          super (SimpleWindow, self).__init__ (parent)

          self.tree = Tree (self)
          self.builder = BuilderWithPalette (self)
          self.prop = Property (self)
          self.info = QTextEdit ()

          splitter = QSplitter ()
          splitter.setOrientation (Qt.Horizontal)
          splitter.addWidget (self.tree)
          splitter.addWidget (self.builder)
          splitter.addWidget (self.prop)
          splitter.setStretchFactor (0, 0)
          splitter.setStretchFactor (1, 1)
          splitter.setStretchFactor (2, 0)

          vsplitter = QSplitter ()
          vsplitter.setOrientation (Qt.Vertical)
          vsplitter.addWidget (splitter)
          vsplitter.addWidget (self.info)
          vsplitter.setStretchFactor (0, 1)
          vsplitter.setStretchFactor (1, 0)

          self.setCentralWidget (vsplitter)

          fileMenu = self.menuBar().addMenu ("&File")

          act = QAction ("&Quit", self)
          act.setShortcut ("Ctrl+Q")
          act.setStatusTip ("Quit program")
          act.triggered.connect (self.close)
          fileMenu.addAction (act)

          self.addToolBar ("main toolbar").addAction (act)
          self.statusBar()

      def showProperties (self, data) :
          self.builder.builder.showProperties (data)

# --------------------------------------------------------------------------

if __name__ == '__main__' :

   def bytearray_to_str (b) :
       return str (b, "ascii")

   def findIcon (name):
       return  QIcon.fromTheme (name)

   use_qt4 = QT_VERSION < 0x050000

   if 0 :
      app = QApplication (sys.argv)
      win = BuilderWithPalette (None)
      win.show ()
      app.exec_ ()
   else :
      app = QApplication (sys.argv)
      win = SimpleWindow ()
      win.show ()
      app.exec_ ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
