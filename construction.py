#!/usr/bin/env python

from __future__ import print_function

import sys

if __name__ == '__main__' :
   if 1 :
      from PyQt5 import QtCore
      from PyQt5.QtCore import *
      from PyQt5.QtGui import *
      from PyQt5.QtWidgets import *
      from PyQt5.Qt3DCore import *
      from PyQt5.Qt3DExtras import *
      use_pyside = False
   if 0 :
      from PyQt6 import QtCore
      from PyQt6.QtCore import *
      from PyQt6.QtGui import *
      from PyQt6.QtWidgets import *
      from PyQt6.Qt3DCore import *
      from PyQt6.Qt3DExtras import *
      use_pyside = False
   if 0 :
      from PySide2 import QtCore
      from PySide2.QtCore import *
      from PySide2.QtGui import *
      from PySide2.QtWidgets import *
      from PySide2.Qt3DCore import *
      from PySide2.Qt3DExtras import *
      use_pyside = True
   if 0 :
      from PySide6 import QtCore
      from PySide6.QtCore import *
      from PySide6.QtGui import *
      from PySide6.QtWidgets import *
      from PySide6.Qt3DCore import *
      from PySide6.Qt3DExtras import *
      use_pyside = True

else :
   from util import import_qt_modules
   import_qt_modules (globals (), ["Qt3DCore", "Qt3DExtras"])

   from OpenGL import GL
   from PyQt5 import QtCore # !?

   from util import findIcon, bytearray_to_str, use_pyside2, use_pyside6
   from tree import TreeItem
   from area import Palette, PalettePage, ComponentItem

   use_pyside = use_pyside2 or use_pyside6

# --------------------------------------------------------------------------

class OrbitTransformController (QObject):
    def __init__ (self, parent) :
        super(OrbitTransformController, self).__init__(parent)
        self.m_target = QTransform ()
        self.m_matrix = QMatrix4x4 ()
        self.m_radius = 1.0
        self.m_angle = 0

    def getTarget (self):
        return self.m_target

    def setTarget (self, target) :
        if self.m_target != target:
            self.m_target = target
            self.targetChanged.emit()

    def getRadius (self) :
        return self.m_radius

    def setRadius (self, radius) :
        if not QtCore.qFuzzyCompare (self.m_radius, radius):
            self.m_radius = radius
            self.updateMatrix ()
            self.radiusChanged.emit ()

    def getAngle (self) :
        return self.m_angle

    def setAngle (self, angle) :
        if not QtCore.qFuzzyCompare (angle, self.m_angle):
            self.m_angle = angle
            self.updateMatrix ()
            self.angleChanged.emit ()

    def updateMatrix (self):
        self.m_matrix.setToIdentity ()
        self.m_matrix.rotate (self.m_angle, QVector3D(0, 1, 0))
        self.m_matrix.translate (self.m_radius, 0, 0)
        self.m_target.setMatrix (self.m_matrix)

    if use_pyside :
       pyqtSignal = Signal
       pyqtProperty = Property

    # QSignal
    targetChanged = pyqtSignal()
    radiusChanged = pyqtSignal()
    angleChanged = pyqtSignal()

    # Qt properties
    target = pyqtProperty(QTransform, fget=getTarget, fset=setTarget)
    radius = pyqtProperty(float, fget=getRadius, fset=setRadius)
    angle = pyqtProperty(float, fget=getAngle, fset=setAngle)

# --------------------------------------------------------------------------

class Construction (Qt3DWindow):

   def createScene (self) :
       "from qt3d-simple-example.py"

       # root
       rootEntity = QEntity()

       # torus
       torusEntity = QEntity (rootEntity)
       torusMesh = QTorusMesh ()
       torusMesh.setRadius (5)
       torusMesh.setMinorRadius (1)
       torusMesh.setRings (100)
       torusMesh.setSlices (20)

       torusTransform = QTransform ()
       torusTransform.setScale3D (QVector3D (1.5, 1.0, 0.5))
       torusTransform.setRotation (QQuaternion.fromAxisAndAngle (QVector3D(1, 0, 0), 45))

       torusMaterial = QPhongMaterial (rootEntity)
       torusMaterial.setAmbient (QColor ("blue"))

       torusEntity.addComponent (torusMesh)
       torusEntity.addComponent (torusTransform)
       torusEntity.addComponent (torusMaterial)

       # sphere
       sphereEntity = QEntity (rootEntity)
       sphereMesh = QSphereMesh ()
       sphereMesh.setRadius (3)

       sphereTransform = QTransform ()
       controller = OrbitTransformController (sphereTransform)
       controller.setTarget (sphereTransform)
       controller.setRadius (20)

       sphereRotateTransformAnimation = QPropertyAnimation (sphereTransform)
       sphereRotateTransformAnimation.setTargetObject (controller)
       sphereRotateTransformAnimation.setPropertyName (b'angle')
       sphereRotateTransformAnimation.setStartValue (0)
       sphereRotateTransformAnimation.setEndValue (360)
       sphereRotateTransformAnimation.setDuration (10000)
       sphereRotateTransformAnimation.setLoopCount (-1)
       sphereRotateTransformAnimation.start()

       sphereMaterial = QPhongMaterial (rootEntity)
       sphereMaterial.setAmbient (QColor ("yellow"))

       sphereEntity.addComponent (sphereMesh)
       sphereEntity.addComponent (sphereTransform)
       sphereEntity.addComponent (sphereMaterial)

       # cone
       coneEntity = QEntity (rootEntity)
       coneMesh = QConeMesh ()
       coneMesh.setLength (4)
       coneMesh.setBottomRadius (3)

       coneTransform = QTransform ()
       coneTransform.setTranslation (QVector3D (-12.0, 0.0, 0.0))

       coneMaterial = QPhongMaterial (rootEntity)
       coneMaterial.setAmbient (QColor ("red"))

       coneEntity.addComponent (coneMesh)
       coneEntity.addComponent (coneTransform)
       coneEntity.addComponent (coneMaterial)

       # cube
       cubeEntity = QEntity (rootEntity)
       cubeMesh = QCuboidMesh ()
       cubeMesh.setXExtent (3)
       cubeMesh.setYExtent (3)
       cubeMesh.setZExtent (3)

       cubeTransform = QTransform ()
       cubeTransform.setTranslation (QVector3D (12.0, 0.0, 0.0))

       cubeMaterial = QPhongMaterial (rootEntity)
       cubeMaterial.setAmbient (QColor ("lime"))

       cubeEntity.addComponent (cubeMesh)
       cubeEntity.addComponent (cubeTransform)
       cubeEntity.addComponent (cubeMaterial)

       # camera
       camera = self.camera()
       camera.lens().setPerspectiveProjection (45.0, 16.0/9.0, 0.1, 1000)
       camera.setPosition (QVector3D(0, 0, 40))
       camera.setViewCenter (QVector3D(0, 0, 0))

       # for camera control
       camController = QOrbitCameraController (rootEntity)
       camController.setLinearSpeed( 50.0 )
       camController.setLookSpeed( 180.0 )
       camController.setCamera(camera)

       self.setRootEntity (rootEntity)
       self.scene = rootEntity # keep scene

   def __init__ (self, win = None) :
       super (Construction, self).__init__ ()
       # super (Construction, self).__init__ (win)
       # self.win = win
       # self.factories = { }
       # self.setAcceptDrops (True)
       self.createScene ()

   def dragEnterEvent (self, event) :
       mime = event.mimeData ()
       if mime.hasFormat ("application/x-view-widget") :
           event.acceptProposedAction ()
       else :
           event.setAccepted (False)

   def dropEvent (self, event) :
       mime = event.mimeData ()
       if mime.hasFormat ("application/x-view-widget") :
          name = bytearray_to_str (mime.data ("application/x-view-widget"))
          self.addComponent (name, event.pos(), None)

   def addComponent (self, name, pos, target) :
       item = self.createComponent (name)
       if item != None :
          Adapter (self, item)
          item.setParent (self)
          if pos != None :
             item.move (pos)
          item.show ()
          self.addTreeNode (item, target)
          self.showProperties (item)

   def createComponent (self, name) :
       if name in self.factories :
          item = self.factories [name] ()
       else :
          item = eval (name + " ()")

       if name == "QLabel" :
          item.setText ("label")

       if name == "QCheckBox" :
          item.setText ("check box")

       if name == "QWidget" :
          item.setStyleSheet("background-color: rgb(255,255,0); margin:5px; border:1px solid rgb(0, 255, 0); ")

       if item.width () < 8 and item.height () < 8 :
          item.resize (8, 8)

       return item

   def addTreeNode (self, data, target = None) :
       if self.win != None and getattr (self.win, "tree") != None :
          text = data.metaObject().className()
          if target == None :
             target = self.win.tree
          node = QTreeWidgetItem (target)
          node.setText (0, text)
          node.obj = data

   def showProperties (self, data) :
       if self.win != None and getattr (self.win, "prop") != None :

          lines = [ ]

          typ = data.metaObject ()
          cnt = typ.propertyCount ()
          for inx in range (cnt) :
              prop = typ.property (inx)
              key = prop.name ()
              value = prop.read (data)
              lines.append ([key, value])

          cnt = typ.methodCount ()
          for inx in range (cnt) :
              method = typ.method (inx)
              if method.methodType () == QMetaMethod.Signal :
                 if use_qt4 :
                    key = method.signature ()
                 else :
                    key = bytearray_to_str (method.methodSignature ())
                 value = "..."
                 lines.append ([key, value])

          prop = self.win.prop
          prop.setRowCount (0)
          cnt = len (lines)
          for inx in range (cnt) :
              name, value = lines[inx]
              prop.showProperty (lines[inx][0], lines[inx][1])

   def addClasses (self, ns, module) :
       page = PalettePage ()
       for item in ns.item_list :
           if isinstance (item, Class) :
              cls = item
              name = cls.item_name
              # page = self.palette.currentWidget ()
              ComponentItem (page, name, "pushbutton")
              self.factories [name] = getattr (module, name)
              print ("COMPONENT", name)
       self.palette.addPage ("Classes", page)

# --------------------------------------------------------------------------

class Palette (QTabWidget):

   def __init__ (self, parent=None) :
       super (Palette, self).__init__ (parent)

   def addPage (self, name, page) :
       self.addTab (page, name)

class PalettePage (QToolBar):

   def __init__ (self, parent=None) :
       super (PalettePage, self).__init__ (parent)

   def addItem (self, name, item) :
       item.setText (name)
       item.setToolTip (name)
       self.addWidget (item)
       # print ("ITEM", name)

class PaletteItem (QToolButton):

   def __init__ (self, parent=None) :
       super (PaletteItem, self).__init__ (parent)

   def setupPixmap (self, drag) :
       pass

   def mousePressEvent (self, event) :

       if event.button() == Qt.LeftButton :
          drag =  QDrag (self)

          mimeData = QMimeData ()
          self.setupMimeData (mimeData)
          drag.setMimeData (mimeData)

          self.setupPixmap (drag)

          dropAction = drag.exec_ (Qt.MoveAction | Qt.CopyAction | Qt.LinkAction)

# --------------------------------------------------------------------------

class ComponentItem (PaletteItem):

   def __init__ (self, page, widget_name, icon_name = None) :
       super (ComponentItem, self).__init__ (page)
       self.name = widget_name
       if icon_name != None :
          self.icon = findIcon (icon_name)
          if self.icon != None :
             # print ("Found icon", icon_name, str (self.icon.actualSize(QSize (32, 32))))
             self.setIcon (self.icon)
       page.addItem (self.name, self)

   def setupMimeData (self, data) :
       data.setData ("application/x-view-widget", bytearray (self.name, "ascii"))

   def setupPixmap (self, drag) :
       size = self.icon.actualSize (QSize (32, 32))
       pixmap = self.icon.pixmap (size);
       drag.setPixmap (pixmap)
       drag.setHotSpot (QPoint (-8, -8))

# --------------------------------------------------------------------------

def initPalette (palette) :
    page = PalettePage ()

    ComponentItem (page, "QCube", "cube")
    ComponentItem (page, "QCone", "cone")
    ComponentItem (page, "QSphere", "sphere")
    ComponentItem (page, "QCylinder", "cylinder")

    palette.addPage ("3D", page)

# --------------------------------------------------------------------------

class ConstructionWithPalette (QWidget) :

   def __init__ (self, win) :
       super (ConstructionWithPalette, self).__init__ (win)
       self.win = win

       layout = QVBoxLayout ()
       self.setLayout (layout)

       self.palette = Palette ()
       initPalette (self.palette)
       layout.addWidget (self.palette)
       layout.setStretchFactor (self.palette, 0)

       self.builder = Construction (self.win)
       self.container = QWidget.createWindowContainer (self.builder);
       layout.addWidget (self.container)
       layout.setStretchFactor (self.container, 1)

       self.builder.palette = self.palette # !?

   def showNavigator (self, tree) :
       for item in self.builder.children () :
           self.showBranch (tree, item)

   def showBranch (self, above, item) :
       text = getattr (item, "Title", "")
       if text == "" :
          text = str (item)
       node = TreeItem (above, text)
       node.obj = item

       if hasattr (item, "__icon__") : # !?
          node.setIcon (0, findIcon (item.__icon__))

       for subitem in item.children () :
           self.showBranch (node, subitem)

# --------------------------------------------------------------------------

if __name__ == '__main__':
   class Tree (QTreeWidget):

       def __init__ (self, win = None) :
           super (Tree, self).__init__ (win)
           self.win = win

           self.header ().hide ()
           self.setAlternatingRowColors (True)

           self.setContextMenuPolicy (Qt.CustomContextMenu)
           self.customContextMenuRequested.connect (self.onContextMenu)

           self.setDragEnabled (True)
           self.viewport().setAcceptDrops (True) # <-- accept draging from another widgets
           self.setDropIndicatorShown (True)
           self.setSelectionMode (QAbstractItemView.SingleSelection)

           self.setAcceptDrops (True)

           self.itemActivated.connect (self.onItemActivated)

       def mimeTypes (self) :
           result = empty_qstringlist ()
           result.append ("application/x-view-widget")
           return result

       def supportedDropActions (self) :
           return Qt.CopyAction | Qt.MoveAction

       def dropMimeData (self, target, index, mime, action) :
           result = False
           if mime.hasFormat ("application/x-view-widget") :
              name = bytearray_to_str (mime.data ("application/x-view-widget"))
              print ("drop", name)
              self.win.builder.builder.addComponent (name, QPoint (0, 0), target)
              # target == None ... top of tree
              result = True
           return result

       def onItemActivated (self, node, column) :
           if node != None and hasattr (node, "obj") :
              self.win.showProperties (node.obj)

       def onContextMenu (self, pos) :
           node = self.itemAt (pos)
           menu = QMenu (self)

           if hasattr (node, "obj") :
              act = menu.addAction ("context menu")

           menu.exec_ (self.mapToGlobal (QPoint (pos)))

# --------------------------------------------------------------------------

if __name__ == '__main__':
   class Property (QTableWidget) :

      def __init__ (self, win = None) :
          super (Property, self).__init__ (win)
          self.win = win
          self.setColumnCount (2)
          self.setRowCount (0)
          self.setHorizontalHeaderLabels (["Name", "Value"])
          self.horizontalHeader().setStretchLastSection (True)
          self.verticalHeader().setVisible (False)

      def showProperty (self, name, value) :
          inx = self.rowCount ()
          self.setRowCount (inx+1)

          item = QTableWidgetItem ()
          item.setText (name)
          item.setFlags (item.flags () & ~ Qt.ItemIsEditable)
          self.setItem (inx, 0, item)

          item = QTableWidgetItem ()
          item.setText (str (value))
          self.setItem (inx, 1, item)

          return item

# --------------------------------------------------------------------------

if __name__ == '__main__':
   class SimpleWindow (QMainWindow) :

      def __init__ (self, parent = None) :
          super (SimpleWindow, self).__init__ (parent)

          self.tree = Tree (self)
          self.prop = Property (self)

          self.builder = ConstructionWithPalette (self)

          splitter = QSplitter ()
          splitter.setOrientation (Qt.Horizontal)
          splitter.addWidget (self.tree)
          splitter.addWidget (self.builder)
          splitter.addWidget (self.prop)
          splitter.setStretchFactor (0, 0)
          splitter.setStretchFactor (1, 1)
          splitter.setStretchFactor (2, 0)

          # vsplitter = QSplitter ()
          # vsplitter.setOrientation (Qt.Vertical)
          # vsplitter.addWidget (splitter)
          # vsplitter.addWidget (self.info)
          # vsplitter.setStretchFactor (0, 1)
          # vsplitter.setStretchFactor (1, 0)

          self.setCentralWidget (splitter)

          fileMenu = self.menuBar().addMenu ("&File")

          act = QAction ("&Quit", self)
          act.setShortcut ("Ctrl+Q")
          act.setStatusTip ("Quit program")
          act.triggered.connect (self.close)
          fileMenu.addAction (act)

          self.addToolBar ("main toolbar").addAction (act)
          self.statusBar()

      def showProperties (self, data) :
          self.builder.builder.showProperties (data)

# --------------------------------------------------------------------------

if __name__ == '__main__' :

   def bytearray_to_str (b) :
       return str (b, "ascii")

   def findIcon (name):
       return  QIcon.fromTheme (name)

   use_py2_qt4 = sys.version_info < (3,) and QT_VERSION < 0x050000

   if 0 :
      app = QApplication (sys.argv)
      win = Construction (None)
      # win = ConstructionWithPalette (None)
      win.show ()
      app.exec_ ()
   else :
      app = QApplication (sys.argv)
      win = SimpleWindow ()
      win.show ()
      app.exec_ ()


# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
