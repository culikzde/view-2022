
# output.py

from __future__ import print_function

import sys, time
from io import StringIO, BytesIO

from input import quoteString

sectionGenerator = None

# jump marks
sourceMark = 1
pythonMark = 2
codeMark = 3
headerMark = 4

parserMark = 2
productMark = 3

# --------------------------------------------------------------------------

use_python3 = ( sys.version_info >= (3,) ) # do not import util

class Output (object) :

   def __init__ (self) :
       self.redirect = None
       self.initialize ()

   def initialize (self) :
       self.indentation = 0
       self.addSpace = False
       self.extraSpace = False
       self.startLine = True
       self.addEmptyLine = False
       self.noEmptyLine = False
       self.outputFile = sys.stdout
       self.sections = None
       self.console = False

       self.pascal = False
       self.csharp = False
       self.java = False
       self.js = False
       self.rust = False
       self.set_consts ()

   def openFile (self, fileName, with_sections = False, with_simple = False, with_mark = 0) :
       self.open (fileName, with_sections, with_mark)

   def open (self, fileName, with_sections = False, with_simple = False, with_mark = 0) :

       # if with_mark != 0 :
       #    if not with_sections and not with_simple :
       #       with_sections = True

       # NO self.initialize ()

       if (with_sections or with_simple) and sectionGenerator != None :
          self.sections = sectionGenerator.createSections (fileName, with_simple)
          self.sections.open ()
       else :
          if fileName == "" or  fileName == "-":
             self.console = True
             self.outputFile = sys.stdout
          else :
             self.outputFile = open (fileName, "w")

       self.setJumpMark (with_mark)

   def close (self) :
       if self.sections != None :
          self.sections.close ()
       else :
          if not self.console :
             self.outputFile.close ()
       self.outputFile = sys.stdout

   def write (self, txt) :
       if self.sections != None :
          self.sections.write (txt)
       else :
          self.outputFile.write (txt)

   # -----------------------------------------------------------------------

   def openString (self) :
       # NO self.initialize ()
       if use_python3 :
          self.outputFile = StringIO ()
       else :
          self.outputFile = BytesIO ()

   def closeString (self) :
       result = self.outputFile.getvalue ()
       self.outputFile.close ()
       self.outputFile = sys.stdout
       return result

   # -----------------------------------------------------------------------

   # put

   def indent (self) :
       if self.redirect != None :
          self.redirect.indent ()
       self.indentation = self.indentation + 3

   def unindent (self) :
       if self.redirect != None :
          self.redirect.unindent ()
       self.indentation = self.indentation - 3

   def incIndent (self) :
       self.indent ()

   def decIndent (self) :
       self.unindent ()

   def beforePut (self) :
       if self.startLine :
          if self.addEmptyLine and not self.noEmptyLine :
             if self.redirect == None :
                self.write ("\n")
          self.write (" " * self.indentation)
          self.startLine = False
          self.addEmptyLine = False
          self.noEmptyLine = False

   def put (self, txt) :
       if txt != "" :
          self.beforePut ()
          if self.redirect != None :
             self.redirect.put (txt)
          else :
             self.write (txt)

   def putEol (self) :
       self.startLine = True
       self.addSpace = False
       self.extraSpace = False
       if self.redirect != None :
          self.redirect.putEol ()
       else :
          self.write ("\n")

   def putCondEol (self) :
       if not self.startLine :
          if self.redirect == None :
             self.putEol ()

   def putLn (self, txt = "") :
       if txt != "":
          self.put (txt)
       self.putEol ()

   # -----------------------------------------------------------------------

   # send

   def isLetterOrDigit (self, c) :
       return c >= 'A' and c <= 'Z' or c >= 'a' and c <= 'z' or c >= '0' and c <= '9' or c == '_'

   def beforeSend (self, txt) :
       if self.startLine :
          self.addSpace = False
          self.extraSpace = False

       c = txt [0]
       if c == ',' or c == ';' or c == ')' or c == ']' :
          self.extraSpace = False
       if not self.isLetterOrDigit (c) :
          self.addSpace = False

       if self.addSpace or self.extraSpace :
          self.put (" ")
          # txt = " " + txt

   def afterSend (self, txt) :
       c = txt [-1]
       self.addSpace = self.isLetterOrDigit (c)
       self.extraSpace = c != '(' and c != '['

   def send (self, txt) :
       if txt != "" :
          if self.redirect != None :
             self.redirect.send (txt)
          else :
             self.beforeSend (txt)
             self.put (txt)
             self.afterSend (txt)

   def sendChr (self, txt) :
       self.send (quoteString (txt, "'"))

   def sendStr (self, txt) :
       self.send (quoteString (txt))

   def no_space (self) :
       if self.redirect != None :
          self.redirect.no_space ()
       else :
          self.extraSpace = False

   def new_line (self) :
       if self.redirect != None :
          self.redirect.new_line ()
       else :
          self.putCondEol ()

   def empty_line (self) :
       if self.redirect != None :
          self.redirect.empty_line ()
       else :
          self.putCondEol ()
          self.addEmptyLine = True

   def no_empty_line (self) :
       if self.redirect != None :
          self.redirect.no_empty_line ()
       else :
          self.noEmptyLine = True

   def style_indent (self) :
       if self.redirect != None :
          self.redirect.style_indent ()
       else :
          self.putCondEol ()
          self.indent ()

   def style_unindent (self) :
       if self.redirect != None :
          self.redirect.style_unindent ()
       else :
          self.unindent ()
          self.putCondEol ()

   def style_no_space (self) :
       self.no_space ()

   def style_new_line (self) :
       self.new_line ()

   def style_empty_line (self) :
       self.empty_line ()

   def style_no_empty_line (self) :
       self.no_empty_line ()

   # -----------------------------------------------------------------------

   def put_class (self, name, above = "", public = False) :
       self.openSection (name)
       if self.pascal :
          # self.send ("type")
          self.send (name)
          self.send ("=")
          self.send ("class")
          if above != "" :
             self.send ("(")
             self.send (above)
             self.send (")")
       elif self.rust :
          if public :
             self.send ("public")
          self.send ("struct")
          self.send (name)
          if above != "" :
             self.send ("(")
             self.send (above)
             self.send (")")
          self.new_line ()
          self.send ("{")
       else :

          if self.csharp :
             self.send ("public")
          if self.java and public :
             self.send ("public")

          self.send ("class")
          self.send (name)
          if above != "" :
             if self.js or self.java :
                self.send ("extends")
                self.send (above)
             elif self.csharp :
                self.send (":")
                self.send (above)
             else :
                self.send (":")
                self.send ("public")
                self.send (above)
          self.new_line ()
          self.send ("{")

       self.new_line ()
       self.indent ()
       # self.indent ()
       self.no_empty_line ()

   def put_class_end (self) :
       self.no_empty_line ()
       self.new_line ()
       # self.unindent ()
       self.unindent ()
       if self.pascal :
          self.send ("end")
          self.send (";")
       elif self.csharp or self.java or self.rust :
          self.send ("}")
       else :
          self.send ("}")
          self.send (";")
       self.closeSection ()
       self.empty_line ()

   def put_public (self) :
       if not self.js and not self.csharp and not self.java:
          self.unindent ()
          self.send ("public")
          self.indent ()
          if not self.pascal :
             self.no_space ()
             self.send (":")
          self.new_line ()

   "enum"

   def put_enum (self, name) :
       self.openSection (name)
       if self.pascal :
          # self.send ("type")
          self.send (name)
          self.send ("=")
          self.new_line ()
          self.send ("(")
       elif self.js :
          self.send ("//")
          self.send ("enum")
          self.send (name)
          self.new_line ()
       else :
          if self.csharp :
             self.send ("public")
          self.send ("enum")
          self.send (name)
          self.new_line ()
          self.send ("{")
       self.new_line ()
       self.indent ()
       self.remember_enum = 0

   def put_enum_item (self, name) :
       self.simpleSection (name)
       if self.js :
          self.send (name)
          self.send ("=")
          self.send (str (self.remember_enum))
          self.send (";")
          self.new_line ()
       else :
          if self.remember_enum > 0 :
             self.send (",")
             self.new_line ()
          self.send (name)
       self.remember_enum = self.remember_enum + 1

   def put_enum_end (self) :
       self.unindent ()
       if not self.js :
          self.new_line ()
          if self.pascal :
             self.send (")")
          else :
             self.send ("}")
          if not self.csharp or self.java  :
             self.send (";")
       self.closeSection ()
       self.empty_line ()

   "set"

   def put_set (self, name, cnt) :
       self.simpleSection (name)
       if self.csharp :
          self.put ("private byte [] " + name + " = {" )
       elif self.java :
          self.put ("private short [] " + name + " = {" )
       elif self.pascal :
          self.put ("const " + name + " : array [0 .. " + str (cnt-1) + "] of byte = (" )
       else :
          self.put ("const unsigned char " + name + " [" + str (cnt) + "] = {" )
       self.remember_set = 0

   def put_set_item (self, value) :
       if self.remember_set > 0 :
          self.put (", ")
       self.put (str (value))
       self.remember_set = self.remember_set + 1

   def put_set_end (self) :
       if self.pascal :
          self.putLn (");")
       else :
          self.putLn ("};")

   "function"

   def put_func (self, name, type = "", cls = "", constructor = False, inline = False, virtual = False, override = False) :
       self.simpleSection (name)
       type = self.rename_type (type)
       self.remember_type = type
       self.remember_inline = inline
       self.remember_virtual = virtual
       self.remember_override = override

       if inline :
          if not self.csharp and not self.java and not self.pascal :
             self.send ("inline")

       if virtual :
          if not self.java and not self.pascal :
             self.send ("virtual")

       if override :
          if self.csharp :
             self.send ("override")
          elif not self.java and not self.pascal :
             self.send ("virtual")

       if self.pascal :
          if constructor :
             self.send ("constructor")
          elif type == "" :
             self.send ("procedure")
          else :
             self.send ("function")
          if cls != "" :
             self.send (cls)
             self.no_space ()
             self.send (".")
             self.no_space ()
          if constructor :
             self.send ("Create")
          else :
             self.send (name)
       elif self.js :
          if constructor :
             self.send ("constructor")
          else :
             self.send ("function")
             self.send (name)
          self.send ("(")
       elif self.rust :
          self.send ("fn")
          self.send (name)
          self.send ("(")
       else :
          if type == "" :
             type = "void"
          if self.csharp or self.java :
             self.send ("public")
          if not constructor :
             self.send (type)
          if cls != "" and not self.csharp and not self.java :
             self.send (cls)
             self.no_space ()
             self.send ("::")
             self.no_space ()
          self.send (name)
          self.send ("(")
       self.remember_param = 0
       self.remember_init = [ ]
       self.remember_var = [ ]

   def put_param (self, name, type, ptr = False, array = False) :
       type = self.rename_type (type)
       if self.pascal :
          if self.remember_param == 0 :
             self.send ("(")
          if self.remember_param > 0 :
             self.send (",")
          self.send (name)
          self.no_space ()
          self.send (":")
          # if ptr :
          #    self.send ("^")
          self.send (type)
       else :
          if self.remember_param > 0 :
             self.send (",")
          if not self.js and not self.rust :
             self.send (type)
             if ptr :
                if not self.csharp and not self.java :
                   self.send ("*")
          self.send (name)
          if not self.js :
             if array :
                self.send ("[ ]")
          if self.rust :
             self.send (":")
             self.send (type)
       self.remember_param = self.remember_param + 1

   def put_init (self, name, value) :
       self.remember_init.append ([name, value])

   def put_var (self, type, name) :
       type = self.rename_type (type)
       self.remember_var.append ([type, name])

   def put_func_cont (self) :
       "internal fuction"
       if self.pascal :
          if self.remember_param > 0 :
             self.send (")")
          if self.remember_type != "" :
             self.send (":")
             self.send (self.remember_type)
          self.send (";")
          if self.remember_inline :
             self.send ("inline")
             self.send (";")
          if self.remember_virtual :
             self.send ("virtual")
             self.send (";")
          if self.remember_override :
             self.send ("override")
             self.send (";")
       else :
          self.send (")")

   def put_func_decl (self) :
       "only function declaration/header"
       self.put_func_cont ()
       # if self.pascal :
       #       self.send ("forward")
       if not self.pascal :
          self.send (";")
       self.new_line ()

   def put_func_body (self) :
       self.put_func_cont ()
       if not self.js and not self.pascal and not self.csharp and not self.java :
          cnt = len (self.remember_init)
          if cnt > 0 :
             self.send (":")
             self.new_line ()
             inx = 0
             for init in self.remember_init :
                 if inx > 0 :
                    self.send (",")
                    self.new_line ()
                 self.send (init [0])
                 self.send ("(")
                 self.send (init [1])
                 self.send (")")
                 inx = inx + 1

       self.new_line ()

       if self.pascal :
          for var in self.remember_var :
              self.send ("var")
              self.send (var [1])
              self.send (":")
              self.send (var [0])
              self.send (";")
              self.new_line ()

       self.put_begin ()

       if self.js or self.pascal or self.csharp or self.java :
          for init in self.remember_init :
              self.put_assign (init [0], init [1])
              self.new_line ()

   def put_member (self, type, name) :
       type = self.rename_type (type)

       if self.pascal :
          self.send (name)
          self.send (":")
          self.send (type)
          self.send (";")

       elif self.js or self.rust :
          if self.js :
             self.send ("var")
          else :
             self.send ("let")
          self.send (name)
          self.send (";")
          self.new_line ()

       else :
          if (self.csharp or self.java) :
             self.send ("public")
          self.send (type)
          self.send (name)
          self.send (";")
          self.new_line ()

   def put_local (self, type, name, value = None, cls_member = False) :
       type = self.rename_type (type)

       if self.pascal :
          if value != None :
             self.send (name)
             self.send (":=")
             self.send (value)
             self.send (";")
             self.new_line ()

       elif self.js or self.rust :
          if self.js :
             self.send ("var")
          else :
             self.send ("let")
          self.send (name)
          if value != None :
             self.send ("=")
             self.send (value)
          self.send (";")
          self.new_line ()

       else :
          self.send (type)
          self.send (name)
          if value != None :
             self.send ("=")
             self.send (value)
          self.send (";")
          self.new_line ()

   def put_func_end (self) :
       self.put_end (semicolon = True)
       self.empty_line ()

   def put_proc (self, name, params = [], type = "", constructor = False) :
       if not self.csharp and not self.java :
          self.put_func (name, type, constructor = constructor)
          for param in params :
              self.put_param (param[0], param[1])
          self.put_func_decl ()

   "statements"

   def put_begin (self) :
       if self.pascal :
          self.send ("begin")
       else :
          self.send ("{")
       self.new_line ()
       self.indent ()
       self.no_empty_line ()

   def put_end (self, semicolon = False) :
       self.no_empty_line ()
       self.new_line ()
       self.unindent ()
       if self.pascal :
          self.send ("end")
          if semicolon :
             self.send (";")
       else :
          self.send ("}")
       self.new_line ()

   def put_if (self) :
       self.send ("if")
       if not self.pascal :
          self.send ("(")

   def put_then (self) :
       if self.pascal :
          self.send ("then")
       else :
          self.send (")")
       self.new_line ()

   def put_if_else (self) :
       self.new_line ()
       self.send ("else")
       self.new_line ()

   def put_while (self) :
       self.send ("while")
       if not self.pascal :
          self.send ("(")

   def put_do (self) :
       if self.pascal :
          self.send ("do")
       else :
          self.send (")")
       self.new_line ()

   def put_return (self, value) :
       if self.pascal :
          if value != "result" :
             self.send ("result")
             self.send (":=")
             self.send (value)
             self.send (";")
       else :
          self.send ("return")
          self.send (value)
          self.send (";")
       self.new_line ()

   def put_assign (self, var, value) :
       if self.pascal :
          self.send (var)
          self.send (":=")
          self.send (value)
          self.send (";")
       else :
          self.send (var)
          self.send ("=")
          self.send (value)
          self.send (";")
       self.new_line ()

   def put_inc (self, var) :
       if self.pascal :
          self.send ("inc")
          self.send ("(")
          self.put (var)
          self.send (")")
          self.send (";")
       else :
          self.put (var)
          self.send ("++")
          self.send (";")
       self.new_line ()

   def put_monitor (self, func, param = "") :
       self.put_if ()
       self.send ("monitor")
       self.put_then ()
       self.indent ()
       if param == "" :
          self.putLn ("monitor" + self.arrow + func + self.call + ";")
       else :
          self.putLn ("monitor" + self.arrow + func + "(" + param + ");")
       self.unindent ()

   "type"

   def pointer (self, type) :
       if self.pascal or self.csharp or self.java  :
          # return "^ " + type # classic pointer
          return type # object pointer
       else :
          return type + " *"

   def pointer_decl (self, type) :
       if self.pascal or self.csharp or self.java  :
          return type + " "
       else :
          return type + " * "

   def vector (self, type) :
       if self.pascal :
          return "array of " + type
       elif self.csharp :
          return "List < " + type + " >"
       elif self.java :
          return "ArrayList < " + type + " >"
       else :
          return "vector < " + type + " >"

   def put_typedef (self, old_type, new_type) :
       if self.pascal :
          return "type " + new_type + " = " + old_type + ";"
       else :
          return "typedef " + old_type + " " + new_type + ";"

   def rename_type (self, name) :
       if name == "bool" :
          if self.java or self.pascal :
             name = "boolean"
       if name == "int" :
          if self.pascal :
             name = "integer"
       if name == "string" :
          if self.java :
             name = "String"
       if name == "ptr" : # !?
             name = "void"
       return name

   "expression"

   def quote (self, value) :
       if self.pascal :
          return quoteString (value, "'")
       else :
          return quoteString (value)

   def quote_char (self, value) :
       return quoteString (value, "'")

   def vector_item (self, index) :
       if self.java :
          return ".get (" + index + ")"
       else :
          return "[" + index + "]"

   def vector_size (self) :
       if self.csharp :
          return ".Count"
       else :
          return ".size ()"

   def put_vector_add (self, param) :
       # only pascal
       self.putLn ("setLength (" + param + ", length (" + param + ") + 1); ")
       self.put (param + " [ high (" + param + ")] := ")

   def string_item (self, name, index) :
       if self.java :
          return name + ".charAt (" + index + ")"
       else :
          return name + "[" + index + "]"

   def string_compare (self, first, second) :
       if self.java :
          return first + ".equals (" + second + ")"
       else :
          return first + self.equal + second

   def string_length (self, expr) :
       if self.csharp :
          return expr + ".Length"
       elif self.java :
          return expr + ".length ()"
       else :
          return expr + ".length ()"

   def new (self, type) :
       if self.pascal :
          return type + ".Create"
       elif self.rust  :
          return type + "::new ()"
       elif self.csharp or self.java  :
          return "new " + type + " ()"
       else :
          return "new " + type

   def is_class (self, expr, type) :
       return expr + self.arrow + "conv_" + type + self.call + self.unequal + self.null
       """
       if self.pascal :
          return expr + " is " + type
       elif self.csharp :
          return expr + " is " + type
       elif self.java :
          return expr + " instanceof " + type
       else :
          return "dynamic_cast < " + type + " * > (" + expr + ")" + self.unequal + self.null
       """

   def type_cast (self, type, expr) :
       return expr + self.arrow + "conv_" + type + self.call
       """
       if self.pascal :
          return expr + " as " + type
       elif self.csharp :
          return expr + " as " + type
       elif self.java :
          return "(" + type + ") " + expr
       else :
          return "dynamic_cast < " + self.pointer (type) + " > (" + expr + ")"
       """

   def set_consts (self) :
       if self.pascal :
          # self.arrow = "^." # Pascal pointer
          self.arrow = "." # Pascal object
          self.assign = " := "
          self.equal = " = "
          self.unequal = " <> "
          self.log_and = " and "
          self.log_or = " or "
          self.log_not = "not " # one space
          self.bit_and = " & "
          self.shl = " shl "
          self.div = " div "
          self.mod = " mod "
          self.this = "self"
          self.null = "nil"
          self.call = ""
          self.push_back = "add"
          self.substr = "substr" # !?
       else :
          if self.js or self.csharp or self.java :
             self.arrow = "."
          else :
             self.arrow = "->"
          self.assign = " = "
          self.equal = " == "
          self.unequal = " != "
          self.log_and = " && "
          self.log_or = " || "
          self.log_not = "!"
          self.bit_and = " & "
          self.shl = " << "
          self.div = " / "
          self.mod = " % "
          self.this = "this"
          if self.csharp or self.java :
             self.null = "null"
          else :
             self.null = "NULL"
          self.call = " ()"
          if self.csharp :
             self.push_back = "Add"
          elif self.java :
             self.push_back = "add"
          else :
             self.push_back = "push_back"
          if self.csharp :
             self.substr = "Substring"
          elif self.java  :
             self.substr = "substring"
          else :
             self.substr = "substr"

   # -----------------------------------------------------------------------

   # sections

   def setSections (self, sections_param) :
       self.sections = sections_param

   def openSection (self, obj) :
       if self.sections != None :
          self.sections.openSection (obj)

   def closeSection (self) :
       if self.sections != None :
          self.sections.closeSection ()

   def simpleSection (self, obj) :
       if self.sections != None :
          self.sections.simpleSection (obj)

   def setInk (self, ink) :
       if self.sections != None :
          self.sections.setInk (ink)

   def setPaper (self, paper) :
       if self.sections != None :
          self.sections.setPaper (paper)

   def addToolTip (self, text, tooltip) :
       if self.sections != None :
          self.sections.addToolTip (text, tooltip)

   def addDefinition (self, text, defn) :
       if self.sections != None :
          self.beforeSend (text)
          self.beforePut ()
          self.sections.addDefinition (text, defn)
          self.afterSend (text)
       else :
          self.send (text)

   def addUsage (self, text, usage) :
       if self.sections != None :
          if text != "" :
             self.beforeSend (text)
          self.beforePut ()
          self.sections.addUsage (text, usage)
          if text != "" :
             self.afterSend (text)
       else :
          self.send (text)

   def setJumpMark (self, mark) :
       if self.sections != None :
          self.sections.jumpMark = mark

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
