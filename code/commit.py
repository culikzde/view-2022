
# commit.py

from __future__ import print_function

from util import import_qt_modules
import_qt_modules (globals ())

use_git = True

if use_git :
   try :
      import git
   except :
      use_git = False
      print ("missing git")

# --------------------------------------------------------------------------

class CommitDialog (QDialog) :

   def __init__ (self, win, local_dir) :
       super (CommitDialog, self).__init__ (win)
       self.local_dir = local_dir

       self.setWindowTitle ("Git Commit & Push")

       topWidget = QWidget (self)
       topLayout = QGridLayout ()
       topWidget.setLayout (topLayout)

       label = QLabel ("Commit Message:")
       topLayout.addWidget (label)

       message = QPlainTextEdit ()
       topLayout.addWidget (message)

       bottomWidget = QWidget (self)
       bottomLayout = QGridLayout ()
       bottomWidget.setLayout (bottomLayout)

       files = QListWidget (self)
       bottomLayout.addWidget (files)

       box = QDialogButtonBox (QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
       bottomLayout.addWidget (box)
       button = box.button (QDialogButtonBox.Ok)
       button.setEnabled (False)

       splitter = QSplitter (self)
       splitter.setOrientation (Qt.Vertical)
       splitter.addWidget (topWidget)
       splitter.addWidget (bottomWidget)
       splitter.setStretchFactor (0, 1);
       splitter.setStretchFactor (1, 3);

       layout = QVBoxLayout (self)
       layout.addWidget (splitter)
       self.setLayout (layout)

       repo = git.Repo (local_dir)

       path_list = [ item.a_path for item in repo.index.diff (None) ]
       path_list = path_list + repo.untracked_files

       # http://stackoverflow.com/questions/33733453/get-changed-files-using-gitpython
       # ( http://stackoverflow.com/questions/47041004/how-do-you-delete-a-file-with-gitpython )
       for path in path_list :
           node = QListWidgetItem ()
           node.setText (path)
           node.setCheckState (Qt.Unchecked)
           if path in repo.untracked_files :
              node.setForeground (QColor ("lime"))
           else :
              node.setForeground (QColor ("blue"))
           files.addItem (node)

       self.repo = repo
       self.message = message
       self.files = files
       self.button = button

       message.textChanged.connect (self.modify)
       files.itemChanged.connect (self.modify)

       box.accepted.connect (self.commit)
       box.rejected.connect (self.reject)

       self.show ()

   def modify (self) :
       msg = self.message.toPlainText ().strip()
       cnt = 0
       for inx in range (self.files.count ()) :
           node = self.files.item (inx)
           if node.checkState () == Qt.Checked :
              cnt = cnt + 1
       self.button.setEnabled (msg != "" and cnt != 0)

   def commit (self) :

       msg = self.message.toPlainText ().strip()
       lst = [ ]
       for inx in range (self.files.count ()) :
           node = self.files.item (inx)
           if node.checkState () == Qt.Checked :
              fileName = node.text ()
              print ("ADD", fileName)
              lst.append (fileName)

       if msg != "" and len (lst) != 0 :
          repo = self.repo
          index = repo.index
          index.add (lst)

          print ("COMMIT", msg)
          index.commit (msg)

          print ("PUSH", repo.remotes.origin)
          repo.remotes.origin.push ()

def git_commit (win, local_dir) :
    if use_git :
       CommitDialog (win, local_dir)
    else :
       print ("Missing git")

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
