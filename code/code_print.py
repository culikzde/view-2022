
# code_print.py

from __future__ import print_function

from code import *
from output import Output

# --------------------------------------------------------------------------

def send_comment (target, decl) :
    any = False
    if hasattr (decl, "comment") :
       comment = decl.comment
       if comment != None and comment != "" :
          lines = comment.split ("\n")
          if len (lines) > 1 :
             target.putEol ()
          for line in lines :
             target.send ("// " + line)
             target.putEol ()
             any = True
    if not any :
       target.putEol ()

# --------------------------------------------------------------------------

def send_declaration (target, decl) :
    if isinstance (decl, Class) :
       target.send ("class")
       target.send (decl.item_name)
       if len (decl.base_classes) != 0 :
          target.send (":")
          first = True
          for item in decl.base_classes :
             if not first :
                target.send (",")
             first = False
             target.send (item.item_name) # !?

    elif isinstance (decl, Enum) :
       target.send ("enum")
       target.send (decl.item_name)
    elif isinstance (decl, Variable) :
       if not decl.is_function :
          if decl.item_type != None :
             target.send (str (decl.item_type))
          target.send (decl.item_name)
       else :
          if decl.result_type != None :
             target.send (str (decl.result_type))
          target.send (decl.item_name)
          target.send ("(")
          any = False
          for param in decl.parameters.item_list :
              if any :
                 target.put (", ")
              any = True
              if param.item_type != None :
                 target.send (str (param.item_type))
              target.send (param.item_name)
          target.send (")")
    else :
       target.send (decl.item_name)

    semicolon = True

    if isinstance (decl, Namespace) or isinstance (decl, Class) or isinstance (decl, Enum) :
       if len (decl.item_list) != 0 :
          send_comment (target, decl)
          semicolon = False
          target.send ("{")
          target.putEol ()
          target.incIndent ()
          for item in decl.item_list :
              send_declaration (target, item)
          target.decIndent ()
          target.send ("}")
          if not isinstance (decl, Namespace) :
             target.send (";")
          if decl.item_name != "" :
             target.send ("// end of " + decl.item_name )
          target.putEol ()

    if semicolon :
       target.send (";")
       send_comment (target, decl)

def print_declaration (decl) :
    target = Output ()
    target.open ("")
    send_declaration (target, decl)
    target.close ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
