Simple C grammar
================

Previous version http://gitlab.fjfi.cvut.cz/culikzde/simple-view

Simple C grammar [tutorial/cecko.g](tutorial/cecko.g)


```c

while_stat :  "while" "(" expr ")" stat   ;

if_stat    :  "if" "(" expr ")" stat ( "else" stat  )?   ;

compound_stat : "{" ( stat )* "}" ;

simple_stat :  expr ";" ;

empty_stat :  ";" ;

stat : while_stat | if_stat | compound_stat | simple_stat | empty_stat ;


simple_expr : identifier | number  | "(" expr ")" ;

mult_expr :  simple_expr ( ("*"|"/") simple_expr )* ;

add_expr :  mult_expr ( ("+"|"-") mult_expr )* ;

expr : add_expr ( "," expr )? ;


program : stat;
```

#### Grammar rule

```
   rule_name : rule_expression ;
```

#### Terminals

Teminals are written in double quotes

```
    "while" "(" ")"
```

#### Nonterminals

Nonterminals (names of another rules) are simple identifiers without quotes

```
   expr
```

#### Alternatives |

Alternatives are denoted by vertical bar

```
   stat : while_stat | if_stat | compound_stat ;
```

```
   ( "*" | "/" )
```

#### Optional section (  )?

```
   ( "else" stat  ) ?
```

#### Repeat zero, one or more times (  )*

```
   ( stat )*
```

#### Repeat one, two or more times (  )+

```
   ( stat )+
```

Simple C Parser
---------------

```python
from lexer import Lexer

class Parser (Lexer) :

   def parse_if_stat (self) :
      self.check ("if")
      self.check ("(")
      self.parse_expr ()
      self.check (")")
      self.parse_stat ()
      if self.tokenText == "else" :
         self.check ("else")
         self.parse_stat ()
```

Grammar which describes grammar
=================================

Grammar [tutorial/grammar.g](tutorial/grammar.g)

Grammar is a list of rules
```
grammar : ( rule )* ;
````

Rule is written as rule identifier followed by colon, description and semicolon

```
rule : identifier ":" description ";" ;
```

Description is a list of alternatives separated by vertical bar
```
description : alternative ( "|" alternative )* ;
```

Alternative is a sequence of simple items.
The sequence could be empty.
```
alternative : ( simple )* ;
```

Simple item is double quoted string (terminal), identifier (nonterminal) or grammar expression in parenthesis
```
simple : string_literal | identifier | ebnf ;
```

Grammar expression consists of left parenthesis, description, right parenthesis and question mark, asterisk, plus or nothing.

```
ebnf : "(" description ")" ( "?" | "*" | "+" |   ) ;
```

C parser stores data
====================

Simple C grammar [tutorial/cecko2.g](tutorial/cecko2.g)

```
if_stat < TIfStat: TStat >  :
   "if"
   "(" cond:expr ")"
   then_code:inner_stat
   (
      <new_line>
      "else"
      else_code:inner_stat
   )?   ;
```

```python
   def parse_if_stat (self) :
      result = TIfStat ()
      self.storeLocation (result)
      self.check ("if")
      self.check ("(")
      result.cond = self.parse_expr ()
      self.check (")")
      result.then_code = self.parse_inner_stat ()
      if self.tokenText == "else" :
         self.check ("else")
         result.else_code = self.parse_inner_stat ()
      return result
```

Class TIfStat stores **if** statement data.

**cond** field stores expression

**then_code** and **else_code** fields store statements

```python
class TIfStat (TStat) :
   def __init__ (self) :
      super (TIfStat, self).__init__ ()
      self.cond = None
      self.then_code = None
      self.else_code = None
```

C product prints data
=====================

```python
class Product (Output) :

   def send_if_stat (self, param) :
      self.send ("if")
      self.send ("(")
      self.send_expr (param.cond)
      self.send (")")
      self.send_inner_stat (param.then_code)
      if param.else_code != None :
         self.style_new_line ()
         self.send ("else")
         self.send_inner_stat (param.else_code)
```

Selection
=========

```
stat < select TStat > :
   while_stat | if_stat | compound_stat | simple_stat | empty_stat ;
```

```python
   def parse_stat (self) :
      if self.tokenText == "while" :
         result = self.parse_while_stat ()
      elif self.tokenText == "if" :
         result = self.parse_if_stat ()
      elif self.tokenText == "{" :
         result = self.parse_compound_stat ()
      elif self.token == self.identifier or self.token == self.number or self.tokenText == "(" :
         result = self.parse_simple_stat ()
      elif self.tokenText == ";" :
         result = self.parse_empty_stat ()
      else :
         self.error ("Unexpected token")
      return result
```

```python
   def send_stat (self, param) :
      if isinstance (param, TWhileStat) :
         self.send_while_stat (param)
      elif isinstance (param, TIfStat) :
         self.send_if_stat (param)
      elif isinstance (param, TCompoundStat) :
         self.send_compound_stat (param)
      elif isinstance (param, TSimpleStat) :
         self.send_simple_stat (param)
      elif isinstance (param, TEmptyStat) :
         self.send_empty_stat (param)
```

```python
   class TSTat :
     pass

   class TWhileStat (TStat) :
      def __init__ (self) :
        super (TWhileStat, self).__init__ ()
        self.cond = None
        self.code = None
```



