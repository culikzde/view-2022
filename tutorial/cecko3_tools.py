
# cecko3_generator.py

from __future__ import print_function

from input import quoteString
from cecko3_parser import *
from cecko3_product import Product

# --------------------------------------------------------------------------

class Writer (Product) :

   # def __init__ (self) :
   #     super (Writer, self).__init__ ()

   def send_expression (self, expr) :
       if isinstance (expr, str) :
          self.send (quoteString (expr))
       elif isinstance (expr, int) :
          self.send (str (expr))
       elif isinstance (expr, float) :
          self.send (str (expr))
       else :
          super (Writer, self).send_expression (expr)

   def send_expr (self, expr) :
       self.send_expression (expr)

   def send_program (self, decl_list) :
       self.process_declarations (self, decl_list)

   # -----------------------------------------------------------------------

   def write_property (self, obj_name, field_name, value) :
       if obj_name != None :
          self.send (obj_name)
          self.style_no_space ()
          self.send (".")
       self.style_no_space ()
       self.send (field_name)
       self.send ("=")
       self.send (value)
       self.putEol ()

   def write_method (self, obj_name, method_name, value_list) :
       if obj_name != None :
          self.send (obj_name)
          self.style_no_space ()
          self.send (".")
       self.style_no_space ()
       self.send (method_name)
       self.send ("(")
       self.send_expr_list (value_list)
       self.send (")")
       self.putEol ()

   # -----------------------------------------------------------------------

   def create_object (self, decl) :
       self.putLn (decl.name + " = " + decl.type + " ()")
       return None

   def assign_property (self, obj, field_name, value) :
       self.write_property (obj, field_name, value)

   def call_method (self, obj, method_name, value_list) :
       self.write_method (obj, method_name, value_list)

   # -----------------------------------------------------------------------

   def process_assign (self, obj, expr) :
       left = expr.left
       right = expr.right
       if left.kind == left.varExp :
          value = None
          if right.kind == right.stringValueExp :
             value = quoteString (right.value)
          if right.kind == right.intValueExp :
             value = right.value
          if right.kind == right.varExp : # !?
             value = right.name
          if value != None :
             print (left.name, "=" , value)
             self.assign_property (obj, left.name, value)

   def process_call (self, obj, expr) :
       left = expr.left
       param_list = expr.param_list
       if left.kind == left.varExp :
          self.call_method (obj, left.name, param_list)

   def process_stat (self, obj, stat) :
       if isinstance (stat, CmmSimpleStat) :
          expr = stat.inner_expr
          if expr.kind == expr.assignExp :
             self.process_assign (obj, expr)
          elif expr.kind == expr.callExp :
             self.process_call (obj, expr)
       elif isinstance (stat, CmmDeclStat) :
          self.process_decl (obj, stat.inner_decl)

   def process_statements (self, obj, stat_list) :
       for stat in stat_list.items :
          self.process_stat (obj, stat)

   def process_decl (self, above_obj, decl) :
       if isinstance (decl, CmmSimpleDecl) :
          print (decl.type, decl.name);
          obj = self.create_object (decl)
          if decl.init_stat != None :
             self.process_statements (obj, decl.init_stat)

   def process_declarations (self, obj, decl_list) :
       for decl in decl_list.items :
          self.process_decl (obj, decl)

# --------------------------------------------------------------------------

class ToFreeCad (Writer) :

   def send_program (self, decl_list) :

       self.putLn ("import FreeCAD")
       self.putLn ("import Part")
       self.putLn ()
       self.putLn ("document = App.ActiveDocument")
       self.putLn ()

       self.process_declarations (None, decl_list)

   def create_object (self, decl) :
       name = decl.name
       type = decl.type
       if type in ["Box", "Sphere", "Cylinder", "Cone", "Torus" ]:
          self.style_empty_line ()
          self.putLn (name + " = document.addObject (" + quoteString ("Part::" + type) + ", " +  quoteString (name) + ")")
          self.write_property (name, "Label", quoteString (name))
          self.putLn ("document.recompute ()")
       return name

   def call_method (self, obj, name, param_list):
       if name in ["PointColor", "LineColor", "ShapeColor" ]:
          # self.write_method (obj, "ViewObject." + name, param_list)
          # self.send ("FreeCADGui.getDocument('Unnamed').getObject (" + quoteString (obj) + ").LineColor = ")
          # self.send ("Gui.activeDocument().getObject (" + quoteString (obj) + ").")
          self.send (obj)
          self.style_no_space ()
          self.send (".")
          self.style_no_space ()
          self.send ("ViewObject")
          self.style_no_space ()
          self.send (".")
          self.style_no_space ()
          self.send (name)
          self.send ("=")
          self.send ("(")
          self.send_expr_list (param_list)
          self.send (")")
          self.putEol ()

# FreeCAD, menu Macro, sub-menu Macros, Macro destination ... select directory

# --------------------------------------------------------------------------

class ToDraw (Writer) :

   def create_object (self, decl):

       param = { "x" : 0, "y" : 0, "width" : 1000, "height" : 1000 }
       if decl.init_stat != None :
          for stat in decl.init_stat.items :
              if isinstance (stat, CmmSimpleStat) :
                 expr = stat.inner_expr
                 if expr.kind == expr.assignExp :
                    left = expr.left
                    right = expr.right
                    if left.kind == left.varExp :
                       field_name = left.name
                       value = None
                       if right.kind == right.stringValueExp :
                          value = right.value
                       elif right.kind == right.intValueExp :
                          value = right.value
                       elif right.kind == right.varExp :
                          value = right.name
                       param [field_name] = value

       name = decl.name
       type = decl.type
       self.putLn (name + " = document.createInstance (" + quoteString ("com.sun.star.drawing." + type) + ")")
       self.putLn ('aPoint = uno.createUnoStruct ("com.sun.star.awt.Point")')
       self.putLn ("aPoint.X = " + param ["x"])
       self.putLn ("aPoint.Y = " + param ["y"])
       self.putLn (name + ".setPosition (aPoint)")
       self.putLn ('aSize = uno.createUnoStruct ("com.sun.star.awt.Size")')
       self.putLn ("aSize.Width = " + param ["width"])
       self.putLn ("aSize.Height = " + param ["height"])
       self.putLn (name + ".setSize (aSize)")
       self.putLn ("drawPage.add (" + name + ")")
       return name

   def call (self, obj, func, *params) :
       self.send (obj)
       self.style_no_space ()
       self.send (".")
       self.style_no_space ()
       self.send (func)
       self.send ("(")
       first = True
       for item in params :
          if not first :
             self.send (",")
          self.send (item)
          first = False
       self.send (")")
       self.putEol ()

   def prop (self, obj, name, value) :
       # if isinstance (value, str) :
       #    value = quoteString (value)
       self.call (obj, "setPropertyValue", quoteString (name), value)

   def assign_property (self, obj, name, value):
       if name == "text" :
          self.call (obj, "setString", value)

       if name in [ "Name", "CornerRadius", "Shadow", "ShadowXDistance", "ShadowYDistance" ] :
          self.prop (obj, name, value)

       if name in [ "FillColor", "LineColor" ] :
          # value = quoteString (value)
          value = "int (" + value + ", 16)"
          self.prop (obj, name, value)

   def send_program (self, decl_list) :

       self.putLn ("import uno")
       self.putLn ("")
       self.putLn ("def func () :")
       self.incIndent ();
       self.putLn ("document = XSCRIPTCONTEXT.getDocument()")
       self.putLn ("drawPage = document.getDrawPages().getByIndex(0)")
       self.putLn ("")

       self.process_declarations (None, decl_list)

       self.putLn ("")
       self.decIndent ();
       self.putLn ("# Functions that can be called from Tools -> Macros -> Run Macro.")
       self.putLn ("g_exportedScripts = func,")


# Libre Office Draw, menu Tools, Options, LibreOffice/Security, Macro Security, Medium

# $HOME/.config/libreoffice/4/user/Scripts/python
# Libre Office Draw, menu Tools, Macros, Run Macros, tree branch My Macros

# http://stackoverflow.com/questions/21413664/how-to-run-python-macros-in-libreoffice
# http://christopher5106.github.io/office/2015/12/06/openoffice-libreoffice-automate-your-office-tasks-with-python-macros.html
# http://stackoverflow.com/questions/21413664/how-to-run-python-macros-in-libreoffice

# https://wiki.openoffice.org/wiki/Documentation/DevGuide/Drawings/Glue_Points_and_Connectors

# --------------------------------------------------------------------------

class ToQt (Writer) :

   def createInstance (self, type) :
       return eval (type + "()")

   def setProperty (self, obj, field, value) :
       # print ("PROPERTY", field, value);
       # setattr (obj, field, value)
       field = "set" + field.capitalize()
       getattr (obj, field) (value)


# --------------------------------------------------------------------------

class Arrays (Writer) :

   def send_program (self, decl_list) :
       for decl in decl_list.items :
          if isinstance (decl, CmmSimpleDecl) :
             if decl.body != None :
                self.process_function (decl)

   def process_function (self, decl) :
       for stat in decl.body.items :
           if isinstance (stat, CmmSimpleStat) :
              expr = stat.inner_expr
              if expr.kind == expr.assignExp :
                 self.process_assign (expr)
              elif expr.kind == expr.callExp :
                 pass # self.process_call (obj, expr)
           elif isinstance (stat, CmmDeclStat) :
              pass # self.process_decl (obj, stat.inner_decl)

   def process_assign (self, expr) :
       left = expr.left
       right = expr.right
       self.send_expr (expr)
       self.putEol ()
       self.process_expr (right)

   def process_expr (self, expr) :
       if expr.kind == expr.varExp :
          name = expr.name
       elif expr.kind == expr.intValueExp or expr.kind == expr.realValueExp :
          value = expr.value
       elif expr.kind == expr.indexExp :
           self.process_array (expr)
       elif expr.kind == expr.subexprExp :
           self.process_expr (expr.inner_expr)
       elif isinstance (expr, CmmPostfixExpr) :
           self.process_expr (expr.left)
       elif isinstance (expr, CmmUnaryExpr) :
           self.process_expr (expr.param)
       elif isinstance (expr, CmmBinaryExpr) :
           self.process_expr (expr.left)
           self.process_expr (expr.right)

   def process_array (self, expr) :
       if expr.left.kind == expr.varExp :
          array_name = expr.left.name
          info = array_name
          for item in expr.param.items :
              var, inc = self.process_index (item)
              if var == None :
                 txt = "<unknown_base>"
              else :
                 txt = var
              if inc == None :
                 txt = txt + " + <unknown increment>"
              if inc >= 0 :
                 txt = txt + " + " + str (inc)
              else :
                 txt = txt + " - " + str (-inc)
              info = info + " [" + txt + "]"
          self.putLn (info)

   def process_index (self, expr) :
       var = None
       inc = 0
       if isinstance (expr, CmmBinaryExpr) :
          var, inc = self.process_index (expr.left)
          if expr.kind == expr.addExp :
             inc = inc + self.process_const (expr.right)
          elif expr.kind == expr.subExp :
             inc = inc - self.process_const (expr.right)
          else :
             inc = None
       elif expr.kind == expr.varExp :
          var = expr.name
       return (var, inc,)

   def process_const (self, expr) :
       result = None
       if expr.kind == expr.intValueExp :
          result = int (expr.value)
       elif expr.kind == expr.subexprExp :
          result = self.process_const (expr.inner_expr)
       elif isinstance (expr, CmmBinaryExpr) :
          left = self.process_const (expr.left)
          right = self.process_const (expr.right)
          if expr.kind == expr.addExp : result = left + right
          if expr.kind == expr.subExp : result = left - right
          if expr.kind == expr.mulExp : result = left * right
          if expr.kind == expr.divExp : result = left / right
          if expr.kind == expr.modExp : result = left % right
       return result

# --------------------------------------------------------------------------

class Data (Writer) :

   def process_class (self, decl) :
       self.class_list.append (decl)

   def process_function (self, decl) :
       name = decl.name
       if name == "read" :
          self.read_list.append (decl)
       if name == "write" :
          self.write_list.append (decl)

   def send_program (self, decl_list) :
       self.class_list = [ ]
       self.read_list = [ ]
       self.write_list = [ ]

       for decl in decl_list.items :
          if isinstance (decl, CmmSimpleDecl) :
             if decl.body != None :
                self.process_function (decl)
          if isinstance (decl, CmmClass) :
             self.process_class (decl)

       for cls in self.class_list :
           self.generate_class_method (cls, read = True)
           self.generate_class_method (cls, read = False)

   def generate_class_method (self, cls, read) :
       name = cls.name
       if read :
          name = "read" + name
       else :
          name = "write" + name

       self.send ("void")
       self.send (name)
       self.send ("(")
       self.send (")")
       self.putEol ()
       self.put ("{")
       self.style_indent ()

       for item in cls.items :
          if item.body == None :
             self.generate_field (item, read)

       self.style_unindent ()
       self.put ("}")
       self.style_empty_line ()

   def generate_field (self, field, read) :
       if read :
          func = self.search_method (self.read_list, field.type)
       else :
          func = self.search_method (self.write_list, field.type)
       if func != None :
          if len (func.param_list.items) == 2 :
             widget_param = func.param_list.items [0]
             variable_param = func.param_list.items [1]
             self.rename_map = { }
             self.rename_map [variable_param.name] = field.name
             for stat in func.body.items :
                 self.send_stat (stat)
                 self.style_new_line ()
             self.style_empty_line ()

   def search_method (self, func_list, type_name) :
       result = None
       for func in func_list :
          if len (func.param_list.items) == 2 :
             variable_param = func.param_list.items [1]
             if variable_param.type == type_name :
                result = func
                break
       return result

   def send_expression (self, expr) :
       if expr.kind == expr.varExp :
          name = expr.name
          if name in self.rename_map :
             name = self.rename_map [name]
          self.send (name)
       else :
          super (Data, self). send_expression (expr)

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

