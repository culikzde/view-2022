
enum Symbol { alpha, beta, gamma };

class Record
{
    int x;
    short y;
    long z;
    float f;
    double d;
    bool b;
    char c;
    Symbol e;
    string s;
    QString t;
    QStringList a;
}

void read (QLineEdit w, QString v) { v = w.text (); }
void write (QLineEdit w, QString v) { w.setText (v); }

void read (QSpinBox w, int v) { v = w.value (); }
void write (QSpinBox w, int v) { w.setValue (v); }

void read (QDoubleSpinBox w, double v) { v = w.value (); }
void write (QDoubleSpinBox w, double v) { w.setValue (v); }

void read (QCheckBox w, bool v) { v = w.checked (); }
void write (QCheckBox w, bool v) { w.setChecked (v); }

void read (QComboBox w, Symbol v) { v = w.currentIndex (); }
void write (QCheckBox w, Symbol v)
{
   w.insertItem (0, "alpha");
   w.insertItem (1, "beta");
   w.insertItem (2, "gamma");
   w.setCurrentIndex (v);
}

void write (QListWidget w, QStringList v) { w.insertItems (v); }
