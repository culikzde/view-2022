
# simple.py

from __future__ import print_function

import sys, os

from util import import_qt_modules, optional_qt_module, optional_qt_modules, use_new_qt, use_new_api, use_pyside6, dialog_to_str
import_qt_modules (globals ())

from util import findColor, findIcon, Text, defineColor

from input import fileNameToIndex, indexToFileName
from lexer import Lexer

from area import Area
from edit import Editor
from tree import TreeItem

# --------------------------------------------------------------------------

class Simple (object) :
   def __init__ (self) :
       self.item_ink = None
       self.item_paper = None
       self.tooltip = None

       self.src_file = -1

class Module (Simple) :
   def __init__ (self) :
       super (Module, self).__init__ ()
       self.item_name = ""
       self.items = [ ]

class Class (Simple) :
   def __init__ (self) :
       super (Class, self).__init__ ()
       self.item_name = ""
       self.items = [ ]

class Function (Simple) :
   def __init__ (self) :
       super (Function, self).__init__ ()
       self.item_name = ""
       self.type = ""
       self.parameters = [ ]

class Variable (Simple) :
   def __init__ (self) :
       super (Variable, self).__init__ ()
       self.item_name = ""
       self.type = ""
       self.value = ""
       self.items = [ ]

# --------------------------------------------------------------------------

class SimpleLexer (Lexer) :

   def __init__ (self) :
       super (SimpleLexer, self).__init__ ()

   def getIdentifier (self, msg = "") :
       name = ""
       if not self.isIdentifier () :
          if msg == "" :
             msg = "Identifier"
          self.error (msg + " expected")
       else :
          name = self.tokenText
       return name

   def readIdentifier (self, msg = "") :
       name = self.getIdentifier (msg)
       self.nextToken ()
       return name

   def readString (self) :
       if self.token != self.string_literal :
          self.error ("String expected")
       value = self.tokenText
       self.nextToken ()
       return value

   def optionalSemicolon (self) :
       if self.isSeparator (";") :
          self.nextToken ()

# --------------------------------------------------------------------------

class SimpleParser (SimpleLexer) :

   def __init__ (self) :
       super (SimpleParser, self).__init__ ()
       self.path = [ ]

   def openScope (self, obj) :
       obj.item_dict = { }
       obj.item_list = [ ]
       self.path.append (obj)

   def closeScope (self) :
       self.path.pop ()

   def enterScope (self, obj) :
       if len (self.path) == 0 :
          obj.item_qual = obj.item_name
       else :
          scope = self.path [-1]
          obj.item_qual = scope.item_qual + "." + obj.item_name
          scope.item_dict [obj.item_name] = obj
          scope.item_list.append (obj)

   def parseNamespace (self) :

       result = Module ()
       result.src_file = self.tokenFileInx
       result.src_line = self.tokenLineNum
       color = findColor ("namespace")
       result.item_ink = color
       result.item_icon = "namespace"
       result.item_expand = True
       self.setInk (color)
       self.nextToken ()

       result.item_name = self.getIdentifier ("Module name")
       self.enterScope (result)
       self.openScope (result)
       self.selectColor (result)
       self.markDefn (result)
       self.markOutline (result)
       self.addToolTip ("namespace identifier")
       self.nextToken ()

       self.addToolTip ("opening namespace " + result.item_name)
       self.setInk (color)
       self.checkSeparator ("{")

       while not self.isSeparator ("}") :
          if not self.parseAttributes (result) :
             item = self.parseCode ()
             result.items.append (item)

       self.addToolTip ("closing namespace " + result.item_name)
       self.setInk (color)
       self.checkSeparator ("}")
       self.optionalSemicolon ()

       self.closeScope ()
       return result

   def parseClass (self) :
       result = Class ()
       result.src_line = self.tokenLineNum
       color = findColor ("class")
       result.item_ink = color
       result.item_icon = "class"
       result.item_expand = True
       self.setInk (color)
       self.nextToken ()

       result.item_name = self.getIdentifier ("Class name")
       self.enterScope (result)
       self.openScope (result)
       self.selectColor (result)
       self.markDefn (result)
       self.markOutline (result)
       self.nextToken ()

       self.setInk (color)
       self.checkSeparator ("{")

       while not self.isSeparator ("}") :
          if not self.parseAttributes (result) :
             item = self.parseCode ()
             result.items.append (item)

       self.setInk (color)
       self.checkSeparator ("}")
       self.optionalSemicolon ()

       self.closeScope ()
       return result

   # function

   def parseParameter (self, func) :
       item = Variable ()
       # self.setUnderlineColor (Qt.blue)
       item.type = self.readIdentifier ("Parameter type")
       item.item_name = self.readIdentifier ("Parameter name")
       func.parameters.append (item)

   def parseFunction (self) :
       result = Function ()
       result.src_line = self.tokenLineNum
       color = findColor ("function")
       result.item_ink = color
       result.item_icon = "function"

       result.type = self.readIdentifier ("Function type")
       if self.isIdentifier () :
          result.item_name = self.readIdentifier ("Function name")
       else :
          result.item_name = result.type
          result.type = ""

       self.enterScope (result)
       self.openScope (result)
       self.selectColor (result)
       self.markDefn (result)
       self.markOutline (result)

       self.setInk (color)
       self.checkSeparator ("(")

       if not self.isSeparator (")") :
          self.parseParameter (result)
          while self.isSeparator (",") :
             self.nextToken ()
             self.parseParameter (result)

       self.setInk (color)
       self.checkSeparator (")")

       if self.isSeparator ("{") :
          self.setInk (color)
          self.checkSeparator ("{")

          level = 1
          while level > 0 :
             if not self.parseAttributes (result) :
                if self.isSeparator ("{") :
                   level = level + 1
                if self.isSeparator ("}") :
                   level = level - 1
                if level > 0 :
                   self.nextToken ()

          self.setInk (color)
          self.checkSeparator ("}")

       self.optionalSemicolon ()
       self.closeScope ()

       return result

   def parseVariable (self) :
       result = Variable ()
       result.src_line = self.tokenLineNum
       color = findColor ("variable")
       result.item_ink = color
       result.item_icon = "variable"

       result.type = self.readIdentifier ("Variable type")
       if self.isIdentifier () :
          result.item_name = self.readIdentifier ("variable name")
       else :
          result.item_name = result.type
          result.type = ""

       if result.item_name != "" :
          self.enterScope (result)
          self.openScope (result)
          self.selectColor (result)
          self.markDefn (result)
          self.markOutline (result)

       if self.isSeparator ("{") :
          self.setInk (color)
          self.checkSeparator ("{")

          while not self.isSeparator ("}") :
             if not self.parseAttributes (result) :
                item = self.parseCode ()
                result.items.append (item)

          self.setInk (color)
          self.checkSeparator ("}")
          self.optionalSemicolon ()

       else :
          if self.isSeparator ("=") :
             self.nextToken ()
             if self.token == self.identifier or self.token == self.string_literal :
                result.value = self.tokenText
                self.nextToken ()
             else :
                self.error ("Variable value expected")

          while not self.isSeparator (";") and not self.isSeparator ("}") and self.token != self.eos :
             self.nextToken ();

          self.checkSeparator (";")

       if result.item_name != "" :
          self.closeScope ()
       return result

   def parseAttributeWithParameter (self) :
       self.nextToken () # attribute name
       self.checkSeparator ("(")
       text = self.readString ()
       self.checkSeparator (")")
       self.optionalSemicolon ()
       return text

   def parseAttributes (self, target) :
       cont = True
       count = 0
       while cont :
          count = count + 1
          if self.isKeyword ("warning") :
             fileName = indexToFileName (self.tokenFileInx)
             line = self.tokenLineNum
             col = self.tokenColNum
             text = self.parseAttributeWithParameter ()
             print (fileName + ":" + str (line) + ":" + str (col) + ": warning: " + text)
          elif self.isKeyword ("error") :
             fileName = indexToFileName (self.tokenFileInx)
             line = self.tokenLineNum
             col = self.tokenColNum
             text = self.parseAttributeWithParameter ()
             print (fileName + ":" + str (line) + ":" + str (col) + ": error: " + text)
          elif self.isKeyword ("ink") :
             target.item_ink = self.parseAttributeWithParameter ()
          elif self.isKeyword ("paper") :
             target.item_paper = self.parseAttributeWithParameter ()
          elif self.isKeyword ("tooltip") :
             target.tooltip = self.parseAttributeWithParameter ()
          else :
             count = count - 1
             cont = False
       return count > 0

   def parseCode (self) :
       result = None

       if self.isKeyword ("namespace") :
          result = self.parseNamespace ()

       elif self.isKeyword ("class") :
          result = self.parseClass ()

       elif self.isIdentifier () :
          func = False
          pos = self.mark ()
          self.nextToken () # first identifier
          if self.isIdentifier () :
             self.nextToken () # next (optional) identifier
          if self.isSeparator ("(") :
             func = True
          self.rewind (pos)

          if func :
             result = self.parseFunction ()
          else:
             result = self.parseVariable ()

       else :
          self.error ("Declaration expected")

       return result

# --------------------------------------------------------------------------

class SimpleDesigner (object) :

    def __init__ (self, design, code) :
       super (SimpleDesigner, self).__init__ ()
       self.design = design
       self.designBranch (code, 100, 100)
       design.addArea (code.area)

    def designBranch (self, code, x0, y0) :
        if isinstance (code, Variable) and (code.type.startswith ("Q") or code.type == "" and code.item_name.startswith ("Q")):
           self.designWidget (code, x0, y0)
        else :
           self.designArea (code, x0, y0)

    def designArea (self, code, x0, y0) :
        area = Area ()
        area.setToolTip (code.item_name)

        if isinstance (code, Module) :
           color = "namespace"
        elif isinstance (code, Class) :
           color = "class"
        elif isinstance (code, Function) :
           color = "function"
        elif isinstance (code, Variable) :
           color = "variable"
        else :
           color = ""

        code.area = area

        color = findColor (color)
        area.setBrush (QBrush (color))
        area.setPen (QPen (Qt.red))

        if code.item_ink != None:
           area.setPen (QColor (code.item_ink))
        if code.item_paper != None :
           area.setBrush (QColor (code.item_paper))
        if code.tooltip != None :
           area.setToolTip (code.tooltip)

        xspace = 20
        yspace = 20
        x = x0 + xspace
        y = y0 + yspace
        w = 60 + 2 * xspace
        h = 2 * yspace
        if hasattr (code, "items") :
           for item in code.items :
               self.designBranch (item, x, y)

               rect = item.area.boundingRect ()
               width = rect.width ()
               height = rect.height ()

               area.addArea (item.area)
               if isinstance (item.area, QGraphicsProxyWidget) :
                  item.area.setPos (x, y + yspace)
               else :
                  item.area.setRect (x, y, width, height)

               if w < width + 2*xspace :
                  w = width + 2*xspace
               y = y + height + yspace
               h = h + height + yspace

        # rect = area.boundingRect ()
        area.setRect (x0, y0, w, h)

    def designWidget (self, code, x0, y0) :
        if code.type == "" :
           code.type = code.item_name
           code.item_name = ""

        widget = eval (code.type + " ()")

        proxy = QGraphicsProxyWidget ()
        proxy.setWidget (widget)
        # proxy.setWindowFlags (Qt.Dialog)
        proxy.setFlag (QGraphicsItem.ItemIsMovable)
        proxy.setFlag (QGraphicsItem.ItemIsSelectable)

        code.area = proxy
        self.design.addArea (proxy)

        for item in code.items :
           if isinstance (item, Variable) :
              if item.value != None :
                 # setattr (widget, item.item_name, item.value)
                 quote = '\"'
                 if item.type == "QColor" :
                     param = "QColor (" + quote + item.value + quote + ")"
                 else :
                     param = quote + item.value + quote
                 eval ("widget.set" + item.item_name.capitalize() + " (" + param + ")")

# --------------------------------------------------------------------------

class SimpleHighlighter (QSyntaxHighlighter) :
   def __init__ (self, parent = None) :
       super (SimpleHighlighter, self).__init__ (parent)
       self.lexer = Lexer ()

       self.moduleFormat = self.format (Qt.green)
       self.classFormat = self.format (Qt.blue)
       self.identifierFormat = self.format (Qt.darkRed)
       self.qtFormat = self.format (findColor ("lime"))
       # self.variableFormat = self.format (QColor ("orange"))

   def format (self, color) :
       result = QTextCharFormat ()
       result.setForeground (color)
       return result

   def highlightBlock (self, text) :
       lexer = self.lexer
       lexer.openString (text)
       while not lexer.isEndOfSource () :
          index = lexer.tokenByteOfs
          length = lexer.charByteOfs - lexer.tokenByteOfs
          if lexer.token == lexer.identifier :
             if lexer.tokenText == "namespace" :
                self.setFormat (index, length, self.moduleFormat)
             elif lexer.tokenText == "class" :
                self.setFormat (index, length, self.classFormat)
             elif lexer.tokenText.startswith ("Q") :
                self.setFormat (index, length, self.qtFormat)
             else:
                self.setFormat (index, length, self.identifierFormat)
          lexer.nextToken ()

# --------------------------------------------------------------------------

class SimpleCompleter (QCompleter) :
   def __init__ (self, parent=None) :
       words = [ "namespace", "class", "function" ]
       super (SimpleCompleter, self).__init__ (words, parent)

# --------------------------------------------------------------------------

def simpleCompile (win, fileName) :

    parser = SimpleParser ()
    parser.openFile (fileName, with_support = True)

    code = parser.parseCode ()
    print ("parsed")

    e = win.loadFile (fileName)

    e.highlighter = SimpleHighlighter (e.document ()) # keep highlighter reference

    e.setCompleter (SimpleCompleter ())

    win.initProject (e) # Project

    win.showClasses (code) # Classes

    win.displayCompilerData (e, code) # Tree

    win.addNavigatorData (e, code) # Navigator

    SimpleDesigner (win.design, code)

# --------------------------------------------------------------------------

class SimplePlugin (QObject) :

   def __init__ (self, main_window) :
       super (SimplePlugin, self).__init__ (main_window)
       self.win = main_window

       self.simpleMenu = self.win.menuBar().addMenu ("Simple")
       self.simpleMenu.aboutToShow.connect (self.onShowSimpleMenu)

       act = QAction ("&Compile", self.win)
       # act.setShortcut ("Ctrl+F11")
       act.triggered.connect (self.simpleCompile)
       self.simpleMenu.addAction (act)

   def onShowSimpleMenu (self) :
       is_simple = self.win.hasExtension (self.win.getEditor(), ".sim")
       for act in self.simpleMenu.actions () :
          act.setEnabled (is_simple)

   def simpleCompile (self) :
       e = self.win.getEditor ()
       if self.win.hasExtension (e, ".sim") :
          source = str (e.toPlainText ())
          e.setPlainText (source) # reset colors and properties

          simpleCompile (self.win, e.getFileName ())

# --------------------------------------------------------------------------

if __name__ == "__main__" :
    parser = SimpleParser ()
    parser.openFile ("tutorial/example.sim")
    code = parser.parseCode ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
