
/*
number
real_numbe
identifier
string_literal
character_literal
*/

defined_name <TDefinesName> :
   id:identifier
   ( '*' <set defn=true> )? ;

qualified_name <TQualName> :
   id:identifier
   ( <add> cont_name )* ;

cont_name <TContName> :
   <no_space> '.' <no_space> id:identifier ;

/* ---------------------------------------------------------------------- */

expr_item <new TExprItem> :
   value:expr ;

expr_list <new TInxList> :
   <add> expr_item ( ',' <add> expr_item )* ;

/* set constructor */

elem_sect <TElemSect> :
   <add> elem_item ( ',' <add> elem_item )* ;

elem_item <TElemItem> :
   low:expr
   (
      ".."
      high:expr
   )? ;

/* factor */

ident_expr <TIdentExpr:TExpr> :
   name:identifier
   <execute on_ident_expr> ;

int_value_expr <TIntValueExpr:TExpr> :
   value:number ;

float_value_expr <TFltValueExpr:TExpr> :
   value:real_number ;

string_value_expr <TStrValueExpr:TExpr> :
   value:string_literal ;

char_value_expr <TStrValueExpr:TExpr> :
   value:character_literal ; /* #digits */

nil_expr <TNilExpr:TExpr> :
   "NIL" ;

true_expr <TTrueExpr:TExpr> :
   "TRUE" ;

false_expr <TFalseExpr:TExpr> :
   "FALSE" ;

sub_expr <TStructExpr:TExpr> :
   '(' value:expr ( ',' <add> expr )* ')' ;

plus_expr <TPlusExpr:TExpr> :
   '+' param:factor ;

minus_expr <TMinusExpr:TExpr> :
   '-' param:factor ;

not_expr <TNotExpr:TExpr> :
   '~' param:factor ;

set_expr <TSetExpr:TExpr> :
   '{' item_list:elem_sect '}' ;

variable <select TExpr> :
   ident_expr |
   sub_expr ;

reference <choose TExpr> :
   variable
   (
      index_expr |
      call_expr |
      deref_expr |
      field_expr
   )* ;

index_expr <TIndexExpr:TExpr> :
   <store left:TExpr>
   '['
   value_list:expr_list
   ']'
   ;

call_expr <TCallExpr:TExpr> :
   <store func:TExpr>
   '('
   value_list:expr_list
   ')'
   ;

deref_expr <TDerefExpr:TExpr> :
   <store param:TExpr>
   '^'
   ;

field_expr <TFieldExpr:TExpr> :
   <store param:TExpr>
   <no_space>
   '.'
   <no_space>
   name:identifier
   <execute on_field_expr>
   ;

/* factor */

factor <select TExpr> :
   int_value_expr |
   float_value_expr |
   string_value_expr  |
   char_value_expr  |
   plus_expr |
   minus_expr |
   not_expr |
   nil_expr |
   true_expr |
   false_expr |
   set_expr |
   reference  ;

/* term */

term <choose TExpr> :
  factor
  (
     <new TTermExpr:TExpr>
     <store left:TExpr>
     (  '*'   <set fce=MulExp>  |
        '/'   <set fce=RDivExp> |
        "DIV" <set fce=DivExp>  |
        "MOD" <set fce=ModExp>  |
        "&"   <set fce=AndExp>  )
     right:factor
  )* ;

/* simple expression */

simple_expr <choose TExpr> :
  term
  (
     <new TSimpleExpr:TExpr>
     <store left:TExpr>
     (  '+'   <set fce=AddExp> |
        '-'   <set fce=SubExp> |
        "OR"  <set fce=OrExp>  )
     right:term
  )* ;

/* expression */

expr <choose TExpr> :
  simple_expr
  (
     <new TCompleteExpr:TExpr>
     <store left:TExpr>
     (  '='  <set fce=EqExp> |
        "#" <set fce=NeExp> |
        '<'  <set fce=LtExp> |
        '>'  <set fce=GtExp> |
        "<=" <set fce=LeExp> |
        ">=" <set fce=GeExp> |
        "in" <set fce=InExp> |
        "is" <set fce=IsExp> )
     right:simple_expr
  )? ;

const_expr <return TExpr> :
   expr;

/* ---------------------------------------------------------------------- */

stat_list <TStatSect> :
   <new_line>
   <indent>
   <add> stat
   ( ';' <new_line> <add> stat )*
   <unindent>
   <new_line> ;

/* if */

if_stat <TIfStat:TStat> :
  "IF"
  cond_expr:expr
  "THEN"
  then_stat:stat_list
  elseif_stat:elseif_sect
  (
     "ELSE"
     else_stat:stat_list
  )?
  "END" ;

elseif_item <TElseItem> :
   "ELSIF"
   cond_expr:expr
   "DO"
   cond_stat:stat_list ;

elseif_sect <TElseSetct> :
   ( <add> elseif_item )* ;

/* case */

case_stat <TCaseStat:TStat> :
  "CASE"
  case_expr:expr
  "OF"
  case_list:case_sect
  (
     "else"
     else_seq:stat_list
  )?
  "END" ;

case_sect <TCaseSect> :
  <add> case_item ( <new_line> '|'  <add> case_item )*;

case_item <TCaseItem> :
   sel_list:elem_sect
   ':'
   sel_stat:stat_list ;

/* while */

while_stat <TWhileStat:TStat> :
  "WHILE" cond_expr:expr "DO"
     body_stat:stat_list
     elseif_stat:elseif_sect
  "END" ;

/* repeat */

repeat_stat <TRepeatStat:TStat> :
  "REPEAT"
  body_seq:stat_list
  "UNTIL" until_expr:expr ;

/* for */

for_stat <TForStat:TStat> :
  "FOR"
  var_expr:variable
  ":="
  from_expr:expr
  "TO"
  to_expr:expr
  ( "BY" by_expr:const_expr ) ?
  "DO"
  body_stat:stat_list
  "END" ;

/* simple statement */

simple_stat <choose TStat> :
  expr
  (
     assign_stat
  |
     call_stat
  ) ;

assign_stat <TAssignStat:TStat> :
  <store left_expr:TExpr>
  ":="
  right_expr:expr
  ;

call_stat <TCallStat:TStat> :
  <store call_expr:TExpr>
  ;

/* statement */

stat <select TStat> :
  if_stat |
  case_stat |
  while_stat |
  repeat_stat |
  for_stat |
  simple_stat ;

/* ---------------------------------------------------------------------- */

param_ident <TParamIdent> :
  name:identifier
  <execute on_param>
  ;

param_item <TParamItem> :
  ( "var" <set var_param=true> )?
  <add> param_ident
  ( ',' <add> param_ident )*
  ':'
  ( "ARRAY" "OF" <set array_of=true> )?
  typ:qualified_name
  <execute add_param>
  ;

formal_param_list <TParamSect> :
   (
      '('
          <add> param_item
          ( ';' <add> param_item )*
      ')'
   )?
   ( ':' answer:type ) ?
   ;


/* ---------------------------------------------------------------------- */

/* array */

array_type <TArrayType:TType> :
   <execute simple_type>
  "ARRAY"
  index_list:index_type_sect
  "OF"
  elem:type;

index_type_sect <TIndexTypeSect> :
   <add> index_type_item ( ',' <add> index_type_item )* ;

index_type_item <TIndexTypeItem> :
   length:const_expr ;

/* record */

record_type <TRecordType:TType> :
   <execute simple_type>
  "RECORD"
  ( "(" base_type:qualified_name ")" )?
  fields:field_sect
  "END"
  ;

field_ident <TFieldItem> :
   name:defined_name
   <execute on_field> ;

field_decl <TFieldDecl:TDeclSect> :
   <add> field_ident
   ( ',' <add> field_ident )*
   ':'
   typ:type
   ';'
   <execute add_field>
   ;

field_sect <TFieldDeclGroup> :
   ( <add> field_decl )* ;

/* pointer */

pointer_type <TPointerType:TType> :
   <execute simple_type>
   "POINTER" "TO" elem:type;

/* procedure */

proc_type <TProcType:TType> :
   <execute simple_type>
   "PROCEDURE"
   param_list:formal_param_list
   ( ':' answer:type )?
   ;

/* other type */

alias_type <TAliasType:TType> :
   <execute simple_type>
   alias_name:qualified_name
   ;

/* type */

type <select TType> :
   array_type |
   record_type |
   pointer_type |
   proc_type |
   alias_type ;

/* ---------------------------------------------------------------------- */

/* constants */

const_decl <TConstDecl> :
   <new_line>
   name:defined_name
   <execute on_const>
   '=' val:const_expr ';'
   <execute add_const>
   ;

const_sect <TConstSect:TDeclSect> :
   "CONST"
   <indent>
   <add> const_decl
   ( <add> const_decl )*
   <unindent> ;

/* types */

type_decl <TTypeDecl> :
   <new_line>
   name:defined_name
   <execute on_type>
   '=' typ:type ';'
   <execute add_type>
   ;

type_sect <TTypeSect:TDeclSect> :
   "TYPE"
   <indent>
   <add> type_decl
   ( <add> type_decl )*
   <unindent> ;

/* variables */

var_item <TVarItem> :
   name:defined_name
   <execute on_var>
   ;

var_decl <TVarDecl> :
   <new_line>
   <add> var_item
   ( ',' <add> var_item )*
   ':' typ:type
   ';'
   <execute add_var>
   ;

var_sect <TVarSect:TDeclSect> :
   "VAR"
   <indent>
   <new_line> <add> var_decl
   ( <new_line> <add> var_decl )*
   <unindent> ;

/* subroutine */

proc_decl <TProcDecl> :
   "PROCEDURE"
   proc_name:defined_name
   <execute on_proc>
   param_list:formal_param_list
   <new_line>
   decl:decl_part
   <execute on_begin_proc>
   (
      "BEGIN"
      body:stat_list
   )?
   (
      "RETURN"
      answer_value:expr
   )?
   "END" second_name:identifier <no_space>
   ';'
   <execute close_proc>
   <empty_line> ;

/* declarations */

decl <select TDeclSect> :
   const_sect |
   type_sect |
   var_sect |
   proc_decl ;

decl_part <TDeclGroup> :
   ( <new_line> <add> decl )* ;

/* import */

import_item <new TImportItem> :
  name:identifier <execute on_import>
  ( ":=" from_name:identifier )? ;

import_sect <new TImportSect> :
  (
     "IMPORT"
     <add> import_item
     ( ',' <add> import_item )*
     ';'
  )? ;

/* module */

module_decl <new TModule> <start> :
   "MODULE" name:identifier <execute on_module> ';'
   <empty_line>
   imports:import_sect
   <empty_line>
   decl:decl_part
   (
      "BEGIN"
      init:stat_list
   )?
   "END" second_name:identifier <no_space> '.'
   <execute close_module>
   ;
