
# pas_plugin.py

from __future__ import print_function

import os, sys, importlib

from util import import_qt_modules
import_qt_modules (globals ())

# from util import findColor, findIcon
# from tree import TreeItem
from env import CodePlugin
# from gcc_options import gcc_options, pkg_options

# --------------------------------------------------------------------------

class PascalPlugin (CodePlugin) :

   def __init__ (self, main_window) :
       super (PascalPlugin, self).__init__ (main_window)

       if 1 :
          menu = self.win.addTopMenu (self.mod.title)

          act = QAction ("Pascal &Make", self.win)
          # act.setShortcut ("Ctrl+F11")
          act.triggered.connect (self.pascalCompile)
          menu.addAction (act)

          act = QAction ("Pascal &Build", self.win)
          # act.setShortcut ("Ctrl+F12")
          act.triggered.connect (self.pascalBuild)
          menu.addAction (act)

          act = QAction ("&Oberon Build", self.win)
          # act.setShortcut ("Ctrl+F10")
          act.triggered.connect (self.oberonBuild)
          menu.addAction (act)

   # -----------------------------------------------------------------------

   def pascalCompile (self) :
       "called from tools.cfg menu item"
       self.pascalFunc (rebuild = False)

   def pascalBuild (self) :
       "called from tools.cfg menu item"
       self.pascalFunc (rebuild = True)

   def pascalFunc (self, rebuild) :

       self.setup ()
       self.start ("pas/examples/idedemo.pas")

       self.grammarFileName = "pas/pas.g"

       self.compilerFileName = "pas/pas_comp.py"
       self.compilerClassName = "Compiler"
       self.compilerFuncName = "parse_module_decl"

       self.buildParser (rebuild)
       self.buildCParser (rebuild)
       self.buildPasParser (rebuild)
       self.compile ()

   # -----------------------------------------------------------------------

   def oberonBuild (self, rebuild = True) :
       "called from tools.cfg menu item"

       self.setup ()
       self.start ("pas/examples/idedemo.pas")

       self.grammarFileName = "pas/oberon.g"

       self.compilerFileName = "pas/oberon_comp.py"
       self.compilerClassName = "Compiler"
       self.compilerFuncName = "parse_module_decl"

       self.buildParser (rebuild)
       # self.buildCParser (rebuild)
       # self.buildPasParser (rebuild)
       self.compile ()
       print ("O.K.")

       # o2c

       outputFileName = self.win.outputFileName (self.sourceFileName, "_o2c.cpp")

       p2c_module = self.win.loadModule ("pas/o2c.py")
       p2c_object = p2c_module.O2C ()

       p2c_object.open (outputFileName, with_sections = True)
       p2c_object.send_module_decl (result)
       p2c_object.close ()

       print ("Oberon to C++ O.K.")

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
