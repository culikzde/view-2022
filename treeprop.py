#!/usr/bin/env python

from __future__ import print_function

import sys

from util import import_qt_modules, use_pyqt6
import_qt_modules (globals ())

# --------------------------------------------------------------------------

class ItemEditor (QWidget) :
    def __init__ (self, parent = None) :
        super (ItemEditor, self).__init__ (parent)

        line_edit = None
        combo_box = None
        button = None

# --------------------------------------------------------------------------

class ListDelegate (QItemDelegate):

    def __init__ (self, parent=None) :
        super (ListDelegate, self).__init__ (parent)

    def createEditor(self, parent, option, index) :
        if index.column () == 0 :
           return None

        editor = ItemEditor (parent)

        layout =  QHBoxLayout (editor)
        # layout.setMargin (0)
        layout.setSpacing (0)
        editor.setLayout (layout)

        if True :
           editor.combo_box = QComboBox (editor)
           layout.addWidget (editor.combo_box)
           editor.combo_box.addItems (["abc", "def", "klm"])
        else :
           editor.line_edit = QLineEdit (editor)
           layout.addWidget (editor.line_edit)

        if True :
           editor.button = QPushButton (editor)
           editor.button.setText ("...")
           editor.button.setMaximumWidth (32)
           layout.addWidget (editor.button)

        return editor

    def setEditorData (self, editor, index) :
        text = index.model().data(index, Qt.DisplayRole).toString()
        if editor.combo_box != None :
           i = editor.combo_box.findText (text)
           if i == -1:
              i = 0
           editor.combo_box.setCurrentIndex (i)

    def setModelData (self, editor, model, index) :
        if editor.combo_box != None :
           model.setData (index, editor.combo_box.currentText())

    def paint (self, painter, option, index) :
        r = QRect (option.rect.left(), option.rect.top(), option.rect.width(), option.rect.height()) # copy
        # r = QRect (option.rect) # copy
        if index.column() == 0 :
           r.setX (0)

        if False :
           if False :
              if index.row () & 1 == 0 :
                 c = self.evenColor
              else :
                 c = self.oddColor
           else :
              if index.row () & 1 == 0 :
                 c = QColor ("lightyellow")
              else :
                 c = QColor ("lightblue")

           painter.save()
           painter.setBrushOrigin (r.x(), r.y())
           painter.fillRect (r, c)
           painter.restore()

        QItemDelegate.paint (self, painter, option, index);

        # color = QColor (QApplication.style().styleHint (QStyle.SH_Table_GridLineColor, option))
        color = QColor ("cornflowerblue")
        oldPen = painter.pen ()
        # painter.setPen (QPen (color))
        painter.setPen (color)

        painter.drawLine (r.right(), r.y(), r.right(), r.bottom())
        painter.drawLine (r.x(), r.bottom(), r.right(), r.bottom())

        painter.setPen (oldPen)

# see ColorDelegate::paint in qt-4.8.0/tools/designer/src/components/propertyeditor/paletteeditor.cpp

    def updateEditorGeometry (self, param_editor, option, index) :
        QItemDelegate.updateEditorGeometry (self, param_editor, option, index)
        param_editor.setGeometry (param_editor.geometry().adjusted(0, 0, -1, -1))

    def sizeHint (self, option, index) :
        return QItemDelegate.sizeHint (self, option, index) + QSize (2*4, 2*4)

# --------------------------------------------------------------------------

class TreeProperty (QTreeWidget) :

    def __init__ (self, parent=None) :
        super (TreeProperty, self).__init__ (parent)

        tree = self

        self.setColumnCount (2)
        self.setHeaderLabels (["Name", "Value"])

        self.setAlternatingRowColors (True)
        self.setEditTriggers (QAbstractItemView.CurrentChanged)

        delegate = ListDelegate ()
        # delegate.evenColor = self.palette().color (QPalette.Normal, QPalette.Base)
        delegate.evenColor = QColor ("white")
        # delegate.oddColor = self.palette().color (QPalette.Normal, QPalette.AlternateBase)
        delegate.oddColor = QColor ("cornflowerblue")
        self.setItemDelegate (delegate)
        # self.setItemDelegateForRow (5, delegate)

        item = QTreeWidgetItem ()
        item.setText (0, "First")
        item.setText (1, "one")
        self.addTopLevelItem (item)

        branch = item
        item = QTreeWidgetItem ()
        item.setText (0, "Second")
        item.setText (1, "two")
        # item.setData (0, Qt.BackgroundRole, QColor ("yellow"))
        # item.setData (0, Qt.ForegroundRole, QColor ("blue"))
        # item.setData (0, Qt.FontRole, QFont ("Courier", 16))
        item.setData (1, Qt.DecorationRole, QColor ("yellow"))
        item.setData (1, Qt.CheckStateRole, Qt.Checked)
        item.setFlags (item.flags() | Qt.ItemIsEditable)
        # self.addTopLevelItem (item)
        branch.addChild (item)

        item = QTreeWidgetItem ()
        item.setText (0, "Boolean")
        item.setData (1, Qt.EditRole, True)
        item.setFlags (item.flags() | Qt.ItemIsEditable)
        self.addTopLevelItem (item)

        item = QTreeWidgetItem ()
        item.setText (0, "Integer")
        item.setData (1, Qt.EditRole, 1)
        item.setData (1, Qt.DisplayRole, 100)
        item.setFlags (item.flags() | Qt.ItemIsEditable)
        self.addTopLevelItem (item)

        item = QTreeWidgetItem ()
        item.setText (0, "Double")
        item.setData (1, Qt.EditRole, 3.14)
        item.setFlags (item.flags() | Qt.ItemIsEditable)
        self.addTopLevelItem (item)

        item = QTreeWidgetItem ()
        item.setText (0, "List")
        item.setData (1, Qt.EditRole, QColor ("yellow"))
        item.setData (1, Qt.EditRole, "klm")
        item.setFlags (item.flags() | Qt.ItemIsEditable)
        self.addTopLevelItem (item)

# --------------------------------------------------------------------------

if __name__ == '__main__' :
   app = QApplication (sys.argv)
   app.setStyle (QStyleFactory.create ("cleanlooks"))
   # app.setStyle ("gtk")
   # app.setStyle ("windows")
   # app.setStyle ("plastique")
   # app.setStyleSheet("QObject {font: 14px;}")

   app.setStyleSheet("QTreeView {alternative_tab_widget-background-color: cornflowerblue; background: yellow; }")
   app.setStyleSheet("QTreeView {border-width: 1px; border-style: solid; border-color: blue; }")

   window = TreeProperty ()
   window.show ()
   app.exec_ ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
