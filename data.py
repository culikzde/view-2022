#!/usr/bin/env python

from __future__ import print_function

import sys

from util import import_qt_modules
import_qt_modules (globals ())

if __name__ == '__main__' :
   sys.path.insert (1, "code")

from lexer import Lexer

# --------------------------------------------------------------------------

class Group (object) :
   def __init__ (self) :
      self.name = ""
      self.groups = [ ]
      self.packages = [ ]

class Package (object) :
   def __init__ (self) :
      self.name = ""
      self.packed_size = 0
      self.install_size = 0
      self.files = [ ]

class File (object) :
   def __init__ (self) :
      self.name = ""
      self.size = 0

# --------------------------------------------------------------------------

def style (dcl) :

    # if "name" in dcl.item_dict :
    #   dcl.setText (0, dcl.item_dict ["name"].item_value)
    # else :
    if dcl.text (0) == "" :
       dcl.setText (0, dcl.item_qual)

    if "ink" in dcl.item_dict :
       dcl.setForeground (0, QColor (dcl.item_dict ["ink"].item_value))

    if "paper" in dcl.item_dict :
       dcl.setBackground (0, QColor (dcl.item_dict ["paper"].item_value))

    if "icon" in dcl.item_dict :
       dcl.setIcon (0, QIcon (dcl.item_dict ["icon"].item_value))

    if "tooltip" in dcl.item_dict :
       dcl.setTooltip (0, dcl.item_dict ["tooltip"].item_value)

    for item in dcl.item_list :
        style (item)

# --------------------------------------------------------------------------

def readValue (inp) :
    obj = None
    above = inp.tree_stack [-1]
    if inp.isKeyword ("None") :
       obj = None
    elif inp.isKeyword ("True") or inp.isKeyword ("true") :
       obj = True
       inp.nextToken ()
    elif inp.isKeyword ("False") or inp.isKeyword ("false")  :
       obj = False
       inp.nextToken ()
    elif inp.isNumber () :
       obj = int (inp.tokenText)
       inp.nextToken ()
    elif inp.isReal () :
       obj = float (inp.tokenText)
       inp.nextToken ()
    elif inp.isString () or inp.isCharacter () :
       obj = inp.tokenText
       inp.nextToken ()
    elif inp.isSeparator ('[') :
       obj = [ ]
       dcl = TreeItem (above, "list")
       inp.tree_stack.append (dcl)
       inp.nextToken ()
       if not inp.isSeparator (']') :
          item = readValue (inp)
          obj.append (item)
          while inp.isSeparator (',') :
             inp.nextToken ()
             item = readValue (inp)
             obj.append (item)
       inp.checkSeparator (']')
       inp.tree_stack.pop ()
    elif inp.isSeparator ('(') :
       obj = ( )
       dcl = TreeItem (above, "tupple")
       inp.tree_stack.append (dcl)
       inp.nextToken ()
       if not inp.isSeparator (')') :
          item = readValue (inp)
          obj = obj + ( item, )
          while inp.isSeparator (',') :
             inp.nextToken ()
             item = readValue (inp)
             obj = obj + ( item, )
       inp.checkSeparator (')')
       inp.tree_stack.pop ()
    elif inp.isSeparator ('{') :
       obj = ( )
       dcl = TreeItem (above, "dict")
       inp.tree_stack.append (dcl)
       inp.nextToken ()
       if not inp.isSeparator ('}') :
          key = readValue (inp)
          inp.checkSeparator (":")
          val = readValue (inp)
          obj [key] = val
          while inp.isSeparator (',') :
             inp.nextToken ()
             key = readValue (inp)
             inp.checkSeparator (":")
             val = readValue (inp)
             obj [key] = val
       inp.tree_stack.pop ()
       inp.checkSeparator ('}')
    elif inp.isIdentifier () :
       obj = readObject (inp)
    else :
       inp.error ("Value expected");
    return obj

def readObject (inp) :
    obj = None
    name = inp.readIdentifier ("Type name expected")
    obj = eval (name + "()")

    above = inp.tree_stack [-1]
    dcl = TreeItem (above)
    dcl.setText (0, name)
    dcl.item_name = name
    dcl.src_file = inp.prevFileInx
    dcl.src_line = inp.prevLineNum
    dcl.src_column = inp.prevColNum
    dcl.src_pos = inp.prevByteOfs
    dcl.src_end = inp.prevEndOfs
    if above.item_qual == "" :
       dcl.item_qual = dcl.item_name
    else :
       dcl.item_qual = above.item_qual + "." + dcl.item_name
    above.item_list.append (dcl)
    above.item_dict [name] = dcl
    inp.tree_stack.append (dcl)

    inp.checkSeparator ("{")
    while not inp.isSeparator ('}') :
       item_name = inp.readIdentifier ("Item name expected");

       above = inp.tree_stack [-1]
       dcl = TreeItem (above)
       dcl.setText (0, item_name)
       dcl.item_name = item_name
       dcl.src_file = inp.prevFileInx
       dcl.src_line = inp.prevLineNum
       dcl.src_column = inp.prevColNum
       dcl.src_pos = inp.prevByteOfs
       dcl.src_end = inp.prevEndOfs
       dcl.item_qual = above.item_qual + "." + dcl.item_name
       above.item_list.append (dcl)
       above.item_dict [item_name] = dcl
       inp.tree_stack.append (dcl)

       inp.checkSeparator ("=")
       dcl.edit_pos = inp.tokenByteOfs
       item_value = readValue (inp)
       setattr (obj, item_name, item_value)
       dcl.item_value = item_value
       dcl.edit_end = inp.prevEndOfs
       inp.checkSeparator (";")
       inp.tree_stack.pop ()

    inp.checkSeparator ("}")
    inp.tree_stack.pop ()
    return obj

# --------------------------------------------------------------------------

def writeValue (out, obj) :
    out.simpleSection (obj)
    if obj == None :
       out.put ("None")
    elif ( isinstance (obj, bool) or
           isinstance (obj, int) or
           isinstance (obj, long) or
           isinstance (obj, float) or
           isinstance (obj, complex) or
           isinstance (obj, str) ) :
       out.put (str (obj))
    elif isinstance (obj, list) :
       out.putLn ("[")
       out.indent ()
       any = False
       for item in obj :
          if any :
             out.putLn (",")
          any = True
          writeValue (out, item)
       out.unindent ()
       out.putLn ("]")
    elif isinstance (obj, tuple) :
       out.putLn ("(")
       out.indent ()
       any = False
       for item in obj :
          if any :
             out.putLn (",")
          any = True
          writeValue (out, item)
       out.unindent ()
       out.putLn (")")
    elif isinstance (obj, dict) :
       out.putLn ("{")
       out.indent ()
       any = False
       for key in obj :
          if any :
             out.putLn (",")
          any = True
          out.put (key)
          out.put (": ")
          writeValue (out, obj [key])
       out.unindent ()
       out.putLn ("}")
    else :
       writeObject (out, obj)

def writeObject (out, obj) :
    out.openSection (obj)
    out.putLn (type (obj).__name__)
    out.putLn ("{")
    out.indent ()

    properties = obj.__dict__
    if hasattr (obj, "_properties_") :
       properties = obj._properties_

    for item_name in properties :
        item_obj = getattr (obj, item_name, None)
        out.put (item_name)
        out.put (" = ")
        writeValue (out, item_obj)
        out.putLn (";")

    out.unindent ()
    out.putLn ("}")
    out.closeSection ()

# --------------------------------------------------------------------------

class ColorItem (QToolButton):

   def __init__ (self, page, name, color) :
       super (ColorItem, self).__init__ (page)
       self.name = name
       self.setText (name)
       self.setColor (color)
       page.addWidget (self)

   def setColor (self, color) :
       self.color = color

       p = self.palette ()
       p.setColor (QPalette.ButtonText, self.color)
       self.setPalette (p)

       pixmap = QPixmap (12, 12)
       pixmap.fill (Qt.transparent)

       painter = QPainter (pixmap)
       painter.setPen (Qt.NoPen)
       painter.setBrush (QBrush (self.color))
       painter.drawEllipse (0, 0, 12, 12)
       painter.end ()

       self.setIcon (QIcon (pixmap))

   def mousePressEvent (self, event) :

       if event.button() == Qt.LeftButton :
          drag =  QDrag (self)

          mimeData = QMimeData ()
          mimeData.setColorData (self.color)
          drag.setMimeData (mimeData)

          pixmap = QPixmap (12, 12)
          pixmap.fill (Qt.transparent)

          painter = QPainter (pixmap)
          painter.setPen (Qt.NoPen)
          painter.setBrush (QBrush (self.color))
          painter.drawEllipse (0, 0, 12, 12)
          painter.end ()

          drag.setPixmap (pixmap)
          drag.setHotSpot (QPoint (6, 6))

          dropAction = drag.exec_ (Qt.MoveAction | Qt.CopyAction | Qt.LinkAction)

# --------------------------------------------------------------------------

class ComponentItem (QToolButton):

   def __init__ (self, page, name) :
       super (ComponentItem, self).__init__ (page)
       self.name = name
       self.setText (name)
       page.addWidget (self)

   def mousePressEvent (self, event) :

       if event.button() == Qt.LeftButton :
          drag =  QDrag (self)

          mimeData = QMimeData ()
          mimeData.setData ("application/x-component", bytearray (self.name, "ascii"))

          drag.setMimeData (mimeData)

          dropAction = drag.exec_ (Qt.MoveAction | Qt.CopyAction | Qt.LinkAction)

# --------------------------------------------------------------------------

class Tree (QTreeWidget):

   def __init__ (self, win = None) :
       super (Tree, self).__init__ (win)
       self.win = win
       self.itemClicked.connect (self.onItemClicked)

   def onItemClicked (self, node, column) :
       self.win.prop.showNode (node)

class TreeItem (QTreeWidgetItem):

   def __init__ (self, above = None, text = "") :
       super (TreeItem, self).__init__ (above)
       if text != "" :
          self.setText (0, text)

       self.item_name = ""
       self.item_value = None
       self.item_qual = ""
       self.item_list = [ ]
       self.item_dict = { }

       self.src_file = 0
       self.src_line = 0
       self.src_column = 0
       self.src_pos = 0
       self.src_end = 0
       self.edit_pos = 0
       self.edit_end = 0

# --------------------------------------------------------------------------

class Property (QTableWidget) :

   def __init__ (self, win = None) :
       super (Property, self).__init__ (win)
       self.win = win
       self.setColumnCount (2)
       self.setRowCount (0)
       self.setHorizontalHeaderLabels (["Name", "Value"])
       self.horizontalHeader().setStretchLastSection (True)
       self.verticalHeader().setVisible (False)
       self.cellChanged.connect (self.onCellChanged)

   def onCellChanged (self, line, col) :
       if col == 1 :
          item = self.item (line, col)
          if hasattr (item, "edit_pos") :
             edit = self.win.edit
             cursor = edit.textCursor ()
             cursor.setPosition (item.edit_pos)
             cursor.setPosition (item.edit_end, QTextCursor.KeepAnchor)
             cursor.removeSelectedText ()
             fmt = cursor.charFormat ()
             fmt.setForeground (QColor ("red"))
             cursor.setCharFormat (fmt)
             item.edit_pos = cursor.position ()
             cursor.insertText (item.text ())
             item.edit_end = cursor.position ()

   def showLine (self, name, value) :
       inx = self.rowCount ()
       self.setRowCount (inx+1)

       item = QTableWidgetItem ()
       item.setText (name)
       item.setFlags (item.flags () & ~ Qt.ItemIsEditable)
       self.setItem (inx, 0, item)

       item = QTableWidgetItem ()
       item.setText (str (value))
       self.setItem (inx, 1, item)

       return item


   def showNode (self, dcl) :
       self.setRowCount (0)
       for name in dcl.item_dict :
           data = dcl.item_dict [name]
           item = self.showLine (name, data.item_value)
           item.edit_pos = data.edit_pos
           item.edit_end = data.edit_end

# --------------------------------------------------------------------------

class MainWindow (QMainWindow) :

   def __init__ (self, parent = None) :
       super (MainWindow, self).__init__ (parent)

       self.toolbar = self.addToolBar ("Main")
       self.status = self.statusBar ()

       # above

       self.palette = QTabWidget (self)

       # left

       self.tree = Tree (self)

       # middle

       self.tabs = QTabWidget (self)

       # right

       self.prop = Property (self)

       # bottom

       self.info = QPlainTextEdit (self)

       # layout

       self.hsplitter = QSplitter ()
       self.hsplitter.addWidget (self.tree)
       self.hsplitter.addWidget (self.tabs)
       self.hsplitter.addWidget (self.prop)
       self.hsplitter.setStretchFactor (0, 1)
       self.hsplitter.setStretchFactor (1, 4)
       self.hsplitter.setStretchFactor (2, 1)

       self.vsplitter = QSplitter ()
       self.vsplitter.setOrientation (Qt.Vertical)
       self.vsplitter.addWidget (self.hsplitter)
       self.vsplitter.addWidget (self.info)
       self.vsplitter.setStretchFactor (0, 3)
       self.vsplitter.setStretchFactor (1, 1)

       self.vbox = QVBoxLayout ()
       self.vbox.addWidget (self.palette)
       self.vbox.addWidget (self.vsplitter)
       self.vbox.setStretchFactor (self.palette, 0)
       self.vbox.setStretchFactor (self.vsplitter, 1)

       self.vwidget = QWidget (self)
       self.vwidget.setLayout (self.vbox)
       self.setCentralWidget (self.vwidget)

       # file menu

       self.fileMenu = self.menuBar().addMenu ("&File")
       menu = self.fileMenu

       act = QAction ("&Quit", self)
       act.setShortcut ("Ctrl+Q")
       act.setShortcutContext (Qt.ApplicationShortcut)
       act.setIcon (QIcon ("application-exit"))
       act.triggered.connect (self.quit)
       menu.addAction (act)

       # edit menu

       self.editMenu = self.menuBar().addMenu ("&Edit")
       menu = self.editMenu

       # view menu

       self.viewMenu = self.menuBar().addMenu ("&View")
       menu = self.viewMenu

       # palette

       colors = [ "red", "blue", "green", "yellow", "orange" ]
       page = QToolBar (self)
       self.palette.addTab (page, "colors")
       for name in colors :
           ColorItem (page, name, QColor (name))

       components = [ "Group", "Package", "File" ]
       page = QToolBar (self)
       self.palette.addTab (page, "components")
       for name in components :
           ComponentItem (page, name)

       # edit

       self.edit = QTextEdit (self)
       self.tabs.addTab (self.edit, "edit")

   def quit (self) :
       QApplication.instance().quit()

   def readSource (self, fileName) :
       inp = Lexer ()
       dcl = TreeItem (self.tree, "data")
       inp.tree_stack = [dcl]
       inp.openFile (fileName)
       while not inp.isEndOfSource () :
          readObject (inp)
       inp.close ()
       self.tree.expandAll ()

       f = open (fileName)
       text = f.read ()
       self.edit.setPlainText (text)

       # style (dcl)

# --------------------------------------------------------------------------

if __name__ == '__main__' :
   app = QApplication (sys.argv)

   win = MainWindow ()
   win.show ()
   win.readSource ("examples/packages.t")

   app.exec_ ()

# --------------------------------------------------------------------------

# kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
